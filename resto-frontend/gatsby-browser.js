/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */
import React from 'react';
import CartProvider from './src/components/providers/cart-provider';
import MenuProvider from './src/components/providers/menu-provider';
import NotificationProvider from './src/components/providers/notification-provider';
import SessionStateProvider from './src/components/providers/session-state-provider';
import SidemenuProvider from './src/components/providers/sidemenu-provider';
import UserProvider from './src/components/providers/user-provider';

// You can delete this file if you're not using it
export const wrapPageElement = ({ element }) => {
  return (
    <SessionStateProvider>
      <UserProvider>
        <SidemenuProvider>
          <MenuProvider>
            <NotificationProvider>
              <CartProvider>
                {element}
              </CartProvider>
            </NotificationProvider>
          </MenuProvider>
        </SidemenuProvider>
      </UserProvider>
    </SessionStateProvider>
  );
};

export const onRouteUpdate = ({ location, prevLocation }) => {
  if (typeof(window.fbq) !== 'undefined') {
    window.fbq('track', 'PageView');
  }
};
