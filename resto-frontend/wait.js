const { default: axios } = require('axios');

(async () => {
  while (true) {
    try {
      await new Promise((resolve) => setTimeout(resolve, 1000));
      await axios.get('http://admin:1337');
      return;
    } catch (e) {
      console.log('MISS admin');
    }
  }
})();
