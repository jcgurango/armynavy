/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */
const path = require('path');
const fs = require('fs');
const slugify = require('slugify');
const crypto = require('crypto');
const showdown = require('showdown');
const { default: axios } = require('axios');
const markdown = new showdown.Converter();
const now = (new Date()).toISOString();

/*
exports.onPreInit = () => {
  if (process.argv[2] === "build") {
    fs.rmdirSync(path.join(__dirname, "dist"), { recursive: true })

    if (fs.existsSync(path.join(__dirname, "public"))) {
      fs.renameSync(
        path.join(__dirname, "public"),
        path.join(__dirname, "public_dev")
      );
    }
  }
};

exports.onPostBuild = () => {
  fs.renameSync(path.join(__dirname, "public"), path.join(__dirname, "dist"))

  if (fs.existsSync(path.join(__dirname, "public_dev"))) {
    fs.renameSync(
      path.join(__dirname, "public_dev"),
      path.join(__dirname, "public")
    );
  }
};
*/
exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type StrapiSharedProductAddonsLink implements Node {
      id: Int!
    }

    type StrapiAddon {
      id: Int!
      Name: String!
      Price: Float
    }

    type StrapiProductVariationsAddons {
      id: Int!
      Name: String!
      Price: Float
      Category: String
    }

    type StrapiSelectableOptions {
      id: Int!
      Name: String!
      Count: Int!
      Options: [StrapiAddon]
    }

    type StrapiProductVariations implements Node {
      SharedAddons: [StrapiSharedProductAddonsLink!]
      FixedOptions: [StrapiAddon]
      SelectableOptions: [StrapiSelectableOptions]
    }
  `;
  createTypes(typeDefs)
};

exports.sourceNodes = async ({ actions }) => {
  const { createNode } = actions;
  const { data: regions } = await axios.get('/orders/regions', {
    baseURL: process.env.BACKEND_API_BASE_URL,
  });

  await Promise.all(regions.map(async (region) => {
    await createNode({
      ...region,
      id: `${region.id}`,
      internal: {
        type: 'OfsRegion',
        contentDigest: crypto
          .createHash(`md5`)
          .update(JSON.stringify(region))
          .digest(`hex`),
      },
    });
  }));
  const { data: cities } = await axios.get('/orders/cities', {
    baseURL: process.env.BACKEND_API_BASE_URL,
  });

  await Promise.all(cities.map(async (city) => {
    await createNode({
      ...city,
      id: `${city.id}`,
      region_id: `${city.region_id}`,
      internal: {
        type: 'OfsCity',
        contentDigest: crypto
          .createHash(`md5`)
          .update(JSON.stringify(city))
          .digest(`hex`),
      },
    });
  }));
};

exports.onCreateNode = ({ node, actions }) => {
  if (node.internal.type === 'StrapiProduct') {
    node.Slug = node.Slug || slugify(node.Name).toLowerCase();
    node.Position = node.Position || 0;
    node.Voucher = node.Voucher || false;
  }

  if (node.internal.type === 'StrapiProductCategories') {
    node.Slug = node.Slug || slugify(node.Category).toLowerCase();
    node.Position = node.Position || 0;
    let defaultColor = null;
    let defaultButtonColor = null;

    if (process.env.GATSBY_SITE === 'ltmt') {
      defaultColor = 'white';
    }

    if (process.env.GATSBY_SITE === 'ww') {
      defaultColor = '#3a3935';
      defaultButtonColor = '#d98723';
    }

    node.Metadata = {
      ...(node.Metadata || {}),
      color: node.Metadata && node.Metadata.color || defaultColor,
      buttonColor: node.Metadata && node.Metadata.buttonColor || defaultButtonColor,
    };
  }

  if (node.internal.type === 'StrapiPage') {
    if (!node.Slug) {
      node.Slug = slugify(node.Name).toLowerCase();
    }

    node.Description = node.Description || '';
    node.MetaTitle = node.MetaTitle || node.Title;
    node.MetaDescription = node.MetaDescription || node.Description;
    node.Layout = node.Layout || 'page';
    node.HtmlContent = node.Content && markdown.makeHtml(node.Content);
  }

  if (node.internal.type === 'StrapiPickupLocations') {
    node.Telephone = node.Telephone || '';
    node.Mobile = node.Mobile || '';
    node.Open247 = node.Open247 || false;
  }

  if (node.internal.type === 'StrapiStorefront') {
    node.UnderMaintenance = node.UnderMaintenance || false;
    node.Unavailable = node.Unavailable || null;
    node.MetaTitle = node.MetaTitle || '';
    node.MetaDescription = node.MetaDescription || '';
  }
};

exports.createResolvers = ({ createResolvers }) => {
  console.log(process.env.GATSBY_SITE);
  createResolvers({
    Query: {
      sitePickupLocations: {
        type: ['StrapiPickupLocations'],
        resolve(source, args, context, info) {
          return context.nodeModel.runQuery({
            query: {
              filter: {
                storefront: {
                  Slug: {
                    eq: process.env.GATSBY_SITE,
                  },
                },
                PickupLocation: {
                  eq: true,
                },
              },
            },
            type: 'StrapiPickupLocations',
            firstOnly: false,
          })
        },
      },
      siteStoreLocations: {
        type: ['StrapiPickupLocations'],
        resolve(source, args, context, info) {
          return context.nodeModel.runQuery({
            query: {
              filter: {
                storefront: {
                  Slug: {
                    eq: process.env.GATSBY_SITE,
                  },
                },
              },
            },
            type: 'StrapiPickupLocations',
            firstOnly: false,
          })
        },
      },
      siteStorefront: {
        type: ['StrapiStorefront'],
        resolve(source, args, context, info) {
          return context.nodeModel.runQuery({
            query: {
              filter: {
                Slug: {
                  eq: process.env.GATSBY_SITE,
                },
              },
            },
            type: 'StrapiStorefront',
            firstOnly: false,
          })
        },
      },
      siteProducts: {
        type: ['StrapiProduct'],
        resolve(source, args, context, info) {
          let extraFilters = {};

          return context.nodeModel.runQuery({
            query: {
              filter: {
                Storefront: {
                  Slug: {
                    eq: process.env.GATSBY_SITE,
                  },
                },
                ...extraFilters,
              },
            },
            type: 'StrapiProduct',
            firstOnly: false,
          })
        },
      },
      siteCmsPages: {
        type: ['StrapiPage'],
        resolve(source, args, context, info) {
          return context.nodeModel.runQuery({
            query: {},
            type: 'StrapiPage',
            firstOnly: false,
          })
        },
      },
      siteProductCategories: {
        type: ['StrapiProductCategories'],
        resolve(source, args, context, info) {
          return context.nodeModel.runQuery({
            query: {
              filter: {
                Storefront: {
                  Slug: {
                    eq: process.env.GATSBY_SITE,
                  },
                },
              },
            },
            type: 'StrapiProductCategories',
            firstOnly: false,
          })
        },
      },
    },
  });
};

exports.createPages = async ({ graphql, actions: { createPage } }) => {
  // **Note:** The graphql function call returns a Promise
  // see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise for more info
  const { data: result } = await graphql(`
    query {
      siteProductCategories {
        Slug
        Products {
          Slug
          Name
        }
      }
      siteCmsPages {
        id
        Slug
        Layout
      }
    }
  `);

  createPage({
    path: `/menu`,
    component: path.resolve(`./src/layouts/menu.js`),
    context: {
      categorySlug: process.env.GATSBY_DEFAULT_CATEGORY,
      site: process.env.GATSBY_SITE,
    },
  });

  result.siteProductCategories.forEach(({ Slug, Products }) => {
    createPage({
      path: `/menu/${Slug}`,
      component: path.resolve(`./src/layouts/menu.js`),
      context: {
        categorySlug: Slug,
        site: process.env.GATSBY_SITE,
      },
    });

    Products.forEach(({ Slug: productSlug, Name }) => {
      createPage({
        path: `/menu/${Slug}/products/${productSlug || slugify(Name).toLowerCase()}`,
        component: path.resolve(`./src/layouts/menu.js`),
        context: {
          categorySlug: Slug,
          site: process.env.GATSBY_SITE,
          productSlug: productSlug || slugify(Name).toLowerCase(),
        },
      });
    });
  });

  result.siteCmsPages.forEach(({ id, Slug, Layout }) => {
    createPage({
      path: `/${Slug}`,
      component: path.resolve(`./src/layouts/${Layout || 'page'}.js`),
      context: {
        pageId: id,
      },
    });
  });
};
