export const Colors = {
  grey: 'rgb(200, 200, 200)',
  primary: 'rgb(150, 219, 229)',
  secondary: 'rgb(186, 205, 185)',
  darkText: 'rgb(50, 49, 47)',
  danger: 'red',
  dangerLight: 'pink',
};

if (process.env.GATSBY_SITE === 'rr') {
  Colors.primary = '#730a11';
  Colors.secondary = '#f27920';
  Colors.darkText = 'white';
  Colors.dangerLight = 'red';
}

if (process.env.GATSBY_SITE === 'ww') {
  Colors.primary = '#d98723';
  Colors.secondary = '#3a3935';
}

if (process.env.GATSBY_SITE === 'an') {
  Colors.primary = '#ed6f23';
  Colors.secondary = '#30705a';
}

if (process.env.GATSBY_SITE === 'pt') {
  Colors.primary = '#cadb2a';
  Colors.secondary = '#cadb2a';
}

const Common = {
  spacing: '0.5em',
};

const Branding = {
  Theme: {
    Colors,
    Common,
  },
  isMessenger: false,
  Input: ({ Theme }) => `
    padding: 0.75em;
    border-radius: 0.5em;
    border: 1px solid rgba(125, 125, 125, 0.25);
    margin-bottom: ${Theme.Common.spacing};
    outline: 0;
    font-size: inherit;

    &.has-error {
      border-color: ${Theme.Colors.dangerLight};
      margin-bottom: -1em;
    }
  `,
  Button: ({ Theme }, color) => `
    background-color: ${Theme.Colors[color || 'primary']};
    color: ${Theme.Colors.darkText};

    ${(['ww', 'an'].includes(process.env.GATSBY_SITE) && (!color || color === 'primary')) ? `
      color: white;
    ` : ''}

    ${(['an'].includes(process.env.GATSBY_SITE) && (!color || color === 'primary')) ? `
      &:hover {
        background-color: ${Theme.Colors.secondary};
      }

      transition: 0.5s;
    ` : ''}

    ${(['pt'].includes(process.env.GATSBY_SITE) && (!color || color === 'primary')) ? `
      &:hover {
        background-color: #f2fc8d !important;
      }

      transition: 0.5s;
    ` : ''}

    ${['an'].includes(process.env.GATSBY_SITE) && color === 'secondary' ? `
      background-color: inherit;
    ` : ''}

    ${['ww'].includes(process.env.GATSBY_SITE) && color === 'secondary' ? `
      color: white;
    ` : ''}

    padding: 0.75em 2em;
    border-radius: 1em;
    border: 0px;
    font-size: inherit;
    margin-bottom: ${Theme.Common.spacing};
    outline: 0;
    font-weight: bold;
    text-transform: uppercase;
    box-shadow: 0px 0px 0.25em rgba(0, 0, 0, 0.25);

    :disabled {
      color: ${Theme.Colors.darkText};
      background-color: ${Theme.Colors.grey};
    }

    ${['pt'].includes(process.env.GATSBY_SITE) ? `
      border-radius: 2em;
      text-transform: initial;
      font-size: 1.5em;
      padding: 0.35em 1em;
    ` : ''}
  `,
  Notification: ({ Theme }) => `
    padding: 0.75em;
    margin: 0.5em;
    margin-left: auto;
    margin-right: auto;
    width: 18em;
    border-radius: 0.25em;
    box-sizing: border-box;
    box-shadow: 0px 0px 0.25em rgba(0, 0, 0, 0.25);
    max-width: 300px;
  `,
  InfoNotification: ({ Theme }) => `
    background-color: ${Theme.Colors.primary};

    ${process.env.GATSBY_SITE === 'rr' ? `
      color: white;
    ` : ''}
  `,
  ErrorNotification: ({ Theme }) => `
    background-color: ${Theme.Colors.dangerLight};
  `,
  Box: ({ Theme }) => `
    border: 0.1em solid ${Theme.Colors.primary};
    border-top-width: 1em;
    padding: 1em;
  `,
};

export default Branding;
