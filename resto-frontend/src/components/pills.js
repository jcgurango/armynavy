import styled from 'styled-components';

const Pills = styled.div`
  margin-top: 0.5em;
  margin-bottom: 0.5em;
  display: flex;
  flex-direction: row;

  > *:not(.ignore-style) {
    display: inline-block;
    padding: 0.25em 0.75em;
    font-size: 1.2em;
    border: 1px solid rgba(125, 125, 125, 0.25);

    ${() => process.env.GATSBY_SITE === 'rr' ? (`
      border-color: white;
    `) : null}

    color: inherit;
    text-decoration: inherit;
    ${({ withBg }) => (
      withBg ? 'background-color: rgb(230, 230, 230);' : ''
    )}
  }

  > :nth-child(n+2) {
    ${({ allBorders }) => (
      allBorders ? '' : 'border-left: 0px;'
    )}
  }

  > :first-child {
    border-top-left-radius: 0.25em;
    border-bottom-left-radius: 0.25em;
  }

  > :last-child {
    border-top-right-radius: 0.25em;
    border-bottom-right-radius: 0.25em;
  }

  > .active {
    background-color: ${({ activeColor = 'white' }) => activeColor};
    color: ${({ activeTextColor = process.env.GATSBY_SITE === 'an' ? 'white' : 'inherit' }) => activeTextColor};
  }
`;

export default Pills;
