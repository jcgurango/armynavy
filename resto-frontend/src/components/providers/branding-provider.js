import React, { createContext, useContext } from 'react';
import Branding from '../../branding';

const BrandingContext = createContext(Branding);
export const useBranding = () => useContext(BrandingContext);

/**
 * @type {React.FunctionComponent<{
 *  override: (theme: Theme) => Theme,
 * }>}
 */
const BrandingProvider = ({
  children,
  override,
}) => {
  const branding = useBranding();

  return (
    <BrandingContext.Provider
      value={override(branding)}
    >
      {children}
    </BrandingContext.Provider>
  );
};

export default BrandingProvider;
