import { graphql, useStaticQuery } from 'gatsby';
import React, { useContext } from 'react';
import moment from 'moment';

export const TestProduct = {
  "Categories": [
    {
      "id": "2"
    }
  ],
  "Addons": [
    {
      "id": 1,
      "Name": "Pistachio Butter",
      "Price": "50.00"
    },
    {
      "id": 2,
      "Name": "Cheesecake",
      "Price": "30.00"
    }
  ],
  "Variations": [
    {
      "id": 1,
      "Price": "15.00",
      "Text": "TEST",
      "Image": {
        "childImageSharp": {
          "gatsbyImageData": {
            "layout": "constrained",
            "backgroundColor": "#f8f8f8",
            "images": {
              "fallback": {
                "src": "/static/2722c7dac1340da8d5084bbc17ec8ef8/02dff/cf7c9c1f72076dcd52d91d7539ad4526.jpg",
                "srcSet": "/static/2722c7dac1340da8d5084bbc17ec8ef8/0fdf4/cf7c9c1f72076dcd52d91d7539ad4526.jpg 300w,\n/static/2722c7dac1340da8d5084bbc17ec8ef8/a89ca/cf7c9c1f72076dcd52d91d7539ad4526.jpg 600w,\n/static/2722c7dac1340da8d5084bbc17ec8ef8/02dff/cf7c9c1f72076dcd52d91d7539ad4526.jpg 1200w",
                "sizes": "(min-width: 1200px) 1200px, 100vw"
              },
              "sources": [
                {
                  "srcSet": "/static/2722c7dac1340da8d5084bbc17ec8ef8/078c3/cf7c9c1f72076dcd52d91d7539ad4526.webp 300w,\n/static/2722c7dac1340da8d5084bbc17ec8ef8/6d09e/cf7c9c1f72076dcd52d91d7539ad4526.webp 600w,\n/static/2722c7dac1340da8d5084bbc17ec8ef8/83805/cf7c9c1f72076dcd52d91d7539ad4526.webp 1200w",
                  "type": "image/webp",
                  "sizes": "(min-width: 1200px) 1200px, 100vw"
                }
              ]
            },
            "width": 1200,
            "height": 1200
          }
        }
      },
      "Addons": [
        {
          "id": 35,
          "Name": "TEST OTHER ADDON",
          "Price": "15.00"
        }
      ]
    }
  ],
  "Image": {
    "childImageSharp": {
      "gatsbyImageData": {
        "layout": "constrained",
        "backgroundColor": "#f8f8f8",
        "images": {
          "fallback": {
            "src": "/static/74972d9224fb9bd318507bdd79976c02/02dff/827866023ed7874d434a970efc5f76f3.jpg",
            "srcSet": "/static/74972d9224fb9bd318507bdd79976c02/0fdf4/827866023ed7874d434a970efc5f76f3.jpg 300w,\n/static/74972d9224fb9bd318507bdd79976c02/a89ca/827866023ed7874d434a970efc5f76f3.jpg 600w,\n/static/74972d9224fb9bd318507bdd79976c02/02dff/827866023ed7874d434a970efc5f76f3.jpg 1200w",
            "sizes": "(min-width: 1200px) 1200px, 100vw"
          },
          "sources": [
            {
              "srcSet": "/static/74972d9224fb9bd318507bdd79976c02/078c3/827866023ed7874d434a970efc5f76f3.webp 300w,\n/static/74972d9224fb9bd318507bdd79976c02/6d09e/827866023ed7874d434a970efc5f76f3.webp 600w,\n/static/74972d9224fb9bd318507bdd79976c02/83805/827866023ed7874d434a970efc5f76f3.webp 1200w",
              "type": "image/webp",
              "sizes": "(min-width: 1200px) 1200px, 100vw"
            }
          ]
        },
        "width": 1200,
        "height": 1200
      }
    }
  },
  "strapiId": 1,
  "Name": "Classic",
  "Description": "Black Tea, Milk, and Pearls",
  "Slug": "classic",
  "Voucher": false,
};

const testData = {
  "siteProducts": [
    TestProduct,
    {
      "Categories": [
        {
          "id": "4"
        }
      ],
      "Addons": [],
      "Variations": [],
      "Image": null,
      "strapiId": 2,
      "Name": "Salted Caramel",
      "Description": "This product isn't as good.",
      "Slug": "salted-caramel"
    }
  ],
  "siteProductCategories": [
    {
      "Image": {
        "childImageSharp": {
          "gatsbyImageData": {
            "layout": "constrained",
            "backgroundColor": "#b8d8e8",
            "images": {
              "fallback": {
                "src": "/static/2d4f77319765a1f4ed58586a6c636615/0d8e8/45d054244af28dc95bb76eaf3102b546.png",
                "srcSet": "/static/2d4f77319765a1f4ed58586a6c636615/185a6/45d054244af28dc95bb76eaf3102b546.png 188w,\n/static/2d4f77319765a1f4ed58586a6c636615/844cd/45d054244af28dc95bb76eaf3102b546.png 376w,\n/static/2d4f77319765a1f4ed58586a6c636615/0d8e8/45d054244af28dc95bb76eaf3102b546.png 751w",
                "sizes": "(min-width: 751px) 751px, 100vw"
              },
              "sources": [
                {
                  "srcSet": "/static/2d4f77319765a1f4ed58586a6c636615/99097/45d054244af28dc95bb76eaf3102b546.webp 188w,\n/static/2d4f77319765a1f4ed58586a6c636615/52ff0/45d054244af28dc95bb76eaf3102b546.webp 376w,\n/static/2d4f77319765a1f4ed58586a6c636615/2a474/45d054244af28dc95bb76eaf3102b546.webp 751w",
                  "type": "image/webp",
                  "sizes": "(min-width: 751px) 751px, 100vw"
                }
              ]
            },
            "width": 751,
            "height": 501
          }
        }
      },
      "Category": "Classic",
      "Slug": "classic",
      "strapiId": 2
    },
    {
      "Image": {
        "childImageSharp": {
          "gatsbyImageData": {
            "layout": "constrained",
            "backgroundColor": "#f8c8a8",
            "images": {
              "fallback": {
                "src": "/static/4d665b16dabab3585dda3a190fef88d4/f118f/385722e6679b0df4086cc33e3deba6cf.png",
                "srcSet": "/static/4d665b16dabab3585dda3a190fef88d4/a41f9/385722e6679b0df4086cc33e3deba6cf.png 188w,\n/static/4d665b16dabab3585dda3a190fef88d4/3e573/385722e6679b0df4086cc33e3deba6cf.png 375w,\n/static/4d665b16dabab3585dda3a190fef88d4/f118f/385722e6679b0df4086cc33e3deba6cf.png 750w",
                "sizes": "(min-width: 750px) 750px, 100vw"
              },
              "sources": [
                {
                  "srcSet": "/static/4d665b16dabab3585dda3a190fef88d4/31395/385722e6679b0df4086cc33e3deba6cf.webp 188w,\n/static/4d665b16dabab3585dda3a190fef88d4/9bc7c/385722e6679b0df4086cc33e3deba6cf.webp 375w,\n/static/4d665b16dabab3585dda3a190fef88d4/d2a19/385722e6679b0df4086cc33e3deba6cf.webp 750w",
                  "type": "image/webp",
                  "sizes": "(min-width: 750px) 750px, 100vw"
                }
              ]
            },
            "width": 750,
            "height": 501
          }
        }
      },
      "Category": "Signature",
      "Slug": "signature",
      "strapiId": 3
    },
    {
      "Image": {
        "childImageSharp": {
          "gatsbyImageData": {
            "layout": "constrained",
            "backgroundColor": "#b8c8b8",
            "images": {
              "fallback": {
                "src": "/static/09eeabdb073f27d90834ca9d2990e2d8/f118f/d830cb858f1304a6c9e1d0d56a5800d5.png",
                "srcSet": "/static/09eeabdb073f27d90834ca9d2990e2d8/a41f9/d830cb858f1304a6c9e1d0d56a5800d5.png 188w,\n/static/09eeabdb073f27d90834ca9d2990e2d8/3e573/d830cb858f1304a6c9e1d0d56a5800d5.png 375w,\n/static/09eeabdb073f27d90834ca9d2990e2d8/f118f/d830cb858f1304a6c9e1d0d56a5800d5.png 750w",
                "sizes": "(min-width: 750px) 750px, 100vw"
              },
              "sources": [
                {
                  "srcSet": "/static/09eeabdb073f27d90834ca9d2990e2d8/31395/d830cb858f1304a6c9e1d0d56a5800d5.webp 188w,\n/static/09eeabdb073f27d90834ca9d2990e2d8/9bc7c/d830cb858f1304a6c9e1d0d56a5800d5.webp 375w,\n/static/09eeabdb073f27d90834ca9d2990e2d8/d2a19/d830cb858f1304a6c9e1d0d56a5800d5.webp 750w",
                  "type": "image/webp",
                  "sizes": "(min-width: 750px) 750px, 100vw"
                }
              ]
            },
            "width": 750,
            "height": 501
          }
        }
      },
      "Category": "Cheesecake",
      "Slug": "cheesecake",
      "strapiId": 4
    },
    {
      "Image": {
        "childImageSharp": {
          "gatsbyImageData": {
            "layout": "constrained",
            "backgroundColor": "#b8d8e8",
            "images": {
              "fallback": {
                "src": "/static/fc2ad6c8680e2e999cb565218fe1edd1/f118f/289e9f1bdd5055812a4b91f8396f83b8.png",
                "srcSet": "/static/fc2ad6c8680e2e999cb565218fe1edd1/a41f9/289e9f1bdd5055812a4b91f8396f83b8.png 188w,\n/static/fc2ad6c8680e2e999cb565218fe1edd1/3e573/289e9f1bdd5055812a4b91f8396f83b8.png 375w,\n/static/fc2ad6c8680e2e999cb565218fe1edd1/f118f/289e9f1bdd5055812a4b91f8396f83b8.png 750w",
                "sizes": "(min-width: 750px) 750px, 100vw"
              },
              "sources": [
                {
                  "srcSet": "/static/fc2ad6c8680e2e999cb565218fe1edd1/31395/289e9f1bdd5055812a4b91f8396f83b8.webp 188w,\n/static/fc2ad6c8680e2e999cb565218fe1edd1/9bc7c/289e9f1bdd5055812a4b91f8396f83b8.webp 375w,\n/static/fc2ad6c8680e2e999cb565218fe1edd1/d2a19/289e9f1bdd5055812a4b91f8396f83b8.webp 750w",
                  "type": "image/webp",
                  "sizes": "(min-width: 750px) 750px, 100vw"
                }
              ]
            },
            "width": 750,
            "height": 501
          }
        }
      },
      "Category": "Cream Cheese",
      "Slug": "cream-cheese",
      "strapiId": 5
    }
  ],
  "allStrapiSharedProductAddons": {
    "nodes": [
      {
        "id": "Shared-product-addons_1",
        "Addons": [
          {
            "Name": "RCP00072",
            "POSCode": "RCP00052",
            "Price": "70.00",
            "id": 602
          }
        ],
        "strapiId": 1,
      }
    ]
  },
};

/**
 * @param {testData} data 
 */
const readData = (data) => {
  const siteProducts = data.siteProducts.filter(({ ValidFrom, ValidTo }) => {
    return process.env.NODE_ENV === 'development' || moment(ValidFrom).isSameOrBefore(moment()) && moment(ValidTo).hour(23).minute(59).second(59).millisecond(999).isSameOrAfter(moment());
  });

  // Read products.
  return {
    Products: siteProducts.map(({ strapiId, ...rest }) => ({
      ...rest,
      id: strapiId,
    })).sort((a, b) => a.Position - b.Position).map((product) => {
      return {
        ...product,
        Variations: product.Variations.map((variation) => ({
          ...variation,
          Addons: variation.Addons.concat(
            (variation.SharedAddons || [])
              .map(({ id }) => data.allStrapiSharedProductAddons.nodes.find(({ strapiId }) => strapiId == id).Addons)
              .reduce((current, next) => ([...current, ...next]), []),
          ),
        })),
      };
    }),
    Categories: data.siteProductCategories.filter((category) => {
      for (let i = 0; i < siteProducts.length; i++) {
        const product = siteProducts[i];

        for (let c = 0; c < product.Categories.length; c++) {
          const cat = product.Categories[c];

          if (String(cat.id) === String(category.strapiId)) {
            return true;
          }
        }
      }

      return false;
    }).sort((a, b) => a.Position - b.Position),
  };
};

export const MenuContext = React.createContext(readData(testData));

export const useMenu = () => useContext(MenuContext);

/**
 * @type {React.FunctionComponent<{}>}
 */
const MenuProvider = ({
  children,
}) => {
  const data = useStaticQuery(graphql`
    query {
      siteProducts {
        Categories {
          id
        }
        Variations {
          id
          Price
          Text
          Image {
            childImageSharp {
              gatsbyImageData(width: 320)
            }
          }
          MessengerImage: Image {
            childImageSharp {
              gatsbyImageData(width: 100, height: 100)
            }
          }
          Addons {
            id
            Name
            Price
            Category
          }
          SharedAddons {
            id
          }
          SelectableOptions {
            Name
            id
            Count
            Options {
              Name
              id
              Price
            }
          }
          FixedOptions {
            Name
            id
            Price
          }
        }
        strapiId
        Name
        Description
        Slug
        Position
        Voucher
        ValidFrom
        ValidTo
      }
      siteProductCategories {
        Image {
          childImageSharp {
            gatsbyImageData(width: 320)
          }
        }
        id
        Category
        Slug
        strapiId
        Metadata {
          color
          buttonColor
        }
        Position
      }
      allStrapiSharedProductAddons {
        nodes {
          id
          Addons {
            Name
            POSCode
            Price
            id
          }
          strapiId
        }
      }
    }
  `);

  return (
    <MenuContext.Provider
      value={readData(data)}
    >
      {children}
    </MenuContext.Provider>
  );
};

export default MenuProvider;
