import React, { createContext, useContext, useEffect, useState } from 'react';
import { useMenu } from './menu-provider';

/**
 * @typedef {{
 *  key: String,
 *  productId: Number,
 *  variationId: Number,
 *  addons: Number[],
 *  specialInstructions: String,
 *  quantity: Number,
 * }} OrderItem
 * @global
 */

/**
 * @type {React.Context<{
 *  items: OrderItem[],
 *  addItem: (
 *    item: OrderItem
 *  ) => void,
 *  calculateSubtotal: (
 *    item: OrderItem
 *  ) => Number,
 *  calculateTotal: (
 *    items: OrderItem[],
 *  ) => Number,
 *  calculateVoucherDiscount: (
 *    items: OrderItem[],
 *  ) => Number,
 *  updateQuantity: (key: string, quantity: Number) => void,
 *  updateItem: (key: string, item: Partial<OrderItem>) => void,
 *  removeItem: (key: string) => void,
 *  clear: () => void,
 *  deliveryData: any,
 *  setDeliveryData: () => void,
 *  editDeliveryData: () => void,
 *  deliveryFee: Number,
 * }>}
 */
export const CartContext = createContext({
  items: [],
  addItem: () => { },
  calculateSubtotal: () => 0,
  calculateTotal: () => 0,
  calculateVoucherDiscount: () => 0,
  updateQuantity: () => { },
  updateItem: () => { },
  removeItem: () => { },
  clear: () => { },
  deliveryData: { }, 
  setDeliveryData: () => { },
  editDeliveryData: () => { },
  deliveryFee: 50,
});

export const useCart = () => useContext(CartContext);

const CartProvider = ({
  children,
}) => {
  const menu = useMenu();

  const [items, setItems] = useState(() => {
    if (typeof(localStorage) !== 'undefined') {
      const savedCart = localStorage.getItem('user-cart');

      if (savedCart) {
        return JSON.parse(savedCart);
      }
    }

    return [];
  });

  useEffect(() => {
    if (typeof(localStorage) !== 'undefined') {
      localStorage.setItem('user-cart', JSON.stringify(items));
    }
  }, [items]);

  const calculateVoucherDiscount = (item) => {
    const product = menu.Products.find(({ id }) => Number(id) === Number(item.productId));

    if (product) {
      const variation = product.Variations.find(({ id }) => Number(id) === Number(item.variationId));

      if (variation && product.Voucher) {
        return Number(variation.Price);
      }
    }

    return 0;
  };

  const calculateSubtotal = (item) => {
    let sum = 0;
    const product = menu.Products.find(({ id }) => Number(id) === Number(item.productId));

    if (product) {
      const variation = product.Variations.find(({ id }) => Number(id) === Number(item.variationId));

      if (variation) {
        sum += Number(variation.Price);
      }

      if (variation.Addons) {
        variation.Addons.forEach((addon) => {
          if (item.addons.indexOf(addon.id) > -1) {
            sum += Number(addon.Price);
          }
        });
      }

      if (variation.FixedOptions) {
        variation.FixedOptions.forEach(({ Price }) => {
          sum += Number(Price);
        });
      }

      if (item.options) {
        Object.keys(item.options).forEach((optionSetId) => {
          const optionSet = variation.SelectableOptions.find(({ id }) => id === Number(optionSetId));

          Object.values(item.options[optionSetId]).forEach((optionId, i) => {
            const option = optionSet.Options.find(({ id }) => id === Number(optionId));
            sum += Number(option.Price);
          });
        });
      }
    }

    return sum * item.quantity;
  };

  return (
    <CartContext.Provider
      value={{
        items,
        addItem: (item) => {
          setItems((items) => items.concat({
            key: Math.random().toString().replace(/[.,]/g, ''),
            ...item,
          }));
        },
        calculateSubtotal,
        calculateTotal: (items) => {
          return items.reduce((total, item) => (total + calculateSubtotal(item)), 0);
        },
        calculateVoucherDiscount: (items) => {
          return items.reduce((total, item) => (total + calculateVoucherDiscount(item)), 0);
        },
        updateQuantity: (key, quantity) => {
          setItems((items) => items.map((item) => {
            if (item.key === key) {
              return {
                ...item,
                quantity,
              };
            }

            return item;
          }));
        },
        updateItem: (key, updates) => {
          setItems((items) => items.map((item) => {
            if (item.key === key) {
              return {
                ...item,
                ...updates,
              };
            }

            return item;
          }));
        },
        removeItem: (key) => {
          setItems((items) => items.filter((item) => item.key !== key));
        },
        clear: () => setItems([]),
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export default CartProvider;
