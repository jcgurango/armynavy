/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */
import React, { createContext, useContext, useState } from 'react';

const SessionStoreContext = createContext({
  set: (key, value) => { },
  get: (key) => {
    return null;
  },
  use: (key) => [null, () => { }],
});

export const useSession = () => useContext(SessionStoreContext);

const SessionStateProvider = ({
  children,
}) => {
  const [sessionState, setSessionState] = useState({ });

  const get = (key, defaultValue) => typeof(sessionState[key]) === 'undefined' ? defaultValue : sessionState[key];
  const set = (key, value) => setSessionState((state) => ({ ...state, [key]: value }));
  const use = (key, defaultValue) => [
    get(key, defaultValue),
    (value) => set(key, value),
  ];

  return (
    <SessionStoreContext.Provider
      value={{
        get,
        set,
        use,
      }}
    >
      {children}
    </SessionStoreContext.Provider>
  );
};

export default SessionStateProvider;
