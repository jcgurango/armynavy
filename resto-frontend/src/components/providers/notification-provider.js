import React, { createContext, useContext, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { useBranding } from './branding-provider';
import { FaTimes } from '@react-icons/all-files/fa/FaTimes';

/**
 * @typedef {{
 *  id: String,
 *  title: String,
 *  text: String,
 *  type: 'info' | 'error',
 *  time?: Number,
 * }} NotificationData
 */

const Notification = styled.div`
  ${({ branding }) => branding.Notification(branding)}
  ${({ type, branding }) => ({
    info: branding.InfoNotification(branding),
    error: branding.ErrorNotification(branding),
  }[type])}
`;

const NotificationContext = createContext({
  /**
   * @param {NotificationData} notification 
   */
  queue: (notification) => { },
});

export const useNotifications = () => useContext(NotificationContext);

const NotificationContainer = ({
  refObject,
}) => {
  const branding = useBranding();
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    if (refObject) {
      refObject.current = {
        /**
         * @param {NotificationData} notification 
         */
        queue: (notification) => {
          const newNotification = {
            id: Math.random().toString(),
            ...notification,
          };
          setNotifications((n) => [newNotification].concat(n));

          setTimeout(() => {
            setNotifications((n) => n.filter((n) => n.id !== newNotification.id));
          }, notification.time || 5000);
        },
      };
    }
  }, [refObject]);

  return (
    <div
      style={{
        position: 'fixed',
        top: '0px',
        right: '0px',
        left: '0px',
        zIndex: 1500,
      }}
    >
      {notifications.map((notification) => (
        <Notification branding={branding} type={notification.type} key={notification.id}>
          <b>{notification.title}</b>
          <a
            style={{ float: 'right', fontSize: '0.75em' }}
            href="/#"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              setNotifications((n) => n.filter((n) => n.id !== notification.id));
            }}
          >
            <FaTimes />
          </a>
          <div>{notification.text}</div>
        </Notification>
      ))}
    </div>
  );
};

/**
 * @type {React.FunctionComponent}
 */
const NotificationProvider = ({
  children,
}) => {
  const containerRef = useRef(null);

  return (
    <NotificationContext.Provider
      value={{
        queue: (...args) => {
          if (containerRef.current) {
            containerRef.current.queue(...args);
          }
        },
        ...containerRef.current,
      }}
    >
      {children}
      <NotificationContainer
        refObject={containerRef}
      />
    </NotificationContext.Provider>
  )
};

export default NotificationProvider;
