import { graphql, Link, useStaticQuery } from 'gatsby';
import React, { createContext, useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import { FaTimes } from '@react-icons/all-files/fa/FaTimes';
import { useLocation } from '@reach/router';
import { useBranding } from './branding-provider';
import { useIsAuthenticated } from '../user-guard';

const MenuContainer = styled.div`
  position: fixed;
  top: 0px;
  right: 0px;
  bottom: 0px;
  transition: 0.25s;
  background-color: white;
  flex-direction: row;
  padding: 4em;
  padding-right: 6em;
  transform: ${({ open }) => open ? 'translateX(0%)' : 'translateX(100%)'};
  z-index: 1000;
`;

const Anchor = styled(Link)`
  color: inherit;
  text-decoration: none;
  font-size: 1.25em;
  margin-bottom: 0.5em;
  display: block;
  transition: 0.25s;

  :hover {
    color: ${({ branding: { Theme: { Colors: { primary } } } }) => primary};
  }

  @media only screen and (min-width: 800px) {
    font-size: 1.5em;
  }
`;

const CloseContainer = styled.a`
  display: flex;
  flex-direction: row;
  position: absolute;
  top: 1em;
  left: 1em;
  text-decoration: none;
  align-items: center;
  color: inherit;
`;

const SidemenuContext = createContext({
  open: () => { },
});

export const useSidemenu = () => useContext(SidemenuContext);

const SidemenuProvider = ({
  children,
}) => {
  const {
    siteStorefront: [
      {
        Sidebar,
      }
    ],
  } = useStaticQuery(graphql`
    query {
      siteStorefront {
        Sidebar {
          id
          Text
          URL
        }
      }
    }  
  `);
  const [open, setOpen] = useState(false);
  const branding = useBranding();
  const location = useLocation();
  const isAuthenticated = useIsAuthenticated();

  useEffect(() => {
    setOpen(false);
  }, [location.href]);

  return (
    <SidemenuContext.Provider
      value={{
        open: () => setOpen(true),
      }}
    >
      {children}
      <MenuContainer open={open}>
        <CloseContainer
          href="/#"
          onClick={((e) => {
            e.preventDefault();
            e.stopPropagation();
            setOpen(false);
          })}
        >
          <FaTimes /> Close
        </CloseContainer>
        <div style={{ height: '100%', flex: 1 }}>
          {Sidebar.map(({ id, Text, URL }) => (
            <Anchor to={URL} key={id} branding={branding}>{Text}</Anchor>
          ))}
          {isAuthenticated ? (
            <Anchor to="/profile" branding={branding}>Account Details</Anchor>
          ) : null}
        </div>
      </MenuContainer>
    </SidemenuContext.Provider>
  );
};

export default SidemenuProvider;
