import moment from 'moment';
import React, { createContext, useContext, useEffect, useState } from 'react';
import api from '../../api';
import { TopBarContent } from '../../pages/messenger';
import DeliveryData from '../forms/delivery-data';
import { useBranding } from './branding-provider';
import { CartContext, useCart } from './cart-provider';
import { useMenu, MenuContext } from './menu-provider';
import { useNotifications } from './notification-provider';
import { useSession } from './session-state-provider';

const AvailabilityContext = createContext({
  isProductAvailable: (variationId) => true,
  isAddonAvailable: (id) => true,
});

export const useAvailability = () => useContext(AvailabilityContext);

/**
 * @type {React.FunctionComponent<{
*    allowBypass: boolean
 * }>}
 */
const AvailabilityProvider = ({
  allowBypass,
  children,
}) => {
  const cart = useCart();
  const menu = useMenu();
  const sessionState = useSession();
  const notifications = useNotifications();
  const branding = useBranding();

  const [editing, setEditing] = useState(false);

  const [deliveryData, setDeliveryData] = useState(() => {
    try {
      if (typeof (localStorage) !== 'undefined') {
        const deliveryData = localStorage.getItem('delivery-session');

        if (deliveryData) {
          const parsed = JSON.parse(deliveryData);

          return {
            ...parsed,
            delivery: {
              ...parsed.delivery,
              DeliveryDate: (parsed.delivery.DeliveryDate && moment(parsed.delivery.DeliveryDate)) || null,
            },
          };
        }
      }
    } catch (e) {
      console.error(e);
    }

    return null;
  });

  const [availabilityData, setAvailabilityData] = sessionState.use('availability');

  useEffect(() => {
    if (deliveryData) {
      localStorage.setItem('delivery-session', JSON.stringify(deliveryData));
    }
  }, [deliveryData]);

  useEffect(() => {
    let cancelled = false;

    if (deliveryData && !availabilityData) {
      (async () => {
        try {
          const availability = await api.checkAvailability({
            address: deliveryData.address,
            delivery: deliveryData.delivery,
          });

          if (!cancelled) {
            setAvailabilityData(availability);
          }
        } catch (e) {
          notifications.queue({
            type: 'error',
            title: 'Error',
            text: e.message,
          });
          setEditing(true);
        }
      })();
    }

    return () => {
      cancelled = true;
    };
  }, [deliveryData, availabilityData]);

  useEffect(() => {
    if (availabilityData) {
      let removed = false;

      cart.items.forEach((item) => {
        const variation = menu.Products.find(({ id }) => id == item.productId)?.Variations.find(({ id }) => id == item.variationId);

        if (!variation || (variation.POSCode && availabilityData.v.indexOf(Number(item.variationId)) === -1)) {
          cart.removeItem(item.key);
          removed = true;
        }

        item.addons.forEach((addon) => {
          if (addon.POSCode && availabilityData.a.indexOf(Number(addon)) === -1) {
            cart.updateItem(item.key, {
              addons: item.addons.filter((a) => a !== addon),
            });

            removed = true;
          }
        });
      });

      if (removed) {
        notifications.queue({
          type: 'error',
          title: 'Cart Updated',
          text: 'Some items were removed from your cart, as they are not available.',
        });
      }
    }
  }, [cart.items, availabilityData]);

  if ((!allowBypass || cart.items.length) && (!deliveryData || editing)) {
    return (
      <div
        style={branding.isMessenger ? {
          minHeight: '100%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          paddingLeft: '1em',
          paddingRight: '1em',
          paddingBottom: '1em',
          backgroundColor: 'white'
        } : {
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center'
        }}
      >
        <DeliveryData
          onSubmit={async (data) => {
            const availability = await api.checkAvailability({
              address: data.address,
              delivery: data.delivery,
            });
            setDeliveryData(data);
            setAvailabilityData(availability);
            setEditing(false);
          }}
          initialDeliveryInfo={deliveryData && deliveryData.delivery}
          initialAddress={deliveryData && deliveryData.address}
        />
      </div>
    );
  }

  const Products = menu.Products.map((product) => {
    if (!availabilityData) {
      if (allowBypass) {
        return product;
      }

      return null;
    }

    const availableVariations = product.Variations.filter(({ id, POSCode }) => (availabilityData.v.indexOf(Number(id)) > -1 || !POSCode));

    if (!availableVariations.length) {
      return null;
    }

    return {
      ...product,
      Variations: availableVariations.map((variation) => ({
        ...variation,
        Addons: variation.Addons.filter(({ id, POSCode }) => (availabilityData.a.indexOf(Number(id)) > -1 || !POSCode)),
      })),
    };
  }).filter(Boolean);

  const Container = branding.isMessenger ? TopBarContent : 'div';

  return (
    <MenuContext.Provider
      value={{
        ...menu,
        Products: Products,
      }}
    >
      <CartContext.Provider
        value={{
          ...cart,
          deliveryData,
          setDeliveryData,
          editDeliveryData: () => setEditing(true),
          clear: () => {
            cart.clear();
          },
          deliveryFee: availabilityData ? availabilityData.deliveryFee : 50,
        }}
      >
        {deliveryData ? (
          <Container>
            <div
              style={branding.isMessenger ? {
                padding: '0.5em',
              } : {
                padding: '1em 1.5em',
                backgroundColor: 'rgb(235, 235, 235)',
                marginBottom: '1em',
                fontSize: '1.15em',
              }}
            >
              {['DeliverNow', 'DeliverLater'].includes(deliveryData.delivery.DeliveryType) ? (
                <>
                  <b>Deliver To:</b>
                  {' '}
                  {[
                    deliveryData.address.CompanyName,
                    deliveryData.address.UnitNumber,
                    deliveryData.address.StreetAddress,
                    deliveryData.address.Barangay,
                    deliveryData.address.City,
                    deliveryData.address.Region,
                    deliveryData.address.ZipCode,
                  ].filter(Boolean).join(', ')}
                </>
              ) : null}
              {deliveryData.delivery.DeliveryType === 'Pickup' ? (
                <>
                  <b>Pickup At:</b>
                  {' '}
                  {deliveryData.address.PickupLocationAddress}
                </>
              ) : null}
              {['DeliverLater', 'Pickup'].includes(deliveryData.delivery.DeliveryType) ? (
                ` (on ${moment(deliveryData.delivery.DeliveryDate).format('LLL')})`
              ) : null}
              <a
                href="/edit-address"
                style={{
                  float: 'right',
                  textDecoration: 'none',
                  color: 'inherit',
                }}
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                  setEditing(true);
                }}
              >
                Edit
              </a>
            </div>
          </Container>
        ) : null}
        {!availabilityData && deliveryData ? (
          <div style={{ textAlign: 'center', marginTop: '3em' }}>
            Checking product availability...
          </div>
        ) : children}
      </CartContext.Provider>
    </MenuContext.Provider>
  );
};

export default AvailabilityProvider;
