import React, { createContext, useContext, useEffect, useState } from 'react';
import api from '../../api';

const UserContext = createContext({
  token: 'sometoken',
  userData: null,
  registerToken: (token, user = null) => { },
  updateUserData: (newData) => { },
  logOut: () => { },
});

export const useUser = () => useContext(UserContext);

/**
 * @type {React.FunctionComponent<{}>}
 */
const UserProvider = ({
  children,
}) => {
  const [
    {
      token = null,
      userData = null,
    },
    setData
  ] = useState(() => {
    if (typeof(localStorage) !== 'undefined') {
      const savedToken = localStorage.getItem('authToken');

      if (savedToken) {
        return {
          token: savedToken,
          userData: null,
        };
      }
    }

    return { };
  });

  useEffect(() => {
    if (token && !userData) {
      api.me({ token }).then((me) => {
        setData({
          token,
          userData: me,
        });
      }).catch((e) => {
        console.error(e);
        setData({ });

        if (typeof(localStorage) !== 'undefined' && e.message == 'Invalid token.') {
          localStorage.removeItem('authToken');
        }
      });
    }
  }, [token, userData]);

  return (
    <UserContext.Provider
      value={{
        token,
        userData,
        registerToken: (token, userData) => {
          setData({
            token,
            userData,
          });
          localStorage.setItem('authToken', token);
        },
        updateUserData: (newData) => {
          setData((d) => ({
            ...d,
            userData: newData,
          }));
        },
        logOut: () => {
          localStorage.removeItem('authToken');
          setData({ });
        },
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
