import { graphql, useStaticQuery } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import { FaChevronLeft } from '@react-icons/all-files/fa/FaChevronLeft';
import { FaChevronRight } from '@react-icons/all-files/fa/FaChevronRight';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Fragment } from 'react';
import { Colors } from '../branding';

const BannerContainer = styled.div`
  width: 100%;
  overflow: hidden;
`;

const BannersContainer = styled.div`
  width: ${({ count }) => count * 100}%;
  display: flex;
  flex-direction: row;
  transition: 0.5s;
  position: relative;
  left: -${({ index }) => (index * 100)}%;
`;

const Banner = styled.div`
  flex: 1;
`;

const BannerLink = styled.a`
  flex: 1;
`;

const ButtonContainer = styled.a`
  position: absolute;
  top: 0px;
  bottom: 0px;
  font-size: 2em;
  padding: 0.35em;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  transition: 0.5s;

  :hover {
    background-color: rgb(0, 0, 0, 0.25);
  }
`;

const IndicatorContainer = styled.div`
  padding: 0.5em;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Indicator = styled.div`
  width: 0.5em;
  height: 0.5em;
  border-radius: 1em;
  border: 0.1em solid ${['ww', 'an'].includes(process.env.GATSBY_SITE) ? (process.env.GATSBY_SITE === 'an' ? Colors.secondary : Colors.primary) : 'black'};
  ${({ active }) => active ? `background-color: ${['ww', 'an'].includes(process.env.GATSBY_SITE) ? (process.env.GATSBY_SITE === 'an' ? Colors.secondary : Colors.primary) : 'black'};` : ''}
  margin-left: 0.25em;
  margin-right: 0.25em;
`;

/**
 * @type {React.FunctionComponent<{
 * 
 * }>}
 */
const Banners = () => {
  const {
    siteStorefront: [
      {
        Banners
      }
    ],
  } = useStaticQuery(graphql`
    query {
      siteStorefront {
        Banners {
          AltText
          Image {
            childImageSharp {
              gatsbyImageData(
                layout: FULL_WIDTH
              )
            }
          }
          Link
        }
      }
    }
  `);

  const [index, setIndex] = useState(0);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setIndex((index) => ((index + 1) % Banners.length));
    }, 5000);

    return () => {
      clearTimeout(timeout);
    };
  });

  return (
    <BannerContainer>
      <div style={{ position: 'relative' }}>
        <BannersContainer count={Banners.length} index={index}>
          {Banners.map((banner) => {
            const bannerComponent = (
              <GatsbyImage
                image={getImage(banner.Image)}
                alt={banner.AltText}
              />
            );

            if (banner.Link) {
              return (
                <BannerLink href={banner.Link} rel="noreferer noopener" alt={banner.AltText} key={banner.id}>
                  {bannerComponent}
                </BannerLink>
              );
            }

            return (
              <Banner key={banner.id}>
                {bannerComponent}
              </Banner>
            );
          })}
        </BannersContainer>
        <ButtonContainer
          style={{ left: 0 }}
          href="/#"
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setIndex((index) => ((index - 1 + Banners.length) % Banners.length));
          }}
        >
          <FaChevronLeft />
        </ButtonContainer>
        <ButtonContainer
          style={{ right: 0 }}
          href="/#"
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setIndex((index) => ((index + 1) % Banners.length));
          }}
        >
          <FaChevronRight />
        </ButtonContainer>
      </div>
      <IndicatorContainer>
        {Banners.map((banner, i) => (
          <Indicator
            active={i === index}
            key={`indicator-${i}`}
          />
        ))}
      </IndicatorContainer>
    </BannerContainer>
  );
};

export default Banners;
