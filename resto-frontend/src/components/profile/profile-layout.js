import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import Layout from '../layout';
import MainContainer from '../main-container';
import { useUser } from '../providers/user-provider';
import UserGuard from '../user-guard';

const ProfilePageContainer = styled.div`
  display: flex;
  flex-direction: column;

  > nav {
    margin-top: 1em;
    margin-bottom: 1em;
    text-align: center;
    flex: 1;
  }

  @media only screen and (min-width: 800px) {
    flex-direction: row;

    > nav {
      margin-right: 1em;
      text-align: left;
    }
    
    > div {
      flex: 3;
    }
  }
`;

const NavigationLink = styled(Link)`
  display: block;
  font-size: 1.25em;
  margin-bottom: 0.5em;
`;

const ProfileLayout = ({
  page,
  children,
  ...props
}) => {
  const user = useUser();

  return (
    <Layout {...props}>
      <UserGuard>
        <MainContainer>
          <ProfilePageContainer>
            <nav>
              <NavigationLink to="/profile">Account Details</NavigationLink>
              <NavigationLink to="/addresses">Saved Addresses</NavigationLink>
              <NavigationLink to="/orders">Orders</NavigationLink>
              <NavigationLink
                to="/"
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                  user.logOut();
                }}
              >
                Logout
              </NavigationLink>
            </nav>
            <div>
              {children}
            </div>
          </ProfilePageContainer>
        </MainContainer>
      </UserGuard>
    </Layout>
  );
};

export default ProfileLayout;
