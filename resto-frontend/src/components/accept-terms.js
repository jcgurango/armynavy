import { Link } from 'gatsby';
import React from 'react';

const AcceptTerms = ({
  onChange,
  value,
  hasPwd,
}) => {
  return (
    <>
      <p>
        Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our <a href="/privacy-notice" target="_blank">Privacy Notice</a>.
      </p>
      {hasPwd ? (
        <p>
          Please note that an agent will contact you to confirm your PWD/Senior Citizen discount and inform you of your total discounted bill field.
        </p>
      ) : null}
      <small>
        <label className="label-required">
          <input type="checkbox" value={value} onChange={(e) => onChange(e.target.checked)} style={{ marginRight: '0.5em', position: 'relative', top: '1px' }} />
          I have read and agree to the website <a href="/terms-of-use" target="_blank">Terms and Conditions</a>.
        </label>
      </small>
    </>
  );
};

export default AcceptTerms;
