import { graphql, Link, useStaticQuery } from 'gatsby';
import React, { useEffect, useMemo } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import { Helmet } from 'react-helmet';

import { useCart } from './providers/cart-provider';
import { useSidemenu } from './providers/sidemenu-provider';
import '../css/fonts.css';
import Footer from './footer';
import { useNotifications } from './providers/notification-provider';
import SiteConstants from '../site-constants';

export const GlobalStyle = createGlobalStyle`
  html, body {
    font-family: 'Arial', 'Helvetica', sans-serif;
    padding: 0px;
    margin: 0px;
    font-size: 18px;
  }

  #___gatsby {
    min-height: 100vh;

    > #gatsby-focus-wrapper {
      min-height: 100vh;

      display: flex;
      flex-direction: column;
    }
  }

  @media only screen and (min-width: 800px) {
    html, body {
      font-size: 16px;
    }
  }

  h1, h2, h3, h4, h5, h6 {
    margin: 0;
    margin-bottom: 0.25em;
  }

  h1 {
    font-size: 1.5em;
  }

  h2 {
    font-size: 1.3em;
  }

  h3 {
    font-size: 1.1em;
  }

  h4 {
    font-size: 1em;
  }

  .label-required:after {
    content: " *";
    color: red;
  }

  a {
    text-decoration: none;
    color: rgb(38, 160, 222);
    -webkit-tap-highlight-color: transparent;
  }

  table {
    width: 100%;
  }

  th, td {
    text-align: center;
  }
`;

const Header = styled.div`
`;

const HeaderInner = styled.div`
  padding: 1.5em;
  max-width: 1200px;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  .nav {
    display: flex;
    flex-direction: row;
  }
`;

const HeaderLink = styled(Link)`
  ${({ logo }) => logo ? 'margin-right' : 'margin-left'}: 1em;
  display: inline-block;
  position: relative;
`;

const HamburgerMenu = styled.a`
  margin-left: 1em;
`;

const Image = styled(GatsbyImage)`
  filter: drop-shadow(0px 0px 0.1em rgba(0, 0, 0, 0.25));
`;

/**
 * @type {React.FunctionComponent<{
 *  title: string,
 *  invert: boolean,
 * }>}
 */
const Layout = ({
  title,
  metaTitle,
  metaDescription,
  children,
  invert,
}) => {
  const {
    siteStorefront: [storefront],
  } = useStaticQuery(graphql`
    query {
      siteStorefront {
        Name
        Logo {
          childImageSharp {
            gatsbyImageData(height: 42)
            favicon: gatsbyImageData(width: 32)
          }
        },
        PtLogo: Logo {
          childImageSharp {
            gatsbyImageData(height: 50)
            favicon: gatsbyImageData(width: 32)
          }
        },
        Cart {
          childImageSharp {
            gatsbyImageData(height: 32)
          }
        },
        Profile {
          childImageSharp {
            gatsbyImageData(height: 32)
          }
        },
        Hamburger {
          childImageSharp {
            gatsbyImageData(height: 32)
          }
        }
        UnderMaintenance
        Unavailable {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH)
          }
        }
        MetaTitle
        MetaDescription
      }
    }
  `);

  const cart = useCart();
  const sidemenu = useSidemenu();
  const notifications = useNotifications();

  const inCart = useMemo(() => {
    let sum = 0;

    cart.items.forEach(({ quantity }) => {
      sum += quantity;
    });

    return sum;
  }, [cart.items]);

  useEffect(() => {
    if (typeof (localStorage) !== 'undefined' && !localStorage.getItem('accepted-cookies')) {
      setTimeout(() => {
        notifications.queue({
          type: 'info',
          title: 'Cookie Notice',
          text: 'We use cookies to ensure you get the best experience on our site. By continued use, you agree to our Privacy Notice and accept our use of such cookies.',
          time: 10000,
        });

        localStorage.setItem('accepted-cookies', 1);
      }, 1000);
    }
  }, [notifications]);

  if (storefront.UnderMaintenance) {
    return (
      <>
        <Helmet>
          <title>Under Maintenance</title>
        </Helmet>
        <GatsbyImage
          image={getImage(storefront.Unavailable)}
          alt="Under Maintenance"
        />
      </>
    );
  }

  return (
    <>
      <Helmet>
        <title>{title ? `${title} - ` : ''}{SiteConstants.websiteTitle || storefront.Name}</title>
        <meta name="title" content={metaTitle || storefront.MetaTitle} />
        <meta name="description" content={metaDescription || storefront.MetaDescription} />
      </Helmet>
      <Header style={invert ? { backgroundColor: '#3a3935' } : undefined}>
        <HeaderInner>
          <HeaderLink to="/" logo>
            {process.env.GATSBY_SITE === 'pt' ? (
              <GatsbyImage
                image={getImage(storefront.PtLogo)}
                alt={storefront.Name}
              />
            ) : (
              <GatsbyImage
                image={getImage(storefront.Logo)}
                alt={storefront.Name}
              />
            )}
          </HeaderLink>
          <div className="nav">
            <HeaderLink to="/cart">
              <Image
                image={getImage(storefront.Cart)}
                alt="Cart"
                style={{ filter: invert ? 'invert(1)' : undefined }}
              />
              {inCart > 0 ? (
                <div
                  style={{
                    position: 'absolute',
                    top: -8,
                    right: -8,
                    backgroundColor: 'rgb(237, 111, 35)',
                    borderRadius: '2em',
                    color: 'white',
                    fontSize: '0.75em',
                    width: '1.5em',
                    height: '1.5em',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  {inCart}
                </div>
              ) : null}
            </HeaderLink>
            <HeaderLink to="/profile">
              <Image
                image={getImage(storefront.Profile)}
                alt="Profile"
                style={{ filter: invert ? 'invert(1)' : undefined }}
              />
            </HeaderLink>
            <HamburgerMenu
              title="Menu"
              href="/#"
              onClick={(e) => {
                e.stopPropagation();
                e.preventDefault();
                sidemenu.open();
              }}
            >
              <Image
                image={getImage(storefront.Hamburger)}
                alt="Menu"
                style={{ filter: invert ? 'invert(1)' : undefined }}
              />
            </HamburgerMenu>
          </div>
        </HeaderInner>
      </Header>
      <GlobalStyle />
      <div style={{ flex: 1, display: 'flex', flexDirection: 'column', backgroundColor: invert ? '#3a3935' : undefined, paddingBottom: '2em' }}>
        {children}
      </div>
      <Footer />
    </>
  );
};

export default Layout;
