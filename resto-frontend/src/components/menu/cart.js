import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import React from 'react';
import styled from 'styled-components';

import { useBranding } from '../providers/branding-provider';
import { useCart } from '../providers/cart-provider';
import { useMenu } from '../providers/menu-provider';
import QuantitySelector from './quantity-selector';

const ItemsContainer = styled.div`
  flex: 2;
`;

const ItemContainer = styled.div`
  padding: 0.75em;
  border-bottom: 0.1em solid ${({ branding: { Theme: { Colors } } }) => Colors.primary};
  display: flex;
  flex-direction: row;
`;

const ItemImageContainer = styled.div`
  flex: 1;
`;

const ItemDescriptionContainer = styled.div`
  margin-left: 0.5em;
  flex: 4;
`;

const ItemSubtotalContainer = styled.div`
  flex: 1;
  text-align: right;
`;

/**
 * @type {React.FunctionComponent<{
 *  editable: Boolean,
 * }>}
 */
const Cart = ({
  editable = false,
  children,
}) => {
  const menu = useMenu();
  const cart = useCart();
  const branding = useBranding();

  return (
    <ItemsContainer>
      {cart.items.map((item) => {
        const {
          productId,
          addons,
          options,
        } = item;
        const product = menu.Products.find(({ id }) => Number(productId) === Number(id));

        if (!product) {
          return null;
        }

        const variation = product.Variations.find(({ id }) => Number(id) === Number(item.variationId));

        if (!variation) {
          return null;
        }

        return (
          <ItemContainer branding={branding} key={item.key}>
            <ItemImageContainer>
              <GatsbyImage
                image={getImage(variation.Image || product.Variations[0].Image || product.Image)}
                alt={product.Name}
                style={process.env.GATSBY_SITE === 'ww' ? {
                  borderRadius: '10em',
                } : undefined}
              />
            </ItemImageContainer>
            <ItemDescriptionContainer>
              <b>{product.Name}{product.Variations.length > 1 ? ` (${variation.Text})` : ''}</b><br />
              {Object.keys(options).map((optionSetId) => {
                const optionSet = variation.SelectableOptions.find(({ id }) => id === Number(optionSetId));

                return Object.values(options[optionSetId]).map((optionId, i) => {
                  const option = optionSet.Options.find(({ id }) => id === Number(optionId));

                  return (
                    <>
                      <div key={option.id}>
                          {option.Name}
                      </div>
                    </>
                  );
                });
              })}
              {addons.map((aid) => {
                const addon = (variation.Addons || []).find(({ id }) => id === aid);

                return (
                  <div key={aid}>
                    {addon.Category ? `${addon.Category} - ` : ''}{addon.Name}
                  </div>
                );
              })}
              {!editable ? (
                <>
                  x {item.quantity}
                </>
              ) : null}
              {item.specialInstructions ? (
                <div style={{ marginTop: '0.5em' }}>
                  <b>Special Instructions</b><br />
                  {item.specialInstructions}
                </div>
              ) : null}
            </ItemDescriptionContainer>
            <ItemSubtotalContainer>
              ₱{cart.calculateSubtotal(item).toLocaleString()}
              {(editable) ? (
                <QuantitySelector
                  value={item.quantity}
                  onChange={(quantity) => {
                    cart.updateQuantity(item.key, quantity);
                  }}
                  onDelete={() => {
                    cart.removeItem(item.key);
                  }}
                  isVoucher={item.voucher}
                />
              ) : null}
            </ItemSubtotalContainer>
          </ItemContainer>
        );
      })}
      {children}
    </ItemsContainer>
  );
};

export default Cart;
