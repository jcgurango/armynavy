import React, { useEffect, useMemo, useRef, useState } from 'react';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import styled from 'styled-components';
import { useMenu } from '../providers/menu-provider';
import Button from '../button';
import Product from './product';
import { Colors } from '../../branding';

const wwCustomRow = `
  background-color: ${Colors.primary};

  div.details {
    color: white !important;
  }

  a {
    background-color: white !important;
    color: ${Colors.primary} !important;
  }
`;

const ProductsGrid = styled.div`
  display: grid;
  gap: 0.5em 0.5em;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: ". .";

  @media only screen and (max-width: 940px) and (min-width: 460px) {
    ${() => process.env.GATSBY_SITE === 'ww' ? `
      > div.product-container:nth-child(4n + 3), > div.product-container:nth-child(4n + 4) {
        ${wwCustomRow}
      }

      > div:nth-child(2n + 0) + div.empty {
        display: none;
      }

      > div:nth-child(2n + 0) + div.empty + div.empty {
        display: none;
      }
    ` : ''}
  }

  @media only screen and (max-width: 460px) {
    grid-template-columns: 1fr;
    grid-template-areas: ".";

    ${() => process.env.GATSBY_SITE === 'ww' ? `
      > div.product-container:nth-child(2n) {
        ${wwCustomRow}
      }

      div.empty {
        display: none;
      }
    ` : ''}
  }

  @media only screen and (min-width: 940px) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-template-areas: ". . . .";

    ${() => ['an', 'pt'].includes(process.env.GATSBY_SITE) ? `
      grid-template-columns: 1fr 1fr 1fr;
      grid-template-areas: ". . .";
    ` : ''}

    ${() => process.env.GATSBY_SITE === 'ww' ? `
      grid-template-columns: 1fr 1fr 1fr;
      grid-template-areas: ". . .";

      > div.product-container:nth-child(6n + 4), > div.product-container:nth-child(6n + 5), > div.product-container:nth-child(6n + 6) {
        ${wwCustomRow}
      }

      > div:nth-child(3n + 0) + div.empty {
        display: none;
      }

      > div:nth-child(3n + 0) + div.empty + div.empty {
        display: none;
      }
    ` : ''}
  }

  ${() => process.env.GATSBY_SITE === 'ww' ? `
    gap: 0em 0em;
    border: 2px solid ${Colors.primary};
    border-bottom-left-radius: 1em;
    border-bottom-right-radius: 1em;
    overflow: hidden;
  ` : ''}
`;

const ProductContainer = styled.div`
  text-align: center;
  height: 100%;
  display: flex;
  flex-direction: column;

  div.details {
    margin-top: 0.75em;
    margin-bottom: 0.75em;
    text-align: left;

    b {
      font-size: 1.1em;
    }
  }

  div.button-container {
    text-align: left;
  }

  picture, img {
    ${() => process.env.GATSBY_SITE === 'ltmt' ? 'height: 17em;' : ''}
    ${() => (process.env.GATSBY_SITE === 'rr') ? `
      height: 9em;

      @media only screen and (min-width: 450px) {
        height: 12em;
      }
      
      @media only screen and (min-width: 700px) {
        height: 16em;
      }
      
      @media only screen and (min-width: 940px) {
        height: 12em;
      }
    ` : ''}

    ${() => process.env.GATSBY_SITE === 'ww' ? `
      border-radius: 32em;
      height: 10em;
    ` : ''}
  }

  ${() => process.env.GATSBY_SITE === 'rr' ? `
    .product-name, .price {
      color: #6c6cac;
    }
  ` : ''}

  ${() => process.env.GATSBY_SITE === 'ww' ? `
    div.details, div.button-container {
      font-size: 1.25em;
      text-align: center;

      a {
        color: white;
      }

      .price {
        color: ${Colors.secondary};
        font-weight: 400;
      }
    }

    box-sizing: border-box;
    padding: 0.5em;
    margin-right: -1px;
    
      
    @media only screen and (min-width: 700px) {
      padding: 1em;
    }
  ` : ''}

  ${() => process.env.GATSBY_SITE === 'an' ? `
    div.details, div.button-container {
      .product-name {
        color: rgb(64, 64, 64);
        font-size: 1.35em;
      }

      .price {
        font-weight: bold;
      }
    }
  ` : ''}
`;

const SquareContainer = ({
  children,
}) => {
  const [height, setHeight] = useState(undefined);
  const ref = useRef();

  useEffect(() => {
    const callback = () => {
      if (ref.current) {
        setHeight(ref.current.clientHeight);
      }
    };

    window.addEventListener('resize', callback);
    callback();

    return () => {
      window.removeEventListener('resize', callback);
    };
  }, [ref]);
  
  if (process.env.GATSBY_SITE === 'an') {
    return (
      <div style={{ position: 'relative' }}>
        <div style={{ paddingTop: '100%' }}>
        </div>
        <div style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          {children}
        </div>
      </div>
    );
  }

  if (process.env.GATSBY_SITE !== 'ww') {
    return (
      <div>
        {children}
      </div>
    );
  }

  return (
    <div
      ref={ref}
      style={{
        marginLeft: 'auto',
        marginRight: 'auto',
        width: height ? height : undefined,
      }}
    >
      {children}
    </div>
  );
};

/**
 * @type {React.FunctionComponent<{
 *  categoryId: String,
 *  categorySlug: String,
 *  productSlug: String,
 * }>}
 */
const ProductsList = ({
  categoryId,
  categorySlug,
  productSlug,
}) => {
  const { Products, Categories } = useMenu();
  const filteredProducts = useMemo(() => {
    return Products.filter(({ Categories }) => {
      return Boolean(Categories.find(({ id }) => id === categoryId));
    });
  }, [Products, categoryId]);
  const selectedProduct = filteredProducts.find(({ Slug }) => Slug === productSlug);
  const category = Categories.find(({ strapiId }) => Number(strapiId) === Number(categoryId));
  const detailsStyle = {};
  const buttonStyle = { fontSize: '0.8em', fontWeight: 'bold' };

  if (process.env.GATSBY_SITE !== 'ltmt') {
    detailsStyle.color = Colors.primary;
    buttonStyle.backgroundColor = Colors.primary;
  }

  if (process.env.GATSBY_SITE === 'rr') {
    buttonStyle.backgroundColor = '#6c6cac';
  }

  if (process.env.GATSBY_SITE === 'an') {
    buttonStyle.width = '100%';
    buttonStyle.fontSize = '1em';
    buttonStyle.borderRadius = '2em';
  }

  return (
    <ProductsGrid>
      {filteredProducts.map(({ id, Name, Variations, Slug }) => (
        <ProductContainer key={id} className="product-container">
          <div style={{ flex: 1 }}>
            <SquareContainer key={id + '-image'}>
              <GatsbyImage
                image={getImage(Variations[0]?.Image)}
                alt={Name}
              />
            </SquareContainer>
            <div
              className="details"
              style={detailsStyle}
            >
              <b className="product-name">
                {Name}
              </b>
              <br />
              <span class="price">₱{Variations[0] && Number(Variations[0]?.Price).toLocaleString()}</span>
            </div>
          </div>
          <div class="button-container">
            <Button
              buttonType="link"
              to={`/menu/${categorySlug}/products/${Slug}`}
              style={buttonStyle}
            >
              Order Now
            </Button>
          </div>
        </ProductContainer>
      ))}
      {process.env.GATSBY_SITE === 'ww' ? (
        <>
          <ProductContainer className="empty product-container" />
          <ProductContainer className="empty product-container" />
        </>
      ) : null}
      {selectedProduct ? (
        <Product
          key={selectedProduct.id}
          product={selectedProduct}
          returnUrl={`/menu/${categorySlug}`}
          category={category}
        />
      ) : null}
    </ProductsGrid>
  );
};

export default ProductsList;
