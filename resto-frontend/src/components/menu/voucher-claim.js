import React, { useState } from 'react';
import TextInput from '../text-input';
import Button from '../button';
import api from '../../api';
import { useNotifications } from '../providers/notification-provider';
import { useCart } from '../providers/cart-provider';

const VoucherClaim = ({
  product,
  claimedVoucher,
  onVoucherClaimed = (voucher) => { },
}) => {
  const [claimingVoucher, setClaimingVoucher] = useState(false);
  const [voucherInput, setVoucherInput] = useState('');
  const notifications = useNotifications();
  const cart = useCart();

  return (
    <div style={{ marginTop: '1em', display: 'flex', flexDirection: 'row' }}>
      <div style={{ flex: 1, marginRight: '1em' }}>
        <TextInput
          placeholder="Enter voucher to claim"
          value={voucherInput}
          onChange={setVoucherInput}
        />
      </div>
      <Button
        disabled={claimingVoucher || claimedVoucher || (cart.items.find(({ voucher }) => voucher === voucherInput))}
        onClick={async () => {
          setClaimingVoucher(true);
          const { result } = await api.claim(product.id, voucherInput);
          
          if (result === 'notFound') {
            notifications.queue({
              title: 'Error',
              text: 'Voucher does not exist or has already been claimed.',
              type: 'error',
            });

            setClaimingVoucher(false);
          } else {
            notifications.queue({
              title: 'Claimed',
              text: 'Voucher claimed.',
              type: 'info',
            });

            onVoucherClaimed(voucherInput);
          }
        }}
      >
        {claimingVoucher ? (claimedVoucher ? 'Claimed' : 'Claiming') : 'Claim'}
      </Button>
    </div>
  );
};

export default VoucherClaim;
