import React from 'react';
import styled from 'styled-components';
import { FaChevronRight } from '@react-icons/all-files/fa/FaChevronRight';
import { Link } from 'gatsby';
import { useMenu } from '../providers/menu-provider';
import { useBranding } from '../providers/branding-provider';
import { Colors as BrandColors } from '../../branding';

const CategoriesContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const CategoryLink = styled(Link)`
  padding: 0.5em;
  color: inherit;
  text-decoration: none;
  margin-bottom: 0.25em;
  border: 0.15em solid transparent;
  ${({ active, color }) => (active ? `border: 0.15em solid ${color};` : '')}
  ${({ active }) => (process.env.GATSBY_SITE === 'rr') ? (`
    border: 0.1em solid #5bbcad;
    color: #5bbcad;
    font-weight: bold;
    text-align: center;
    font-size: 1.65em;
    padding: 0.25em;
    margin-bottom: 1em;

    ${active ? (`
      border-color: #6c6cac;
      background-color: #6c6cac;
      color: white;
    `) : ''}
  `) : ''}
  ${({ active }) => (process.env.GATSBY_SITE === 'ww') ? (`
    border: 1px solid ${BrandColors.primary};
    color: ${BrandColors.primary};
    font-weight: bold;
    text-align: center;
    font-size: 1.65em;
    padding: 0.25em;
    margin-bottom: 1em;

    ${active ? (`
      border: 0;
      background-color: ${BrandColors.primary};
      color: white;
    `) : ''}
  `) : ''}
  border-radius: 0.5em;
  ${() => ((process.env.GATSBY_SITE === 'rr' || process.env.GATSBY_SITE === 'ww') ? `
    @media only screen and (min-width: 800px) {
      svg {
        display: none;
      }
    }
  ` : '')}
  ${({ active }) => ((process.env.GATSBY_SITE === 'an') ? `
    border-radius: 0;
    border: 0;
    font-size: 1.25em;
    padding: 1em;
    font-weight: bold;

    ${active ? `
      background-color: ${BrandColors.secondary};
      color: white;
    ` : ''}
  ` : '')}

  ${({ active }) => process.env.GATSBY_SITE === 'pt' ? `
    color: white;
    font-size: 1.15em;
    border-radius: 2em;
    padding: 0.7em 2em;

    ${active ? `
      color: #3a3935;
      background-color: ${BrandColors.primary};
      font-weight: bold;
    ` : ''}
  ` : ''}
`;

/**
 * @type {React.FunctionComponent<{
 *  currentCategory?: String
 * }>}
 */
const MenuList = ({
  currentCategory = null,
}) => {
  const {
    Categories,
  } = useMenu();
  const branding = useBranding();

  return (
    <CategoriesContainer>
      {Categories.map(({ id, Category, Slug, Metadata: { color } = { } }, i) => {
        const active = (!currentCategory && !i) || Slug === currentCategory;

        return (
          <CategoryLink
            key={id}
            to={`/menu/${Slug}`}
            active={active}
            color={color}
            branding={branding}
          >
            {Category}
            {process.env.GATSBY_SITE === 'ltmt' ? (
              <FaChevronRight
                style={{ float: 'right', color, fontSize: active ? null : '0.7em', opacity: active ? null : '0.65' }}
              />
            ) : null}
            {(process.env.GATSBY_SITE === 'rr' || process.env.GATSBY_SITE === 'ww' || process.env.GATSBY_SITE === 'an') ? (
              <FaChevronRight
                style={{ float: 'right', marginTop: '0.05em' }}
              />
            ) : null}
          </CategoryLink>
        );
      })}
    </CategoriesContainer>
  );
};

export default MenuList;
