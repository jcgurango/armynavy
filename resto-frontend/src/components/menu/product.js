import React, { useEffect, useMemo, useState } from 'react';
import { Link, navigate } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import styled from 'styled-components';
import { FaTimes } from '@react-icons/all-files/fa/FaTimes';
import BrandingProvider, { useBranding } from '../providers/branding-provider';
import TextInput from '../text-input';
import Button from '../button';
import { useCart } from '../providers/cart-provider';
import { useNotifications } from '../providers/notification-provider';
import Pills from '../pills';
import QuantitySelector from './quantity-selector';
import { Colors } from '../../branding';
import VoucherClaim from './voucher-claim';

const ProductModal = styled.div`
  justify-content: center;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(0, 0, 0, 0.25);
  color: ${({ textColor }) => textColor};
  overflow: auto;
  z-index: 1000;

  div.product-container {
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: 800px;
    background-color: ${({ color }) => color};
    padding: 1em;
    box-sizing: border-box;
  }

  ${process.env.GATSBY_SITE === 'an' ? `
    .price {
      color: ${Colors.primary};
    }
  ` : ''}

  @media only screen and (max-width: 800px) {
    .close-button {
      position: absolute;
      right: 0px;
      top: 0px;
    }
  }

  @media only screen and (min-width: 800px) {
    display: flex;
    flex-direction: row;
    padding: 2em;

    div.product-container {
      flex-direction: row;
      padding: 5em;
      box-sizing: border-box;
      margin-bottom: 2em;
    }

    > div {
      display: flex;
      flex-direction: row;
      align-items: flex-start;
      margin: 1em;
    }

    .details {
      margin-left: 2em;
    }
  }
`;

const CloseButton = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgb(75, 75, 75);
  color: white;
  border-radius: 1.5em;
  width: 1.5em;
  height: 1.5em;
  
  @media only screen and (min-width: 800px) {
    margin-left: -0.75em;
    margin-top: -0.75em;
  }
`;

const Checkbox = styled.input`
  width: 1.25em;
  height: 1.25em;
  appearance: none;
  background-color: transparent;
  border: 1px solid rgba(125, 125, 125, 0.55);
  border-radius: 0.25em;

  &:checked {
    background-color: blue;
    display: flex;
    align-items: center;
    justify-content: center;

    &:after {
      content: '✓';
      color: white;
    }
  }
`;

/**
 * @type {React.FunctionComponent<{
 *  product: import('../providers/menu-provider').TestProduct,
 *  returnUrl: String,
 * }>}
 */
const Product = ({
  product,
  category,
  returnUrl,
  inline,
  onAdd = () => { },
}) => {
  const cart = useCart();
  const notifications = useNotifications();
  const [variationIndex, setVariationIndex] = useState(0);
  const [specialInstructions, setSpecialInstructions] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [addons, setAddons] = useState([]);
  const [options, setOptions] = useState({});
  const [claimedVoucher, setClaimedVoucher] = useState(null);

  const newItem = useMemo(() => {
    return {
      productId: product.id,
      variationId: product.Variations[variationIndex].id,
      addons,
      options,
      specialInstructions,
      quantity,
      voucher: claimedVoucher,
    };
  }, [product, addons, options, specialInstructions, variationIndex, quantity, claimedVoucher]);

  const branding = useBranding();
  const sum = useMemo(() => cart.calculateSubtotal(newItem), [cart, newItem]);
  const requiredOptions = product?.Variations[variationIndex]?.SelectableOptions?.reduce((prev, { Count }) => (prev + Count), 0);
  const selectedOptions = Object.keys(options).reduce((last, next) => last + Object.values(options[next]).length, 0);

  useEffect(() => {
    setAddons([]);
    setOptions({});
  }, [variationIndex]);

  let color = branding.Theme.Colors.primary;
  let textColor = branding.Theme.Colors.darkText;
  let buttonColor = category.Metadata?.buttonColor || 'white';
  let activeVariationTextColor = '';

  if (process.env.GATSBY_SITE === 'ww') {
    color = Colors.secondary;
    textColor = Colors.primary;
    activeVariationTextColor = 'white';
  }

  if (process.env.GATSBY_SITE === 'rr') {
    color = '#5bbcad';
    buttonColor = '#6c6cac';
  }

  if (['an', 'pt'].includes(process.env.GATSBY_SITE)) {
    color = 'white';
    buttonColor = Colors.primary;
  }

  let addonCategories = [];

  if (product.Variations[variationIndex].Addons) {
    product.Variations[variationIndex].Addons.forEach(({ Category }) => {
      if (!addonCategories.includes(Category)) {
        addonCategories.push(Category);
      }
    });
  }

  const content = (
    <div className="product-container">
      <div style={{ flex: 2 }}>
        <GatsbyImage
          image={getImage(product.Variations[variationIndex].Image || product.Variations[0].Image || product.Image)}
          alt={product.Name}
        />
      </div>
      <div style={{ flex: 4 }} className="details">
        <h2 style={{ margin: 0 }}>{product.Name}</h2>
        <h3 style={{ margin: 0, fontWeight: 'normal', marginTop: '0.25em', marginBottom: '0.35em' }}>
          <span className="price">₱{Number(sum).toLocaleString()}</span>
        </h3>
        <div style={{ whiteSpace: 'pre-wrap' }}>{String(product.Description || '').trim()}</div>
        {product.Voucher ? (
          <VoucherClaim
            product={product}
            claimedVoucher={claimedVoucher}
            onVoucherClaimed={setClaimedVoucher}
          />
        ) : null}
        {(product.Variations.length > 1) ? (
          <Pills style={{ marginTop: '2em', marginBottom: '2em' }} activeColor={buttonColor} activeTextColor={activeVariationTextColor}>
            {product.Variations.map((variation, index) => (
              <a
                key={variation.id}
                className={index === variationIndex ? 'active' : ''}
                href="/#"
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                  setVariationIndex(index);
                }}
              >
                {variation.Text}
              </a>
            ))}
          </Pills>
        ) : null}
        {(product.Variations[variationIndex].SelectableOptions?.length) ? (
          product.Variations[variationIndex].SelectableOptions.map((optionSet) => {
            return (new Array(optionSet.Count)).fill(null).map((n, i) => (
              <div style={{ marginTop: '2em' }} key={`${optionSet.id}-${i}`}>
                <b>{optionSet.Name} {optionSet.Count > 1 ? (i + 1) : null}</b>
                {optionSet.Options.map((option) => (
                  <div key={String(option.id)} style={{ marginTop: '0.65em' }}>
                    <label style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                      <Checkbox
                        type="radio"
                        name={`${optionSet.id}-${i}`}
                        checked={options[optionSet.id]?.[i] === option.id}
                        onChange={(e) => {
                          if (e.target.checked) {
                            setOptions((options) => ({
                              ...options,
                              [optionSet.id]: {
                                ...options[optionSet.id],
                                [i]: option.id,
                              },
                            }));
                          }
                        }}
                      />
                      <span>
                        {option.Name} {option.Price ? <>(+<span className="price">₱{Number(option.Price).toLocaleString()}</span>)</> : null}
                      </span>
                    </label>
                  </div>
                ))}
              </div>
            ));
          })
        ) : null}
        {(product.Variations[variationIndex].Addons && product.Variations[variationIndex].Addons.length) ? (
          addonCategories.map((addonCategory) => {
            return (
              <div style={{ marginTop: '2em' }} key={addonCategory}>
                <b>{addonCategory || 'Add-ons'}</b>
                {product.Variations[variationIndex].Addons.filter(({ Category }) => (!Category && !addonCategory) || addonCategory === Category).map((addon) => (
                  <div key={String(addon.id)} style={{ marginTop: '0.65em' }}>
                    <label style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                      <Checkbox
                        type="checkbox"
                        checked={addons.indexOf(addon.id) > -1}
                        onChange={(e) => {
                          if (e.target.checked) {
                            setAddons((addons) => addons.concat(addon.id).sort());
                          } else {
                            setAddons((addons) => addons.filter((id) => id !== addon.id).sort());
                          }
                        }}
                      />
                      <span>
                        {addon.Name} (+<span className="price">₱{Number(addon.Price).toLocaleString()}</span>)
                      </span>
                    </label>
                  </div>
                ))}
              </div>
            );
          })
        ) : null}
        {(!['an', 'pt'].includes(process.env.GATSBY_SITE) && product.Variations[variationIndex].FixedOptions && product.Variations[variationIndex].FixedOptions.length) ? (
          <>
            <br />
            {product.Variations[variationIndex].FixedOptions.map((addon) => (
              <div key={String(addon.id)} style={{ marginTop: '0.65em' }}>
                <label style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                  <Checkbox
                    type="checkbox"
                    checked
                  />
                  <span>
                    {addon.Name}
                    {addon.Price ? <>(+<span className="price">₱{Number(addon.Price).toLocaleString()}</span>)</>
                      : null}
                  </span>
                </label>
              </div>
            ))}
          </>
        ) : null}
        <div style={{ marginTop: '2em' }}>
          <div style={{ marginBottom: '1em' }}>
            <b>Special Instructions</b>
          </div>
          <TextInput
            multiline
            value={specialInstructions}
            onChange={setSpecialInstructions}
            style={{ backgroundColor: 'transparent', borderColor: process.env.GATSBY_SITE === 'rr' ? 'white' : null, color: ['rr', 'ww'].includes(process.env.GATSBY_SITE) ? 'white' : null }}
            maxLength={256}
          />
        </div>
        {['an', 'pt'].includes(process.env.GATSBY_SITE) ? (
          <>
            <div style={{ borderTop: '1px solid rgba(125, 125, 125, 0.25)', borderBottom: '1px solid rgba(125, 125, 125, 0.25)', padding: '0.5em' }}>
              <div style={{ display: 'flex', flexDirection: 'row', fontWeight: 'bold' }}>
                <div style={{ flex: 1 }}>
                  {quantity}x {product.Name}
                </div>
                <div>
                  ₱{Number(product.Variations[variationIndex].Price * quantity).toLocaleString()}
                </div>
              </div>
              {Object.keys(options).map((optionSetId) => {
                const optionSet = product.Variations[variationIndex].SelectableOptions.find(({ id }) => id === Number(optionSetId));

                return Object.values(options[optionSetId]).map((optionId, i) => {
                  const option = optionSet.Options.find(({ id }) => id === Number(optionId));

                  return (
                    <>
                      <div style={{ display: 'flex', flexDirection: 'row', marginLeft: '1em', marginTop: '0.5em' }} key={option.id}>
                        <div style={{ flex: 1 }}>
                          {option.Name}
                        </div>
                        <div>
                          ₱{Number(option.Price * quantity).toLocaleString()}
                        </div>
                      </div>
                    </>
                  );
                });
              })}
              {(product.Variations[variationIndex].FixedOptions && product.Variations[variationIndex].FixedOptions.length) ? product.Variations[variationIndex].FixedOptions.map((addon) => (
                <>
                  <div style={{ display: 'flex', flexDirection: 'row', marginLeft: '1em', marginTop: '0.5em' }} key={addon.id}>
                    <div style={{ flex: 1 }}>
                      {addon.Name}
                    </div>
                    <div>
                      ₱{Number(addon.Price * quantity).toLocaleString()}
                    </div>
                  </div>
                </>
              )) : null}
              {(product.Variations[variationIndex].Addons && product.Variations[variationIndex].Addons.length) ? product.Variations[variationIndex].Addons.filter((addon) => addons.indexOf(addon.id) > -1).map((addon) => (
                <>
                  <div style={{ display: 'flex', flexDirection: 'row', marginLeft: '1em', marginTop: '0.5em' }} key={addon.id}>
                    <div style={{ flex: 1 }}>
                      Add-ons -{addon.Category ? ` ${addon.Category} -` : ''} {addon.Name}
                    </div>
                    <div>
                      ₱{Number(addon.Price * quantity).toLocaleString()}
                    </div>
                  </div>
                </>
              )) : null}
            </div>
            <div style={{ marginBottom: '1em', borderBottom: '1px solid rgba(125, 125, 125, 0.25)', padding: '0.5em', color: Colors.primary, fontWeight: 'bold', textAlign: 'right' }}>
              <span style={{ fontSize: '1.15em' }}>Subtotal</span> ₱{Number(sum).toLocaleString()}
            </div>
          </>
        ) : null}
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ flex: 1 }}>
            {!product.Voucher ? (
              <QuantitySelector
                value={quantity}
                onChange={setQuantity}
                style={{ marginTop: '0px' }}
              />
            ) : null}
          </div>
          <div style={{ flex: 1, textAlign: 'right' }}>
            <BrandingProvider
              override={(branding) => ({
                ...branding,
                Theme: {
                  ...branding.Theme,
                  Colors: {
                    ...branding.Theme.Colors,
                    primary: buttonColor,
                  },
                },
              })}
            >
              <Button
                onClick={() => {
                  cart.addItem(newItem);

                  notifications.queue({
                    type: 'info',
                    title: 'Added to Cart',
                    text: `"${product.Name}" has been added to your cart.`,
                  });

                  if (returnUrl) {
                    navigate(returnUrl);
                  }

                  if (onAdd) {
                    onAdd();
                  }
                }}
                style={{
                  fontSize: '0.8em',
                }}
                disabled={selectedOptions < requiredOptions || (product.Voucher && !claimedVoucher)}
              >
                {['an', 'pt'].includes(process.env.GATSBY_SITE) ? 'Add to Cart' : 'Order Now'}
              </Button>
            </BrandingProvider>
          </div>
        </div>
      </div>
    </div>
  );

  if (inline) {
    return content;
  }

  return (
    <ProductModal
      color={color}
      textColor={textColor}
    >
      <div>
        {content}
        <CloseButton to={returnUrl} className="close-button">
          <FaTimes />
        </CloseButton>
      </div>
    </ProductModal>
  );
};

export default Product;
