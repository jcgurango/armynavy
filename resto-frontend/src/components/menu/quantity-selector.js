import React from 'react';
import { FaMinus } from '@react-icons/all-files/fa/FaMinus';
import { FaPlus } from '@react-icons/all-files/fa/FaPlus';
import Pills from '../pills';
import { Colors } from '../../branding';

/**
 * @type {React.FunctionComponent<{
 *  value: Number,
 *  onChange: (quantity: Number) => void,
 *  onDelete: () => void,
 * }>}
 */
const QuantitySelector = ({
  value,
  onChange,
  onDelete,
  activeTextColor = Colors.darkText,
  isVoucher,
  ...props
}) => {
  return (
    <Pills {...props} activeTextColor={activeTextColor}>
      <a
        href="/#"
        onClick={(e) => {
          e.stopPropagation();
          e.preventDefault();

          if (value === 1) {
            if (onDelete) {
              onDelete();
            }
          } else {
            onChange(Math.max(1, value - 1));
          }
        }}
      >
        <FaMinus />
      </a>
      <input
        type="number"
        style={{
          width: '60px',
          textAlign: 'center',
          padding: '0px',
          backgroundColor: 'transparent',
        }}
        value={value}
        onChange={(e) => {
          e.preventDefault();
          e.stopPropagation();
          onChange(Math.max(Math.floor(Number(e.target.value)), 1));
        }}
        className="active"
      />
      <a
        href="/#"
        onClick={(e) => {
          e.stopPropagation();
          e.preventDefault();

          if (!isVoucher) {
            onChange(value + 1);
          }
        }}
      >
        <FaPlus />
      </a>
    </Pills>
  );
};

export default QuantitySelector;
