import React from 'react';
import styled from 'styled-components';
import { useBranding } from './providers/branding-provider';

const ErrorContainer = styled.pre`
  margin-bottom: ${({ branding }) => branding.Theme.Common.spacing};
  text-align: right;
  color: ${({ branding }) => branding.Theme.Colors.danger};
  font-family: inherit;
  font-size: inherit;
  white-space: pre-wrap;
`;

/**
 * @type {React.FunctionComponent<React.HTMLAttributes<HTMLDivElement>>}
 */
const ErrorText = (props) => {
  const branding = useBranding();

  return (
    <ErrorContainer
      branding={branding}
      {...props}
    />
  );
};

export default ErrorText;
