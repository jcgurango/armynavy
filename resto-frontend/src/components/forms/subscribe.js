import React, { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import v from 'vlid';

import api from '../../api';
import SiteConstants from '../../site-constants';
import Form from '../form';
import { useNotifications } from '../providers/notification-provider';
import ReCaptcha from '../recaptcha';
import TextInput from '../text-input';
import useValidation from '../utils/useValidation';

const OuterModal = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.25);
  z-index: 10000;
`;

const Container = styled.div`
  width: 100%;
  max-width: 600px;
  background-color: white;
  padding: 1em;
  text-align: center;
`;

const Subscribe = ({
  onClose = () => { },
}) => {
  const notifications = useNotifications();
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [captcha, setCaptcha] = useState('');

  const {
    isValid,
    errors,
  } = useValidation(useCallback(() => v.object({
    email: v.string().email('Please enter a valid email.').allow(''),
    phone: v.string().regex(/^09\d{9}$/g, 'Please enter a valid phone number in the format "09170000000".').allow(''),
    captcha: v.string().required('Please complete the captcha.'),
  }).rule((obj) => {
    return !!(obj.email || obj.phone);
  }, 'Please provide either an email or phone number.'), []), useMemo(() => ({
    email,
    phone,
    captcha,
  }), [email, phone, captcha]));

  return (
    <OuterModal
      onClick={() => {
        onClose();
      }}
    >
      <Container
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <b>Subscribe to get promos and updates!</b>
        <div style={{ height: '1em' }} />
        <Form
          valid={isValid}
          validationError={errors.map(({ message }) => message).join('\n')}
          onSubmit={async () => {
            await api.submit({
              Type: 'Subscription',
              Brand: SiteConstants.brand,
              Email: email,
              Phone: phone,
              Captcha: captcha,
            });

            setEmail(null);
            setPhone(null);
            onClose();

            notifications.queue({
              type: 'info',
              title: 'Subscribed Successfully',
              text: 'Thanks for subscribing!',
            });
          }}
        >
          <TextInput
            label="Email"
            value={email}
            onChange={setEmail}
          />
          <TextInput
            label="Phone"
            value={phone}
            onChange={setPhone}
          />
          <ReCaptcha onChange={setCaptcha} />
        </Form>
      </Container>
    </OuterModal>
  );
};

export default Subscribe;
