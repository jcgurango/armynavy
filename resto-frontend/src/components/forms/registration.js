import React, { useState } from 'react';

import TextInput from '../text-input';
import Form from '../form';
import api from '../../api';
import { useNotifications } from '../providers/notification-provider';
import { Link } from 'gatsby';

/**
 * @type {React.FunctionComponent}
 */
const RegistrationForm = () => {
  const notifications = useNotifications();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Form
      onSubmit={() => api.register({
        username: name,
        email,
        password,
      })}
      onSuccess={() => {
        notifications.queue({
          type: 'info',
          title: 'Success',
          text: 'Thank you for registering! We\'ve sent you an email to activate your account.',
        });
        setName('');
        setEmail('');
        setPassword('');
      }}
      onFail={(e) => {
        notifications.queue({
          type: 'error',
          title: 'Registration Error',
          text: e.message,
        });
      }}
      submitText="Register"
    >
      <TextInput
        type="text"
        label="Username"
        value={name}
        onChange={setName}
      />
      <br />
      <TextInput
        type="email"
        label="Email Address"
        value={email}
        onChange={setEmail}
      />
      <br />
      <TextInput
        type="password"
        label="Password"
        value={password}
        onChange={setPassword}
      />
      <small style={{ display: 'block', marginBottom: '1em' }}>
        Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our <Link to="/privacy-notice">Privacy Notice</Link>.
      </small>
    </Form>
  );
};

export default RegistrationForm;
