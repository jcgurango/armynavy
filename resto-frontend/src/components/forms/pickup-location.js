import React, { useMemo, useState } from 'react';
import 'react-datetime/css/react-datetime.css';
import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Form from '../form';
import SelectInput from '../select-input';
import { useBranding } from '../providers/branding-provider';
import moment from 'moment';

const PickupLocationContainer = styled.label`
  display: flex;
  flex-direction: row;
  padding: 1em;
  border-top: 1px solid ${({ color }) => color};
  align-items: center;
`;

/**
 * @type {React.FunctionComponent<{
 *  onSubmit: ({
 *    delivery: any,
 *    address: any,
 *  }) => Promise<void>,
 * }>}
 */
const PickupLocation = ({
  initialAddress,
  deliveryDate,
  onSubmit,
  children,
}) => {
  const {
    sitePickupLocations: pickupLocations,
  } = useStaticQuery(graphql`
    query {
      sitePickupLocations {
        strapiId
        City
        Location
        FullAddress
        StoreOpen
        StoreClose
        Open247
      }
    }
  `);

  const pickupCities = useMemo(() => {
    const cities = [];

    pickupLocations.forEach(({ City }) => {
      if (cities.indexOf(City) === -1) {
        cities.push(City);
      }
    });

    return cities;
  }, [pickupLocations]);

  const branding = useBranding();
  const [pickupCity, setPickupCity] = useState((initialAddress && initialAddress.PickupCity) || pickupCities[0]);
  const [pickupLocation, setPickupLocation] = useState((initialAddress && initialAddress.PickupLocation) || null);
  const location = pickupLocations.find(({ strapiId }) => strapiId === pickupLocation);
  let storeOpen = null;
  let storeClose = null;

  if (location && deliveryDate) {
    storeOpen = moment(moment(deliveryDate).format('YYYY-MM-DD ') + location.StoreOpen);
    storeClose = moment(moment(deliveryDate).format('YYYY-MM-DD ') + location.StoreClose);

    if (storeClose.isBefore(storeOpen)) {
      storeClose = storeClose.add(1, 'day');
    }
  }

  return (
    <Form
      submitText="Save"
      style={{
        width: '100%',
        maxWidth: '500px',
        marginTop: '1em',
      }}
      valid={
        pickupCity
          && pickupLocation
          && deliveryDate
          && (
            moment(deliveryDate).isValid()
              && moment(deliveryDate).isAfter(moment().add(30, 'minutes'))
              && pickupLocations.find(({ strapiId }) => strapiId === pickupLocation)
              && (
                pickupLocations.find(({ strapiId }) => strapiId === pickupLocation).Open247
                || (
                  moment(deliveryDate).isAfter(storeOpen)
                  && moment(deliveryDate).isBefore(storeClose)
                )
              )
          )
      }
      onSubmit={() => onSubmit({
        PickupCity: pickupCity,
        PickupLocation: pickupLocation,
        PickupLocationName: pickupLocations.find(({ strapiId }) => strapiId === pickupLocation).Location,
        PickupLocationAddress: pickupLocations.find(({ strapiId }) => strapiId === pickupLocation).FullAddress,
      })}
    >
      <b>Select City/Town</b>
      <div style={{ height: '1em' }} />
      <SelectInput value={pickupCity} onChange={(value) => { setPickupLocation(null); setPickupCity(value); }} required>
        {pickupCities.map((city) => (
          <option value={city} key={city}>{city}</option>
        ))}
      </SelectInput>
      <div style={{ height: '1em' }} />
      <b>Select Store</b>
      <div style={{ height: '1em' }} />
      {pickupLocations.filter(({ City }) => City === pickupCity).map(({ strapiId, Location, FullAddress, StoreOpen, StoreClose, Open247 }) => (
        <PickupLocationContainer key={strapiId} color={branding.Theme.Colors.primary}>
          <div style={{ paddingRight: '1em' }}>
            <input
              type="radio"
              name="pickupLocation"
              value={strapiId}
              checked={pickupLocation === strapiId}
              onChange={(e) => {
                if (e.target.checked) {
                  setPickupLocation(strapiId);
                }
              }}
            />
          </div>
          <div style={{ flex: 1, textAlign: 'left' }}>
            <b>{Location}</b>
            <div>
              {FullAddress}
            </div>
            <div>
              <b>Store Hours:</b> {Open247 ? 'Open 24/7' : `${moment('2021-01-01 ' + StoreOpen).format('h:mm A')} - ${moment('2021-01-01 ' + StoreClose).format('h:mm A')}`}
            </div>
          </div>
        </PickupLocationContainer>
      ))}
      <div style={{ height: '1em' }} />
      {children}
      <div style={{ height: '1em' }} />
    </Form>
  );
};

export default PickupLocation;
