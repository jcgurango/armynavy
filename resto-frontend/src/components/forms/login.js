import React, { useState } from 'react';

import TextInput from '../text-input';
import Form from '../form';
import api from '../../api';
import { useNotifications } from '../providers/notification-provider';
import { useUser } from '../providers/user-provider';
import { Link } from 'gatsby';

/**
 * @type {React.FunctionComponent}
 */
const LoginForm = () => {
  const notifications = useNotifications();
  const userService = useUser();
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Form
      onSubmit={() => api.login({
        identifier: name,
        password,
      })}
      onSuccess={(response) => {
        const { jwt, user } = response;
        userService.registerToken(jwt, user);

        setName('');
        setPassword('');
      }}
      onFail={(e) => {
        notifications.queue({
          type: 'error',
          title: 'Login Error',
          text: e.message,
        });
      }}
      submitText="Login"
    >
      <TextInput
        type="text"
        label="Email or Username"
        value={name}
        onChange={setName}
      />
      <br />
      <TextInput
        type="password"
        label="Password"
        value={password}
        onChange={setPassword}
      />
      <div style={{ textAlign: 'right', marginBottom: '0.5em' }}>
        <Link to="/forgot-password">Lost your password?</Link>
      </div>
    </Form>
  );
};

export default LoginForm;
