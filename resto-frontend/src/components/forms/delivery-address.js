import React, { useEffect, useState } from 'react';
import 'react-datetime/css/react-datetime.css';

import TextInput from '../text-input';
import Form from '../form';
import SelectInput from '../select-input';
import { graphql, useStaticQuery } from 'gatsby';
import { useUser } from '../providers/user-provider';

/**
 * @global
 * @typedef {{
 *  Region: String,
 *  City: String,
 *  StreetAddress: String,
 *  Barangay: String,
 *  UnitNumber: String,
 *  ZipCode: String,
 * }} Address
 */

/**
 * @type {React.FunctionComponent<{
 *  initialAddress: Address,
 *  onSubmit: (address: Address) => Promise<void>,
 *  addressType: 'Home' | 'Office',
 *  valid: Boolean,
 * }>}
 */
const DeliveryAddress = ({
  initialAddress,
  children,
  onSubmit,
  valid = true,
  addressType,
  ...props
}) => {
  const {
    allOfsRegion: {
      nodes: regions,
    },
    allOfsCity: {
      nodes: cities,
    },
  } = useStaticQuery(graphql`
    query {
      allOfsRegion {
        nodes {
          id
          region_name
        }
      }
      allOfsCity {
        nodes {
          id
          city_name
          region_id
        }
      }
    }  
  `);

  const user = useUser();
  const [region, setRegion] = useState((initialAddress && initialAddress.Region) || 'METRO MANILA');
  const [city, setCity] = useState((initialAddress && initialAddress.City) || '');
  const [streetAddress, setStreetAddress] = useState((initialAddress && initialAddress.StreetAddress) || '');
  const [barangay, setBarangay] = useState((initialAddress && initialAddress.Barangay) || '');
  const [unitNumber, setUnitNumber] = useState((initialAddress && initialAddress.UnitNumber) || '');
  const [zipCode, setZipCode] = useState((initialAddress && initialAddress.ZipCode) || '');
  const [companyName, setCompanyName] = useState((initialAddress && initialAddress.CompanyName) || '');

  useEffect(() => {
    if (user.userData && (!initialAddress || !initialAddress.StreetAddress)) {
      const address = user.userData[`${addressType}Address`];

      if (address) {
        setRegion(address.Region || 'METRO MANILA');
        setCity(address.City || '');
        setStreetAddress(address.StreetAddress || '');
        setBarangay(address.Barangay || '');
        setUnitNumber(address.UnitNumber || '');
        setZipCode(address.ZipCode || '');
        setCompanyName(address.CompanyName || '');
      } else if (!initialAddress) {
        setRegion('METRO MANILA');
        setCity('');
        setStreetAddress('');
        setBarangay('');
        setUnitNumber('');
        setZipCode('');
        setCompanyName('');
      }
    }
  }, [user.userData, addressType, initialAddress]);

  return (
    <Form
      submitText="Set Delivery Address"
      valid={valid && region && city && streetAddress && barangay}
      onSubmit={() => onSubmit({
        Region: region,
        City: city,
        StreetAddress: streetAddress,
        Barangay: barangay,
        UnitNumber: unitNumber,
        ZipCode: zipCode,
        CompanyName: companyName,
      })}
      {...props}
    >
      <div style={{ maxWidth: '500px', marginLeft: 'auto', marginRight: 'auto' }}>
        <b className="label-required">Country</b>
        <div style={{ height: '0.5em' }} />
        <TextInput
          value="Philippines"
          disabled
        />
        <div style={{ height: '0.5em' }} />
        <b className="label-required">Region</b>
        <div style={{ height: '0.5em' }} />
        <SelectInput value={region} onChange={setRegion} required>
          {regions.map((region) => (
            <option value={region.region_name} key={region.id}>
              {region.region_name}
            </option>
          ))}
        </SelectInput>
        <div style={{ height: '0.5em' }} />
        <b className="label-required">Town/City</b>
        <div style={{ height: '0.5em' }} />
        <SelectInput value={city} onChange={setCity} required>
          <option value="">Select an Option</option>
          {cities.filter(({ region_id }) => {
            if (region) {
              const foundRegion = regions.find(({ region_name }) => region_name === region);

              if (foundRegion) {
                return foundRegion.id === region_id;
              }
            }

            return false;
          }).map((city) => (
            <option value={city.city_name} key={city.id}>
              {city.city_name}
            </option>
          ))}
        </SelectInput>
        <div style={{ height: '0.5em' }} />
        <b className="label-required">Street Address</b>
        <div style={{ height: '0.5em' }} />
        {addressType === 'Office' ? (
          <TextInput
            label="Company Name"
            value={companyName}
            onChange={setCompanyName}
          />
        ) : null}
        <TextInput
          label="Apartment, suite, unit, etc. (optional)"
          value={unitNumber}
          onChange={setUnitNumber}
        />
        <TextInput
          label="House number and street name"
          value={streetAddress}
          onChange={setStreetAddress}
          required
        />
        <TextInput
          label="Barangay"
          value={barangay}
          onChange={setBarangay}
          required
        />
        <div style={{ height: '0.5em' }} />
        <b>Postcode / Zip</b>
        <div style={{ height: '0.5em' }} />
        <TextInput
          label="Postcode / Zip (optional)"
          value={zipCode}
          onChange={setZipCode}
        />
        <div style={{ height: '1em' }} />
      </div>
    </Form>
  );
};

export default DeliveryAddress;
