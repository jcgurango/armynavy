import React, { useState } from 'react';
import 'react-datetime/css/react-datetime.css';
import './date.css';
import DateTime from 'react-datetime';
import moment from 'moment';

import Pills from '../pills';
import { useUser } from '../providers/user-provider';
import DeliveryAddress from '../forms/delivery-address';
import api from '../../api';
import styled from 'styled-components';
import PickupLocation from './pickup-location';
import { useBranding } from '../providers/branding-provider';

const getOrderType = (type) => {
  if (type === 'Pickup') {
    return 'pickup';
  }

  return 'delivery';
};

const getDeliveryType = (type) => {
  if (type === 'DeliverNow') {
    return 'now';
  }

  if (type === 'DeliverLater') {
    return 'later';
  }
};

const OptionBox = styled.a`
  display: inline-block;
  box-sizing: border-box;
  background-color: rgb(200, 200, 200);
  padding: 0.75em;
  text-transform: uppercase;
  color: inherit;
  text-decoration: none;
  border: 0.1em solid black;

  &.active {
    background-color: white;
  }
  
  @media only screen and (min-width: 800px) {
    font-size: 1.75em;
  }
`;

const DeliveryType = styled(OptionBox)`
  padding-top: 1em;

  &:first-child {
    padding-right: 2em;
  }

  &:last-child {
    padding-left: 2em;
    border-left: 0px;
  }
`;

const NowOrLaterContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 1em;
  margin-bottom: 1em;
`;

/**
 * @type {React.FunctionComponent<{
 *  onSubmit: ({
 *    delivery: any,
 *    address: any,
 *  }) => Promise<void>,
 * }>}
 */
const DeliveryData = ({
  initialDeliveryInfo,
  initialAddress,
  onSubmit,
}) => {
  const user = useUser();
  const branding = useBranding();
  const [orderType, setOrderType] = useState((initialDeliveryInfo && getOrderType(initialDeliveryInfo.DeliveryType)) || null);
  const [deliveryType, setDeliveryType] = useState((initialDeliveryInfo && getDeliveryType(initialDeliveryInfo.DeliveryType)) || null);
  const [deliveryDate, setDeliveryDate] = useState((initialDeliveryInfo && initialDeliveryInfo.DeliveryDate) || moment());
  const [addressType, setAddressType] = useState((initialDeliveryInfo && initialDeliveryInfo.AddressType) || 'Home');

  const dtp = (
    <DateTime
      input={false}
      value={moment(deliveryDate)}
      onChange={setDeliveryDate}
      isValidDate={(date) => moment(date).isSameOrAfter(moment().add(30, 'minutes').startOf('day'))}
    />
  );

  const activeColor = (process.env.GATSBY_SITE === 'ww' || branding.isMessenger) ? branding.Theme.Colors.primary : branding.Theme.Colors.secondary;

  return (
    <>
      <div style={{ textAlign: 'center', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Pills withBg style={{ fontSize: '1.5em' }} activeColor={activeColor}>
          <a
            href="/#"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              setOrderType('delivery');
              setDeliveryDate(null);
            }}
            className={orderType === 'delivery' ? 'active' : null}
          >
            Delivery
          </a>
          <a
            href="/#"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              setOrderType('pickup');
            }}
            className={orderType === 'pickup' ? 'active' : null}
          >
            Pick-up
          </a>
        </Pills>
        {orderType === 'delivery' ? (
          <>
            <Pills withBg style={{ fontSize: '1.25em' }} allBorders activeColor={activeColor}>
              <a
                href="/#"
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setDeliveryType('now');
                  setDeliveryDate(null);
                }}
                className={deliveryType === 'now' ? 'active' : null}
              >
                Deliver
                <br />
                Now
              </a>
              <div style={{ fontWeight: 'bold', marginLeft: '1em', marginRight: '1em', display: 'flex', flexDirection: 'row', alignItems: 'center' }} className="ignore-style">
                OR
              </div>
              <a
                href="/#"
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setDeliveryType('later');
                }}
                className={deliveryType === 'later' ? 'active' : null}
              >
                Deliver
                <br />
                Later
              </a>
            </Pills>
            {deliveryType === 'later' ? (
              <>
                <div style={{ height: '1em' }} />
                <b>Delivery Date & Time</b>
                <div style={{ height: '0.5em' }} />
                {dtp}
              </>
            ) : null}
            {deliveryType ? (
              <>
                <Pills withBg style={{ marginTop: '1.5em', marginBottom: '1.5em' }} activeColor={activeColor}>
                  <a
                    href="/#"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      setAddressType('Home');
                    }}
                    className={addressType === 'Home' ? 'active' : null}
                  >
                    Home
                  </a>
                  <a
                    href="/#"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      setAddressType('Office');
                    }}
                    className={addressType === 'Office' ? 'active' : null}
                  >
                    Office
                  </a>
                </Pills>
                <DeliveryAddress
                  addressType={addressType}
                  initialAddress={initialAddress}
                  onSubmit={async (address) => {
                    if (user.token) {
                      // First save the address to the user for the future.
                      const data = await api.updateAddress({
                        token: user.token,
                        address: {
                          addressType,
                          ...address,
                        },
                      });

                      user.updateUserData(data);
                    }

                    await onSubmit({
                      delivery: {
                        DeliveryType: deliveryType === 'now' ? 'DeliverNow' : 'DeliverLater',
                        AddressType: addressType,
                        DeliveryDate: deliveryDate && moment(deliveryDate).toISOString(),
                      },
                      address,
                    });
                  }}
                  valid={deliveryType === 'now' || (deliveryDate && moment(deliveryDate).isAfter(moment().add(30, 'minutes')))}
                />
              </>
            ) : null}
          </>
        ) : null}
        {orderType === 'pickup' ? (
          <PickupLocation
            initialAddress={initialAddress}
            onSubmit={async (address) => {
              await onSubmit({
                delivery: {
                  DeliveryType: 'Pickup',
                  AddressType: addressType,
                  DeliveryDate: moment(deliveryDate).toISOString(),
                },
                address,
              });
            }}
            deliveryDate={deliveryDate}
          >
            <b>Select Date & Time</b>
            <div style={{ height: '1em' }} />
            {dtp}
          </PickupLocation>
        ) : null}
      </div>
    </>
  );
};

export default DeliveryData;
