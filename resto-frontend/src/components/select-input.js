import React from 'react';
import styled from 'styled-components';
import ErrorText from './error-text';
import { useBranding } from './providers/branding-provider';

const Select = styled.select`${({ branding }) => branding.Input(branding)}`;

/**
 * @type {React.FunctionComponent<{
 *  type: 'text' | 'password' | 'email',
 *  label: string,
 *  value: any,
 *  onChange: (newValue: any) => void,
 *  allowNull: Boolean
 * } & React.InputHTMLAttributes<HTMLSelectElement>>}
 */
const SelectInput = ({
  type,
  value,
  label,
  onChange,
  children,
  allowNull,
  error,
  ...props
}) => {
  const branding = useBranding();

  return (
    <>
      <Select
        branding={branding}
        type={type}
        placeholder={label}
        value={value || ''}
        onChange={(e) => {
          if (onChange) {
            onChange(e.target.value);
          }
        }}
        style={{
          width: '100%',
          boxSizing: 'border-box',
        }}
        className={error ? 'has-error' : null}
        {...props}
      >
        {label ? (
          <option value="" disabled={!allowNull}>{label}</option>
        ) : null}
        {children}
      </Select>
      {error ? (
        <ErrorText style={{ textAlign: 'left' }}>
          {error.message}
        </ErrorText>
      ) : null}
    </>
  );
};

export default SelectInput;
