import { graphql, Link, useStaticQuery } from 'gatsby';
import { GatsbyImage, getImage, StaticImage } from 'gatsby-plugin-image';
import React, { useState } from 'react';
import styled from 'styled-components';
import { FaFacebookF } from '@react-icons/all-files/fa/FaFacebookF';
import { FaTwitter } from '@react-icons/all-files/fa/FaTwitter';
import { FaInstagram } from '@react-icons/all-files/fa/FaInstagram';
import Button from './button';
import Subscribe from './forms/subscribe';

const FooterContainer = styled.div`
  > div.chevron-lines {
    margin-top: 1em;
    margin-bottom: 1em;
    max-width: 1100px;
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    padding-left: 1em;
    padding-right: 1em;
    box-sizing: border-box;
  }

  > div.brands {
    background-color: rgb(58, 57, 53);
  }

  > div:last-child {
    margin-top: 1em;
    margin-bottom: 1em;
    max-width: 1100px;
    padding-left: 1em;
    padding-right: 1em;
    margin-left: auto;
    margin-right: auto;
    display: flex;
    flex-direction: column;

    @media only screen and (min-width: 800px) {
      flex-direction: row;
      align-items: center;
    }

    .social-links {
      display: flex;
      flex-direction: row;
      justify-content: space-around;

      @media only screen and (min-width: 800px) {
        a {
          margin-left: 1em;
        }
      }
    }

    .center {
      flex: 1;
      margin-top: 1em;
      margin-bottom: 1em;
      display: flex;
      flex-direction: column;
      text-align: center;

      > .nav {
        display: flex;
        flex-direction: row;

        > div {
          flex: 1;
        }

        a {
          display: block;
          color: inherit;

          @media only screen and (min-width: 800px) {
            font-size: 1.25em;
            text-align: left;
          }
        }

        @media only screen and (min-width: 800px) {
          align-items: center;
        }
      }

      > .newsletter {
        margin-top: 1em;
        margin-bottom: 1em;
        font-size: 0.85em;

        @media only screen and (min-width: 800px) {
          margin: 0;
        }
      }

      > div {
        flex: 1;
      }

      @media only screen and (min-width: 800px) {
        margin-left: 1.5em;
        margin-right: 1.5em;
        flex-direction: row;
        text-align: left;
      }
    }
  }

  margin-top: 2px;

  @media only screen and (min-width: 800px) {
    .brand-logo {
      min-width: 171px;
    }
  }
`;

const OtherBrandsContainer = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 1100px;
  margin-left: auto;
  margin-right: auto;

  > div:first-child {
    border-right: 0.1em solid white;
  }

  > div {
    flex: 1;
    background-color: rgb(58, 57, 53);
    padding: 0.75em 1em;
  }

  > div:last-child {
    flex: 6;

    > div:last-child {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;

      > div {
        margin-left: 0.25em;
        margin-right: 0.25em;

        @media only screen and (min-width: 800px) {
          &:first-child {
            margin-left: 0.25em;
          }
        }
      }
    }

    .callout {
      text-align: center;
      color: rgb(161, 203, 212);
      margin-bottom: 0.5em;
    }
  }

  @media only screen and (min-width: 700px) {
    height: ${() => process.env.GATSBY_SITE === 'ww' ? '180px' : '150px'};

    > div:last-child {
      .callout {
        font-size: 1.1em;
      }
    }
  }
`;

const SocialButton = styled.a`
  background-color: rgb(58, 57, 53);
  width: 2em;
  height: 2em;
  border-radius: 2em;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
`;

/**
 * @type {React.FunctionComponent<{
 * }>}
 */
const Footer = () => {
  const {
    siteStorefront: [
      {
        Logo,
        Name,
        SocialLinks,
        Footer,
      },
    ],
  } = useStaticQuery(graphql`
    query {
      siteStorefront {
        Logo {
          ltmt: childImageSharp {
            gatsbyImageData(height: 42)
          }
          rr: childImageSharp {
            gatsbyImageData(height: 112)
          }
          ww: childImageSharp {
            gatsbyImageData(height: 75)
          }
          an: childImageSharp {
            gatsbyImageData(height: 55)
          }
          pt: childImageSharp {
            gatsbyImageData(height: 55)
          }
        }
        SocialLinks {
          Type
          URL
        }
        Footer {
          id
          Text
          URL
        }
      }
    }
  `);
  const [showSubscription, setShowSubscription] = useState(false);

  return (
    <>
      <FooterContainer>
        {process.env.GATSBY_SITE === 'an' ? (
          <div className="chevron-lines">
            <StaticImage src="../images/chevron-lines.png" alt="Army Navy brands" layout="fullWidth" />
          </div>
        ) : null}
        <div className="brands">
          <OtherBrandsContainer>
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
              <div>
                <StaticImage src="../images/brands/brands.png" alt="Army Navy brands" layout="constrained" height="126" />
              </div>
            </div>
            <div>
              <div className="callout">
                Check out the other brands!
              </div>
              <div>
                {process.env.GATSBY_SITE === 'ww' ? (
                  <>
                    {process.env.GATSBY_SITE !== 'an' ? (
                      <a href="https://www.armynavy.com.ph" style={{ display: 'block', border: 0 }}>
                        <StaticImage src="../images/brands/armynavy.png" alt="Army Navy" layout="constrained" height="110" />
                      </a>
                    ) : null}
                    {process.env.GATSBY_SITE !== 'pt' ? (
                      <a href="https://www.pizzatelefono.com" style={{ display: 'block', border: 0 }}>
                        <StaticImage src="../images/brands/pizzatelefono.png" alt="Pizza Telefono" layout="constrained" height="110" />
                      </a>
                    ) : null}
                    {process.env.GATSBY_SITE !== 'rr' ? (
                      <a href="https://www.ricerocket.com.ph" style={{ display: 'block', border: 0 }}>
                        <StaticImage src="../images/brands/ricerocket.png" alt="Rice Rocket" layout="constrained" height="110" />
                      </a>
                    ) : null}
                    {process.env.GATSBY_SITE !== 'ltmt' ? (
                      <a href="https://www.libertea.com.ph" style={{ display: 'block', border: 0 }}>
                        <StaticImage src="../images/brands/libertea.png" alt="LiberTea" layout="constrained" height="110" />
                      </a>
                    ) : null}
                    {process.env.GATSBY_SITE !== 'ww' ? (
                      <a href="https://www.wrightwings.com" style={{ display: 'block', border: 0 }}>
                        <StaticImage src="../images/brands/wrightwings.png" alt="Wright Wings" layout="constrained" height="110" />
                      </a>
                    ) : null}
                  </>
                ) : (
                  process.env.GATSBY_SITE === 'pt' ? (
                      <>
                      {process.env.GATSBY_SITE !== 'an' ? (
                        <a href="https://www.armynavy.com.ph" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/armynavy.png" alt="Army Navy" layout="constrained" height="80" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'pt' ? (
                        <a href="https://www.pizzatelefono.com" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/pizzatelefono.png" alt="Pizza Telefono" layout="constrained" height="80" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'rr' ? (
                        <a href="https://www.ricerocket.com.ph" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/ricerocket.png" alt="Rice Rocket" layout="constrained" height="80" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'ltmt' ? (
                        <a href="https://www.libertea.com.ph" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/libertea.png" alt="LiberTea" layout="constrained" height="80" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'ww' ? (
                        <a href="https://www.wrightwings.com" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/wrightwings.png" alt="Wright Wings" layout="constrained" height="80" />
                        </a>
                      ) : null}
                    </>
                  ) : (
                    <>
                      {process.env.GATSBY_SITE !== 'an' ? (
                        <a href="https://www.armynavy.com.ph" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/armynavy.png" alt="Army Navy" layout="constrained" height="95" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'pt' ? (
                        <a href="https://www.pizzatelefono.com" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/pizzatelefono.png" alt="Pizza Telefono" layout="constrained" height="95" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'rr' ? (
                        <a href="https://www.ricerocket.com.ph" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/ricerocket.png" alt="Rice Rocket" layout="constrained" height="95" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'ltmt' ? (
                        <a href="https://www.libertea.com.ph" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/libertea.png" alt="LiberTea" layout="constrained" height="95" />
                        </a>
                      ) : null}
                      {process.env.GATSBY_SITE !== 'ww' ? (
                        <a href="https://www.wrightwings.com" style={{ display: 'block', border: 0 }}>
                          <StaticImage src="../images/brands/wrightwings.png" alt="Wright Wings" layout="constrained" height="95" />
                        </a>
                      ) : null}
                    </>
                  )
                )}
              </div>
            </div>
          </OtherBrandsContainer>
        </div>
        <div>
          <div style={{ alignSelf: 'center' }} className="brand-logo">
            <Link to="/" alt={Name} style={{ marginTop: 'auto', marginBottom: 'auto', display: 'block' }}>
              <GatsbyImage
                image={getImage(Logo[process.env.GATSBY_SITE])}
                alt={Name}
              />
            </Link>
          </div>
          <div className="center">
            <div className="nav">
              <div>
                {Footer.slice(0, Footer.length / 2).map(({ id, Text, URL }) => (
                  <Link key={id} to={URL}>{Text}</Link>
                ))}
              </div>
              <div>
                {Footer.slice(Footer.length / 2).map(({ id, Text, URL }) => (
                  <Link key={id} to={URL}>{Text}</Link>
                ))}
              </div>
            </div>
            <div className="newsletter">
              <b>Get exclusive offers and discounts.</b><br />
              By leaving your email address, you are agreeing to our <Link to="/privacy-notice">Privacy Notice</Link> and <Link to="/terms-of-use">Terms of Use</Link>.<br />
              <br />
              <Button
                buttonType="button"
                color={['ww', 'pt'].includes(process.env.GATSBY_SITE) ? 'primary' : 'secondary'}
                onClick={() => {
                  setShowSubscription(true);
                }}
              >
                Subscribe to our newsletter
              </Button>
            </div>
          </div>
          <div className="social-links">
            {SocialLinks.map(({ Type, URL }) => (
              <SocialButton
                key={Type}
                href={URL}
                target="_blank"
              >
                {{
                  Facebook: (
                    <FaFacebookF />
                  ),
                  Twitter: (
                    <FaTwitter />
                  ),
                  Instagram: (
                    <FaInstagram />
                  ),
                }[Type]}
              </SocialButton>
            ))}
          </div>
        </div>
      </FooterContainer>
      {showSubscription ? (
        <Subscribe
          onClose={() => setShowSubscription(false)}
        />
      ) : null}
    </>
  );
};

export default Footer;
