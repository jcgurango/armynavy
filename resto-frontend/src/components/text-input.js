import React from 'react';
import styled from 'styled-components';
import ErrorText from './error-text';
import { useBranding } from './providers/branding-provider';

const Input = styled.input`${({ branding }) => branding.Input(branding)}`;
const BigInput = styled.textarea`
  font-family: inherit;
  font-size: inherit;
  min-height: 100px;
  resize: none;
  ${({ branding }) => branding.Input(branding)}
`;

/**
 * @type {React.FunctionComponent<{
 *  type: 'text' | 'password' | 'email',
 *  label: string,
 *  value: any,
 *  multiline: Boolean,
 *  onChange: (newValue: any) => void
 * } & React.InputHTMLAttributes<HTMLInputElement>>}
 */
const TextInput = ({
  type,
  value,
  label,
  multiline,
  onChange,
  error,
  ...props
}) => {
  const branding = useBranding();
  const Component = multiline ? BigInput : Input;

  return (
    <>
      <Component
        branding={branding}
        type={type}
        placeholder={label}
        value={value || ''}
        onChange={(e) => {
          if (onChange) {
            onChange(e.target.value);
          }
        }}
        {...props}
        style={{
          width: '100%',
          boxSizing: 'border-box',
          ...props.style || {},
        }}
        className={error ? 'has-error' : null}
      />
      {error ? (
        <ErrorText style={{ textAlign: 'left' }}>
          {error.message}
        </ErrorText>
      ) : null}
    </>
  );
};

export default TextInput;
