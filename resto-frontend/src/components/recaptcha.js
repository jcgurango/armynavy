import React from 'react';
import ReCAPTCHA from 'react-google-recaptcha';

const ReCaptcha = ({
  onChange,
}) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end', marginBottom: '1em' }}>
      <ReCAPTCHA
        sitekey={process.env.RECAPTCHA_SITE_KEY}
        onChange={onChange}
      />
    </div>
  );
};

export default ReCaptcha;
