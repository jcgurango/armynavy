import styled from 'styled-components';

const MainContainer = styled.main`
  width: 100%;
  max-width: ${() => process.env.GATSBY_SITE === 'pt' ? '1200px' : '1100px'};
  margin-left: auto;
  margin-right: auto;
  padding-left: 0.5em;
  padding-right: 0.5em;
  height: auto;
  flex: 1;
  box-sizing: border-box;
`;

export default MainContainer;