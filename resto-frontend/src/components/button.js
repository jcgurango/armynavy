import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import { useBranding } from './providers/branding-provider';

const InnerButton = styled.button`
  cursor: pointer;
  ${({ branding, color }) => branding.Button(branding, color)}
`;
const InnerLink = styled(Link)`
  display: inline-block;
  text-decoration: none;
  text-align: center;
  box-sizing: border-box;
  ${({ branding, color }) => branding.Button(branding, color)}
`;


/**
 * @type {React.FunctionComponent<
 *  ({ buttonType: 'button' } & React.ButtonHTMLAttributes)
 *  | ({ buttonType: 'link' } & import('gatsby-link').GatsbyLinkProps)
 * >}
 */
const Button = ({
  buttonType,
  children,
  onClick,
  color = 'primary',
  ...props
}) => {
  let branding = useBranding();

  if (buttonType === 'link') {
    return (
      <InnerLink
        {...props}
        branding={branding}
        color={color}
      >
        {children}
      </InnerLink>
    );
  }

  return (
    <InnerButton
      {...props}
      branding={branding}
      color={color}
      onClick={(e) => {
        if (onClick) {
          e.preventDefault();
          e.stopPropagation();
          onClick();
        }
      }}
    >
      {children}
    </InnerButton>
  );
}

export default Button;
