import { useEffect, useMemo, useState } from 'react';

if (!Error.captureStackTrace) {
  Error.captureStackTrace = () => { };
}

const useValidation = (schema, values) => {
  const [{
    errors,
    isValid,
  }, setData] = useState({
    errors: [],
    isValid: false,
  });
  const isFirstRun = useMemo(() => ({ value: true }), []);

  const firstError = (p) => {
    return errors.find(({ path }) => {
      return path.indexOf(p) === 0;
    });
  };

  useEffect(() => {
    if (isFirstRun.value) {
      isFirstRun.value = false;
    } else {
      const {
        errors,
        isValid,
      } = (typeof (schema) === 'function' ? schema() : schema).validateSync(values);

      setData({
        errors,
        isValid,
      });
    }
  }, [schema, values, isFirstRun]);

  return {
    errors,
    isValid,
    firstError,
  };
};

export default useValidation;
