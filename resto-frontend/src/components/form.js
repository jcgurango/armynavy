import React, { useState } from 'react';
import Button from './button';
import ErrorText from './error-text';

/**
 * @type {React.FunctionComponent<{
 *  onSubmit: () => Promise<any>,
 *  onSuccess?: (result: any) => void,
 *  onFail?: (error: any) => void,
 *  valid: Boolean,
 *  validationError: String,
 *  submitText: String,
 * }>}
 */
const Form = ({
  children,
  onSubmit,
  onSuccess,
  onFail,
  valid = true,
  validationError = '',
  submitText = 'Submit',
  errorProps = { },
  ...props
}) => {
  const [error, setError] = useState('');
  const [submitting, setSubmitting] = useState(false);

  return (
    <form
      onSubmit={async (e) => {
        e.stopPropagation();
        e.preventDefault();
        setSubmitting(true);
        setError('');

        try {
          const result = await onSubmit();

          if (onSuccess) {
            onSuccess(result);
          }
        } catch (e) {
          setError(e.message);

          if (onFail) {
            onFail(e);
          }
        }

        setSubmitting(false);
      }}
      {...props}
    >
      {children}
      <div
        style={{
          textAlign: 'right',
        }}
      >
        {error ? (
          <ErrorText>Error: {error}</ErrorText>
        ) : null}
        {validationError ? (
          <ErrorText {...errorProps}>{validationError}</ErrorText>
        ) : null}
        <Button type="submit" disabled={!valid || submitting} style={{ width: '100%' }}>
          {submitText}
        </Button>
      </div>
    </form>
  );
};

export default Form;
