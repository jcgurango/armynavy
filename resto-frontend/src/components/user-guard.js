import React from 'react';
import styled from 'styled-components';
import MainContainer from '../components/main-container';
import RegistrationForm from '../components/forms/registration';
import LoginForm from '../components/forms/login';
import { useUser } from './providers/user-provider';

const FormsContainer = styled.div`
  display: flex;
  flex-direction: column-reverse;

  @media only screen and (min-width: 760px) {
    flex-direction: row;
  }
`;

const FormContainer = styled.div`
  flex: 1;
  margin-top: ${({ margin }) => margin ? '1em' : '0'};
  box-sizing: border-box;

  @media only screen and (min-width: 760px) {
    margin-top: 0px;
    margin-left: ${({ margin }) => margin ? '2em' : '0'};
  }
`;

export const useIsAuthenticated = () => {
  const user = useUser();

  return Boolean(user.token && user.userData);
};

export const AuthForm = ({
  children,
}) => {
  return (
    <FormsContainer>
      <FormContainer>
        <h1>Log In</h1>
        <LoginForm />
      </FormContainer>
      <FormContainer margin>
        <h1>Register</h1>
        <RegistrationForm />
        {children}
      </FormContainer>
    </FormsContainer>
  );
};

const UserGuard = ({
  children,
}) => {
  const authenticated = useIsAuthenticated();

  if (!authenticated) {
    return (
      <MainContainer>
        <AuthForm />
      </MainContainer>
    );
  }

  return children;
};

export default UserGuard;
