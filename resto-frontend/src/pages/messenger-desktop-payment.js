import React, { useEffect } from 'react';

const ThankYouPage = () => {
  useEffect(() => {
    if (typeof (window) !== 'undefined') {
      (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/messenger.Extensions.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'Messenger'));
    }
  }, []);

  if (typeof (window) !== 'undefined') {
    return (
      <a
        href={`/orders/${String(window.location.hash).substring(1)}/pay`}
        target="_blank"
        style={{ fontFamily: 'Arial, Helvetica, sans-serif', fontSize: '4em', textAlign: 'center', color: 'cornflowerblue', textDecoration: 'none', margin: '10%', marginTop: '20%', display: 'block' }}
        onClick={(e) => {
          window.MessengerExtensions.requestCloseBrowser();
        }}
      >
        Please click here to continue to payment.
      </a>
    );
  }

  return null;
};

export default ThankYouPage;
