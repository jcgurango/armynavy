import React, { useState } from 'react';
import api from '../api';
import Form from '../components/form';
import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import TextInput from '../components/text-input';

const ForgotPasswordPage = () => {
  const [submitted, setSubmitted] = useState(false);
  const [email, setEmail] = useState('');

  return (
    <Layout title="Reset Password">
      <MainContainer>
        {submitted ? (
          <>
            <h1>Password reset email has been sent.</h1>
            <p>A password reset email has been sent to the email address on file for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.</p>
          </>
        ) : (
          <Form
            submitText="Reset Password"
            valid={!!email}
            onSubmit={async () => {
              await api.forgotPassword({
                email,
              });

              setSubmitted(true);
            }}
          >
            <p>Lost your password? Please enter your email address. You will receive a link to create a new password via email.</p>
            <TextInput
              label="Email"
              name="email"
              type="email"
              value={email}
              onChange={setEmail}
            />
          </Form>
        )}
      </MainContainer>
    </Layout>
  );
};

export default ForgotPasswordPage;
