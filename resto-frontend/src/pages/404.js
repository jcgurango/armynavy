import React from 'react';

import Layout from '../components/layout';
import MainContainer from '../components/main-container';

const NotFoundPage = () => {
  return (
    <Layout>
      <MainContainer>
        Sorry, we couldn't find that page.
      </MainContainer>
    </Layout>
  );
};

export default NotFoundPage;
