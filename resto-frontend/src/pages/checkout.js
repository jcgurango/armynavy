import React, { useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import 'react-datetime/css/react-datetime.css';
import v from 'vlid';

import Button from '../components/button';
import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import { useBranding } from '../components/providers/branding-provider';
import { useCart } from '../components/providers/cart-provider';
import { useUser } from '../components/providers/user-provider';
import { AuthForm, useIsAuthenticated } from '../components/user-guard';
import api from '../api';
import Cart from '../components/menu/cart';
import SelectInput from '../components/select-input';
import Form from '../components/form';
import TextInput from '../components/text-input';
import { navigate } from 'gatsby-link';
import AvailabilityProvider from '../components/providers/availability-provider';
import { useNotifications } from '../components/providers/notification-provider';
import moment from 'moment';
import useValidation from '../components/utils/useValidation';
import AcceptTerms from '../components/accept-terms';

const CartContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media only screen and (min-width: 700px) {
    flex-direction: row;
  }
`;

const SummaryContainer = styled.div`
  @media only screen and (min-width: 700px) {
    margin-left: 1em;
  }

  @media only screen and (min-width: 1000px) {
    flex: 1;
  }
`;

const SummaryItem = styled.div`
  padding: 0.75em;
  border-bottom: 0.1em solid ${({ branding: { Theme: { Colors } } }) => Colors.primary};
  display: flex;
  flex-direction: row;

  > :last-child {
    flex: 3;
    text-align: right;
  }
`;

const validationSchema = () => v.object({
  FirstName: v.string().required('Please enter your first name.'),
  LastName: v.string().required('Please enter your last name.'),
  ContactNumber: v.string()
    .regex(/^0\d{10}$/, 'Contact number must be in the format "09170000000".'),
  RecipientContactNumber: v.string()
    .regex(/^0\d{10}$/, 'Contact number must be in the format "09170000000".')
    .allow(['', null]),
  Email: v.string().email('Please provide a valid email.'),
  agreed: v.any().rule((v) => v === true, 'You must agree to the website terms and conditions.'),
}).rule((v) => {
  if (v.paymentMethod === 'COD') {
    return Number(v.ChangeFor) >= v.total;
  }

  return true;
}, 'Change for amount must be greater than order total.').rule((v) => {
  if (v.delivery !== 'Pickup') {
    return v.subtotal >= 300;
  }

  return true;
}, 'The minimum order for delivery is ₱300.');

export const CheckoutFlow = ({
  editable,
  messengerId,
}) => {
  const user = useUser();
  const cart = useCart();
  const [agreed, setAgreed] = useState(false);
  const [deliveryDetails, setDeliveryDetails] = useState({
    FirstName: cart.deliveryData?.details?.FirstName || user?.userData?.FirstName || '',
    LastName: cart.deliveryData?.details?.LastName || user?.userData?.LastName || '',
    Email: cart.deliveryData?.details?.Email || user?.userData?.email || '',
    ContactNumber: cart.deliveryData?.details?.ContactNumber || user?.userData?.Phone || '',
    RecipientName: cart.deliveryData?.details?.RecipientName ||'',
    RecipientContactNumber: cart.deliveryData?.details?.RecipientContactNumber ||'',
    SpecialInstructions: cart.deliveryData?.details?.SpecialInstructions || '',
    PwdId: cart.deliveryData?.details?.PwdId || '',
    ScdId: cart.deliveryData?.details?.ScdId || '',
    ChangeFor: cart.deliveryData?.details?.ChangeFor || '',
  });

  useEffect(() => {
    cart.setDeliveryData((details) => ({ ...details, details: deliveryDetails }));
  }, [deliveryDetails]);

  const [paymentMethod, setPaymentMethod] = useState('COD');
  const branding = useBranding();
  const subtotal = cart.calculateTotal(cart.items);
  const voucherDiscount = cart.calculateVoucherDiscount(cart.items);
  const total = subtotal + (cart.deliveryData.delivery.DeliveryType !== 'Pickup' ? cart.deliveryFee : 0) - voucherDiscount;
  const notifications = useNotifications();

  const validationValues = useMemo(() => {
    return {
      ...deliveryDetails,
      subtotal,
      agreed,
      total,
      paymentMethod,
      delivery: cart.deliveryData.delivery.DeliveryType,
    };
  }, [deliveryDetails, subtotal, total, agreed, paymentMethod, cart.deliveryData.delivery.DeliveryType]);
  const {
    isValid,
    errors,
    firstError,
  } = useValidation(validationSchema, validationValues);

  useEffect(() => {
    if (cart.deliveryData.delivery.DeliveryType !== 'Pickup' && subtotal < 300) {
      notifications.queue({
        type: 'error',
        title: 'Order Below Minimum',
        text: 'The minimum order for delivery is ₱300.',
      });
    }
  }, [cart.deliveryData.delivery.DeliveryType, subtotal, notifications]);

  const paymentOptions = [
    <option value="COD">Cash{cart.deliveryData.delivery.DeliveryType === 'Pickup' ? '' : ' on Delivery'}</option>,
  ]

  if (process.env.GATSBY_SITE !== 'pt') {
    paymentOptions.push(
      ...[
        <option value="Card">Credit/Debit Card {cart.deliveryData.delivery.DeliveryType === 'Pickup' ? 'in store' : 'on Delivery'}</option>,
        <option value="GCashScan">GCash {cart.deliveryData.delivery.DeliveryType === 'Pickup' ? 'in store' : 'Scan to Pay'}</option>,
      ],
    );
  }

  paymentOptions.push(...[
    <option value="Online">Online Payment</option>,
  ]);

  return (
    <CartContainer id="cart">
      <SummaryContainer style={{ flex: 2, marginLeft: 0 }}>
        <SummaryItem branding={branding}>
          <div style={{ textAlign: 'left' }}>
            <h2>Customer Information</h2>
            <TextInput
              label="First Name"
              name="first_name"
              value={deliveryDetails.FirstName || ''}
              onChange={(FirstName) => setDeliveryDetails((details) => ({ ...details, FirstName }))}
              required
              error={firstError('FirstName')}
            />
            <TextInput
              label="Last Name"
              name="last_name"
              value={deliveryDetails.LastName || ''}
              onChange={(LastName) => setDeliveryDetails((details) => ({ ...details, LastName }))}
              required
              error={firstError('LastName')}
            />
            <TextInput
              label="Email"
              name="email"
              value={deliveryDetails.Email || ''}
              onChange={(Email) => setDeliveryDetails((details) => ({ ...details, Email }))}
              type="email"
              required
              error={firstError('Email')}
            />
            <TextInput
              label="Contact Number"
              name="contact_number"
              value={deliveryDetails.ContactNumber || ''}
              onChange={(ContactNumber) => setDeliveryDetails((details) => ({ ...details, ContactNumber }))}
              required
              error={firstError('ContactNumber')}
            />
            <TextInput
              label="Recipient Name (if different)"
              name="recipient_name"
              value={deliveryDetails.RecipientName || ''}
              onChange={(RecipientName) => setDeliveryDetails((details) => ({ ...details, RecipientName }))}
              error={firstError('RecipientName')}
            />
            <TextInput
              label="Recipient Contact Number (if different)"
              name="recipient_number"
              value={deliveryDetails.RecipientContactNumber || ''}
              onChange={(RecipientContactNumber) => setDeliveryDetails((details) => ({ ...details, RecipientContactNumber }))}
              error={firstError('RecipientContactNumber')}
            />
            <div style={{ textAlign: 'left', marginTop: '1em' }}>
              {cart.deliveryData.delivery.DeliveryType === 'Pickup' ? (
                <>
                  <h3>Pickup At</h3>
                  <div>
                    <div>
                      <b>{cart.deliveryData.address.PickupLocationName}</b> Branch
                      </div>
                    <div>
                      {cart.deliveryData.address.PickupLocationAddress}
                    </div>
                    {cart.deliveryData.delivery.DeliveryDate && `On ${moment(cart.deliveryData.delivery.DeliveryDate).format('LLL')}`}
                  </div>
                </>
              ) : (
                <>
                  <h3>Deliver To</h3>
                  <div>
                    {[
                      cart.deliveryData.address.CompanyName,
                      cart.deliveryData.address.UnitNumber,
                      cart.deliveryData.address.StreetAddress,
                      cart.deliveryData.address.Barangay,
                      cart.deliveryData.address.City,
                      cart.deliveryData.address.Region,
                      cart.deliveryData.address.ZipCode,
                    ].filter(Boolean).map((part) => (
                      <div key={part}>
                        {part}
                      </div>
                    ))}
                    {cart.deliveryData.delivery.DeliveryDate && `On ${moment(cart.deliveryData.delivery.DeliveryDate).format('LLL')}`}
                  </div>
                </>
              )}
              <a
                href="/#"
                style={{ float: 'right' }}
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  cart.editDeliveryData();
                }}
              >
                Edit
              </a>
            </div>
          </div>
        </SummaryItem>
        <SummaryItem branding={branding}>
          <div style={{ textAlign: 'left' }}>
            <h2>Discount</h2>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <TextInput
                label="PWD ID No."
                value={deliveryDetails.PwdId || ''}
                onChange={(PwdId) => setDeliveryDetails((details) => ({ ...details, PwdId }))}
              />
              <TextInput
                label="Senior Citizen ID No."
                value={deliveryDetails.ScdId || ''}
                onChange={(ScdId) => setDeliveryDetails((details) => ({ ...details, ScdId }))}
                style={{ marginLeft: '1em' }}
              />
            </div>
          </div>
        </SummaryItem>
        <SummaryItem branding={branding}>
          <div style={{ textAlign: 'left' }}>
            <h2>Order Notes</h2>
            <TextInput
              multiline
              label="Special Instructions"
              value={deliveryDetails.SpecialInstructions || ''}
              onChange={(SpecialInstructions) => setDeliveryDetails((details) => ({ ...details, SpecialInstructions }))}
              maxLength={256}
            />
          </div>
        </SummaryItem>
      </SummaryContainer>
      <SummaryContainer style={{ flex: 1 }}>
        <Form
          submitText="Checkout"
          valid={isValid}
          validationError={
            errors.length ? (
              errors.map(({ message }) => message).join('\n')
            ) : null
          }
          errorProps={{
            style: {
              whiteSpace: 'pre-line',
            },
          }}
          onSubmit={async () => {
            if (user.token) {
              const data = await api.updateDetails({
                token: user.token,
                userData: {
                  FirstName: deliveryDetails.FirstName,
                  LastName: deliveryDetails.LastName,
                  Phone: deliveryDetails.ContactNumber,
                },
              });

              user.updateUserData(data);
            }

            const { id, OrderNumber, PaymentMethod } = await api.placeOrder({
              token: user.token,
              cart: cart.items,
              address: cart.deliveryData.address,
              delivery: {
                ...deliveryDetails,
                ...cart.deliveryData.delivery,
                PaymentMethod: paymentMethod,
              },
              messengerId,
            });

            localStorage.setItem('last-order', OrderNumber);
            console.log(messengerId, window.top != window);

            if (PaymentMethod === 'Online' && total) {
              window.location.href = '/orders/' + id + '/pay';
            } else {
              if (messengerId) {
                navigate('/thank-you-messenger');
              } else {
                navigate('/thank-you');
              }
            }
          }}
        >
          <Cart editable={editable || cart.DeliveryType === 'Pickup'} />
          <SummaryItem branding={branding}>
            <div>Subtotal</div>
            <div>₱{subtotal.toLocaleString()}</div>
          </SummaryItem>
          {cart.deliveryData.delivery.DeliveryType !== 'Pickup' ? (
            <SummaryItem branding={branding}>
              <div>Delivery Fee</div>
              <div>₱{cart.deliveryFee.toFixed(0)}</div>
            </SummaryItem>
          ) : null}
          {voucherDiscount ? (
            <SummaryItem branding={branding}>
              <div>Voucher Discount</div>
              <div>{`-₱${(voucherDiscount).toLocaleString()}`}</div>
            </SummaryItem>
          ) : null}
          <SummaryItem branding={branding}>
            <div>Total</div>
            <div>{`₱${(total).toLocaleString()}`}</div>
          </SummaryItem>
          <SummaryItem branding={branding} style={{ alignItems: 'center' }}>
            <div style={{ marginRight: '0.5em' }}>Payment Options</div>
            <div>
              <SelectInput
                value={paymentMethod}
                onChange={setPaymentMethod}
              >
                {paymentOptions}
              </SelectInput>
            </div>
          </SummaryItem>
          {paymentMethod === 'COD' ? (
            <SummaryItem branding={branding}>
              <TextInput
                label="Change For"
                value={deliveryDetails.ChangeFor || ''}
                onChange={(ChangeFor) => setDeliveryDetails((details) => ({ ...details, ChangeFor }))}
              />
            </SummaryItem>
          ) : null}
          {(paymentMethod === 'GCashScan' && cart.deliveryData.delivery.DeliveryType !== 'Pickup') ? (
            <SummaryItem branding={branding}>
              <small>
                * Rider will bring the QR code for you to scan
              </small>
            </SummaryItem>
          ) : null}
          <SummaryItem branding={branding}>
            <div style={{ textAlign: 'left' }}>
              <AcceptTerms value={agreed} onChange={setAgreed} hasPwd={deliveryDetails.PwdId || deliveryDetails.ScdId} />
            </div>
          </SummaryItem>
          <div style={{ height: '0.5em' }} />
        </Form>
      </SummaryContainer>
    </CartContainer>
  );
};

const CheckoutWizard = () => {
  const cart = useCart();
  const [guestOrder, setIsGuestOrder] = useState(cart?.deliveryData?.guestOrder);
  const isAuthenticated = useIsAuthenticated();

  useEffect(() => {
    cart.setDeliveryData((data) => ({ ...data, guestOrder }));
  }, [guestOrder]);

  if (!isAuthenticated && !guestOrder) {
    return (
      <AuthForm>
        <div style={{ textAlign: 'right' }}>
          <Button onClick={() => setIsGuestOrder(true)} style={{ width: '100%' }}>
            Order as Guest
          </Button>
        </div>
      </AuthForm>
    );
  }

  return (
    <CheckoutFlow />
  );
};

const CheckoutPage = () => {

  return (
    <Layout title="Checkout">
      <MainContainer>
        <AvailabilityProvider>
          <CheckoutWizard />
        </AvailabilityProvider>
      </MainContainer>
    </Layout>
  );
};

export default CheckoutPage;
