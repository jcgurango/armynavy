import { navigate } from 'gatsby-link';
import React, { useEffect, useState } from 'react';
import api from '../../api';
import ErrorText from '../../components/error-text';

import Layout from '../../components/layout';
import MainContainer from '../../components/main-container';
import { useNotifications } from '../../components/providers/notification-provider';

const EmailConfirmation = () => {
  const notifications = useNotifications();
  const [error, setError] = useState('');

  useEffect(() => {
    (async () => {
      try {
        await api.confirmAccount({
          token: String(window.location.hash).substring(1),
        });
        
        notifications.queue({
          type: 'info',
          title: 'Email Confirmed',
          text: 'Your email has been confirmed. You may now log in.',
        });

        navigate('/');
      } catch (e) {
        setError(e.message);
      }
    })();
  }, [notifications]);

  return (
    <Layout>
      <MainContainer>
        {error ? (
          <ErrorText style={{ textAlign: 'inherit' }}>
            {error}
          </ErrorText>
        ) : (
          <>
            Confirming your account. Please wait...
          </>
        )}
      </MainContainer>
    </Layout>
  );
};

export default EmailConfirmation;
