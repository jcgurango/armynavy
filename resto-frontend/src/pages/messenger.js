import React, { createContext, useContext, useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import BrandingProvider, { useBranding } from '../components/providers/branding-provider';
import { GlobalStyle } from '../components/layout';
import { CheckoutFlow } from './checkout';
import AvailabilityProvider from '../components/providers/availability-provider';
import MenuProvider, { useMenu } from '../components/providers/menu-provider';
import Button from '../components/button';
import { useCart } from '../components/providers/cart-provider';
import Product from '../components/menu/product';

const MessengerContainer = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  flex-direction: column;
  background-color: rgb(245, 245, 245);
`;

const TopBarContainer = styled.div`
  background-color: white;
  border-bottom: 1px solid rgb(220, 220, 220);
`;

const CategoryLinksContainer = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 0.75em;
  padding-left: 0.75em;
  padding-bottom: 0.75em;
  width: 100%;
  overflow-x: auto;
  box-sizing: border-box;
`;

const CategoryLink = styled.a`
  padding: 0.25em 1em;
  background-color: rgb(230, 230, 230);
  border-radius: 1em;
  margin-right: 1em;
  color: inherit;
  white-space: pre;

  &.active {
    background-color: rgb(30, 153, 251);
    color: white;
    border-radius: 1em;
    margin-right: 1em;
    font-weight: bold;
  }
`;

const CategoryContainer = styled.div`
  margin-top: 1em;
  background-color: white;
  padding: 1em;
`;

const ProductContainer = styled.a`
  display: block;
  color: inherit;
  margin-top: 0.5em;
  margin-bottom: 0.5em;
  border-bottom: 1px solid rgb(220, 220, 220);
  display: flex;
  flex-direction: row;
  padding-bottom: 0.5em;

  &:last-child {
    border-bottom: 0px;
  }
`;

const OrderButtonContainer = styled.div`
  background-color: white;
  padding: 1em;
  border-top: 1px solid rgb(220, 220, 220);
  margin-top: 1em;
`;

const DisplayOverlay = styled.div`
  position: fixed;
  inset: 0;
  transition: 0.25s;
  background-color: transparent;

  &.visible {
    background-color: rgba(0, 0, 0, 0.2);
  }
`;

const ModalContainer = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  transition: 0.25s;
  transform: translate(0%, 100%);

  &.visible {
    transition: 0.25s;
    transform: translate(0%, 0%);
  }
`;

const ProductDisplayContainerHeader = styled.div`
  background-color: white;
  padding: 1em;
  border-top-left-radius: 0.75em;
  border-top-right-radius: 0.75em;
  border-bottom: 1px solid rgb(220, 220, 220);
  text-align: center;

  h3 {
    margin: 0;
  }
`;

const TopBarContext = createContext({
  container: null,
});

const MainContainerContext = createContext({
  container: null,
});

export const TopBarContent = ({
  children,
}) => {
  const { container } = useContext(TopBarContext);

  if (!container) {
    return children || null;
  }

  return ReactDOM.createPortal(children, container);
};

const MessengerModal = ({
  onClose = () => { },
  visible = false,
  children,
}) => {
  const [display, setDisplay] = useState('none');
  const [hasVisible, setHasVisible] = useState(false);

  useEffect(() => {
    if (visible) {
      setDisplay('initial');

      const timeout = setTimeout(() => {
        setHasVisible(true);
      }, 5);

      return () => {
        clearTimeout(timeout);
      };
    } else {
      setHasVisible(false);

      const timeout = setTimeout(() => {
        setDisplay('none');
      }, 250);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [visible]);

  return (
    <>
      <DisplayOverlay
        style={{ display }}
        className={hasVisible ? 'visible' : ''}
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          onClose();
        }}
      />
      <ModalContainer className={visible ? 'visible' : ''}>
        {children}
      </ModalContainer>
    </>
  );
};

const ProductDisplay = ({
  onClose = () => { },
  visible = false,
  productId,
}) => {
  const menu = useMenu();
  const product = menu.Products.find(({ id }) => id == productId);

  let content = null;

  if (product) {
    content = (
      <ModalContainer className={visible ? 'visible' : ''}>
        <ProductDisplayContainerHeader>
          <h3>
            {product.Name}
          </h3>
          <a
            href="/#"
            style={{
              fontSize: '2em',
              position: 'absolute',
              right: '0.5em',
              top: '0.25em',
              color: 'inherit',
            }}
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              onClose();
            }}
          >
            ×
          </a>
        </ProductDisplayContainerHeader>
        <div style={{ overflow: 'auto', backgroundColor: 'white', maxHeight: '65vh', paddingLeft: '1em', paddingRight: '1em' }}>
          <Product
            product={product}
            category={{ Metadata: {} }}
            inline
            onAdd={onClose}
          />
        </div>
      </ModalContainer>
    );
  }

  return (
    <MessengerModal onClose={onClose} visible={visible}>
      {content}
    </MessengerModal>
  );
};

const MessengerMenu = () => {
  const menu = useMenu();
  const { container: mainContent } = useContext(MainContainerContext);
  const [checkingOut, setCheckingOut] = useState(false);
  const [currentCat, setCurrentCat] = useState(menu.Categories[0] && menu.Categories[0].Slug);
  const [productId, setProductId] = useState(null);
  const cart = useCart();
  const branding = useBranding();

  const placeOrderContainer = (
    <>
      {cart.calculateTotal(cart.items) > 0 ? (
        <OrderButtonContainer>
          <Button
            style={{ width: '100%' }}
            onClick={() => {
              setCheckingOut(true);
            }}
          >
            View Cart (₱{cart.calculateTotal(cart.items).toFixed(2)})
          </Button>
        </OrderButtonContainer>
      ) : null}
    </>
  );

  useEffect(() => {
    if (mainContent) {
      const {
        y: scrollOffset,
      } = mainContent.getBoundingClientRect();

      const scroll = (e) => {
        const categoryContainers = mainContent.querySelectorAll('.category-container');

        for (let i = 0; i < categoryContainers.length; i++) {
          const {
            y: top,
            height: height,
          } = categoryContainers[i].getBoundingClientRect();

          if ((top + height - (scrollOffset * 2)) > 0) {
            setCurrentCat(categoryContainers[i].getAttribute('data-slug'));
            return;
          }
        }
      };

      mainContent.addEventListener('scroll', scroll);

      return () => {
        mainContent.removeEventListener('scroll', scroll);
      };
    }
  }, [mainContent]);

  useEffect(() => {
    if (currentCat) {
      const link = document.querySelector(`a.category-link[data-slug=${currentCat}]`);

      if (link) {
        link.scrollIntoView();
      }
    }
  }, [currentCat]);

  return (
    <>
      <TopBarContent>
        <CategoryLinksContainer>
          {menu.Categories.map(({ strapiId, Category, Slug }) => (
            <CategoryLink
              key={strapiId}
              className={`category-link ${currentCat === Slug ? 'active' : ''}`}
              data-slug={Slug}
              href="/#"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                const container = document.querySelector(`.category-container[data-slug=${Slug}]`);
                console.log(container);

                if (container) {
                  container.scrollIntoView();
                }
              }}
            >
              {Category}
            </CategoryLink>
          ))}
        </CategoryLinksContainer>
      </TopBarContent>
      {menu.Categories.map(({ strapiId, Category, Slug }) => (
        <CategoryContainer key={strapiId} className={`category-container`} data-slug={Slug}>
          <h1>{Category}</h1>
          {menu.Products.filter(({ Categories }) => Categories.find(({ id }) => id == strapiId)).map(({ id, Name, Description, Variations }) => (
            <ProductContainer
              key={id}
              onClick={(e) => {
                e.stopPropagation();
                e.preventDefault();
                setProductId(id);
              }}
            >
              <div>
                <GatsbyImage
                  image={getImage(Variations[0].MessengerImage)}
                  style={{
                    borderRadius: '100px'
                  }}
                />
              </div>
              <div style={{ flex: 1, marginLeft: '1em' }}>
                <h3>{Name}</h3>
                <p style={{ whiteSpace: 'pre-wrap' }}>{Description}</p>
                <h4>
                  ₱{Number(Variations[0].Price).toFixed(2)}
                </h4>
              </div>
            </ProductContainer>
          ))}
        </CategoryContainer>
      ))}
      {placeOrderContainer}
      <div style={{ position: 'fixed', left: 0, right: 0, bottom: 0 }}>
        {placeOrderContainer}
      </div>
      <ProductDisplay
        visible={!!productId}
        productId={productId}
        onClose={() => setProductId(null)}
      />
      <MessengerModal
        onClose={() => setCheckingOut(false)}
        visible={checkingOut}
      >
        <ProductDisplayContainerHeader>
          <h3>
            Checkout
          </h3>
          <a
            href="/#"
            style={{
              fontSize: '2em',
              position: 'absolute',
              right: '0.5em',
              top: '0.25em',
              color: 'inherit',
            }}
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              setCheckingOut(false);
            }}
          >
            ×
          </a>
        </ProductDisplayContainerHeader>
        <div style={{ overflow: 'auto', backgroundColor: 'white', maxHeight: '65vh', paddingLeft: '1em', paddingRight: '1em' }}>
          <CheckoutFlow editable messengerId={branding.isMessenger} />
        </div>
      </MessengerModal>
    </>
  );
};

const MessengerMiniApp = () => {
  const [topBar, setTopBar] = useState(null);
  const mainContentRef = useRef();

  return (
    <BrandingProvider
      override={(theme) => ({
        ...theme,
        Theme: {
          ...theme.Theme,
          Colors: {
            ...theme.Theme.Colors,
            primary: 'rgb(30, 153, 251)',
            secondary: 'rgb(228, 230, 235)',
          },
        },
        isMessenger: typeof(window) !== 'undefined' ? window.location.hash.substring(1) : true,
      })}
    >
      <Helmet>
        <title>Army Navy - Order</title>
      </Helmet>
      <GlobalStyle />
      <MessengerContainer>
        <TopBarContainer
          ref={(ref) => {
            setTopBar(ref);
          }}
        />
        <div style={{ flex: 1, overflow: 'auto' }} ref={mainContentRef}>
          <TopBarContext.Provider value={{ container: topBar }}>
            <MainContainerContext.Provider value={{ container: mainContentRef.current }}>
              <MenuProvider>
                <AvailabilityProvider>
                  <MessengerMenu />
                </AvailabilityProvider>
              </MenuProvider>
            </MainContainerContext.Provider>
          </TopBarContext.Provider>
        </div>
      </MessengerContainer>
    </BrandingProvider>
  );
};

export default MessengerMiniApp;
