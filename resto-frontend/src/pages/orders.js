import React, { useCallback, useEffect, useState } from 'react';

import api from '../api';
import ProfileLayout from '../components/profile/profile-layout';
import { useNotifications } from '../components/providers/notification-provider';
import { useUser } from '../components/providers/user-provider';
import { Fragment } from 'react';
import Button from '../components/button';
import moment from 'moment';
import styled from 'styled-components';

const Table = styled.table`
  .show-mobile {
    display: none;
  }

  @media only screen and (max-width: 900px) {
    .show-mobile {
      display: block;
    }

    .hide-mobile {
      display: none;
    }

    td, th {
      border-bottom: 1px solid rgb(180, 180, 180);
    }
  }
`;

const Page = () => {
  const user = useUser();
  const notifications = useNotifications();
  const [orders, setOrders] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [hasMore, setHasMore] = useState(true);
  const [openRows, setOpenRows] = useState([]);

  const loadMore = useCallback(() => {
    (async () => {
      setLoading(true);

      try {
        const orders = await api.orders(user.token, page);

        setOrders((o) => o.concat(orders));

        setPage((p) => (p + 1));

        if (orders.length < 10) {
          setHasMore(false);
        }
      } catch (e) {
        notifications.queue({
          type: 'error',
          title: 'Error',
          text: e.message,
        });
      }

      setLoading(false);
    })();
  }, [page]);

  useEffect(() => {
    loadMore();
  }, []);

  return (
    <Table>
      <thead>
        <tr>
          <th>Order #</th>
          <th className="hide-mobile">Date</th>
          <th className="hide-mobile">Recipient</th>
          <th className="hide-mobile">Type</th>
          <th className="hide-mobile">Status</th>
          <th className="hide-mobile">Total</th>
        </tr>
      </thead>
      <tbody>
        {orders.map((order) => {
          let total = order.OrderItems.reduce((last, item) => {
            let itemTotal = Number(item.Price);

            item.Addons.forEach((addon) => {
              itemTotal += Number(addon.Price);
            });

            return last + (itemTotal * item.Quantity);
          }, 0);
          const delivery = order.Type !== 'Pickup';

          if (delivery) {
            total += 50;
          }

          return (
            <Fragment key={order.OrderNumber}>
              <tr>
                <td>
                  <a
                    href={`/orders/${order.OrderNumber}`}
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();

                      setOpenRows((rows) => {
                        return rows.indexOf(order.OrderNumber) > -1
                          ? rows.filter((i) => i !== order.OrderNumber)
                          : rows.concat(order.OrderNumber);
                      });
                    }}
                  >
                    {order.OrderNumber}
                  </a>
                  <div className="show-mobile">
                    <div>Date: {moment(order.Date).format('LLL')}</div>
                    <div>Recipient: {order.RecipientName || `${order.FirstName} ${order.LastName}`}</div>
                    <div>Type: {order.Type === 'Pickup' ? 'Pickup' : 'Delivery'}</div>
                    <div>Status: {order.Status}</div>
                    <div>Total: {total.toFixed(2)}</div>
                    <div>
                      <a
                        href={`/orders/${order.OrderNumber}`}
                        onClick={(e) => {
                          e.preventDefault();
                          e.stopPropagation();

                          setOpenRows((rows) => {
                            return rows.indexOf(order.OrderNumber) > -1
                              ? rows.filter((i) => i !== order.OrderNumber)
                              : rows.concat(order.OrderNumber);
                          });
                        }}
                      >
                        Details
                      </a>
                    </div>
                  </div>
                </td>
                <td className="hide-mobile">
                  {moment(order.Date).format('LLL')}
                </td>
                <td className="hide-mobile">
                  {order.RecipientName || `${order.FirstName} ${order.LastName}`}
                </td>
                <td className="hide-mobile">
                  {order.Type === 'Pickup' ? 'Pickup' : 'Delivery'}
                </td>
                <td className="hide-mobile">
                  {order.Status}
                </td>
                <td className="hide-mobile" style={{ textAlign: 'center' }}>
                  {total.toFixed(2)}
                </td>
              </tr>
              {openRows.indexOf(order.OrderNumber) > -1 ? (
                <tr>
                  <td colspan="6">
                    <table style={{ padding: '0.5em', border: '1px solid rgb(200, 200, 200)' }}>
                      <thead>
                        <tr>
                          <th>Item</th>
                          <th>Price</th>
                          <th className="hide-mobile">Quantity</th>
                          <th className="hide-mobile">Subtotal</th>
                        </tr>
                      </thead>
                      <tbody>
                        {order.OrderItems.map((item) => {
                          let totalPrice = Number(item.Price);

                          item.Addons.forEach((addon) => {
                            totalPrice += Number(addon.Price);
                          });

                          return (
                            <tr key={item.id}>
                              <td>
                                {item.ProductName}
                                {item.Addons.map(({ id, Name, Price }) => (
                                  <small style={{ display: 'block' }} key={id}>
                                    +{Name} (₱{Number(Price).toFixed(2)})
                                  </small>
                                ))}
                              </td>
                              <td>
                                {totalPrice.toFixed(2)}
                                <div className="show-mobile">
                                  x {item.Quantity} ({(totalPrice * item.Quantity).toFixed(2)})
                                </div>
                              </td>
                              <td className="hide-mobile">
                                {item.Quantity}
                              </td>
                              <td className="hide-mobile">
                                {(totalPrice * item.Quantity).toFixed(2)}
                              </td>
                            </tr>
                          );
                        })}
                        {delivery ? (
                          <tr>
                            <td>
                              Delivery Fee
                            </td>
                            <td className="hide-mobile" />
                            <td className="hide-mobile" />
                            <td>
                              50.00
                            </td>
                          </tr>
                        ) : null}
                      </tbody>
                    </table>
                  </td>
                </tr>
              ) : null}
            </Fragment>
          );
        })}
      </tbody>
      {hasMore ? (
        <tfoot>
          <tr>
            <td colspan="6">
              <Button buttonType="button" disabled={loading} onClick={loadMore}>
                Load More
              </Button>
            </td>
          </tr>
        </tfoot>
      ) : null}
    </Table>
  );
};

const OrdersPage = () => {
  return (
    <ProfileLayout>
      <Page />
    </ProfileLayout>
  );
};

export default OrdersPage;
