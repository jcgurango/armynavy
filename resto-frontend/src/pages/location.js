import React, { useEffect, useMemo, useRef, useState } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import { navigate } from 'gatsby-link';
import moment from 'moment';
import styled from 'styled-components';
import { MapContainer, TileLayer, Marker, Popup, useMap } from 'react-leaflet';

import api from '../api';
import Form from '../components/form';
import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import { useNotifications } from '../components/providers/notification-provider';
import SelectInput from '../components/select-input';
import { Helmet } from 'react-helmet';
import { Fragment } from 'react';
import TextInput from '../components/text-input';

const LocationsContainer = styled.div`
  max-height: 450px;
  overflow-y: auto;
`;

const LocationContainer = styled.a`
  display: block;
  border-bottom: 1px solid rgba(0, 0, 0, 0.25);
  padding: 1em;
  text-align: center;
  color: inherit;
`;

const PageContainer = styled.div`
  display: flex;
  flex-direction: column;

  > :first-child {
    flex: 2;
    margin-right: 1em;
  }

  > :last-child {
    flex: 5;
  }

  @media only screen and (min-width: 800px) {
    flex-direction: row;
  }
`;

const MapController = ({
  center,
  openPopup,
}) => {
  const persistent = useMemo(() => ({ moved: false }), []);
  const map = useMap();

  useEffect(() => {
    map.setView(center, persistent.moved ? 15 : 5);
    persistent.moved = true;
  }, [center]);

  useEffect(() => {
    if (openPopup) {
      map.eachLayer((layer) => {
        if (layer.getLatLng) {
          const {
            lat,
            lng,
          } = layer.getLatLng();

          if (lat == openPopup[0] && lng == openPopup[1]) {
            layer.openPopup();
          }
        }
      });
    }
  }, [openPopup]);

  return null;
};

const LocationsPage = () => {
  const {
    siteStoreLocations: locations,
  } = useStaticQuery(graphql`
    query {
      siteStoreLocations {
        id
        Location
        FullAddress
        City
        StoreOpen
        StoreClose
        Telephone
        Mobile
        Open247
        Latitude
        Longitude
        Delivers
      }
    }
  `);

  const mapRef = useRef();
  const [delivery, setDelivery] = useState(false);
  const [open247, setOpen247] = useState(false);
  const [openNow, setOpenNow] = useState(false);
  const [city, setCity] = useState('');
  const [center, setCenter] = useState([13.0435216, 120.1733553]);
  const [search, setSearch] = useState('');
  const [openPopup, setOpenPopup] = useState(null);

  const cities = useMemo(() => {
    const cities = [];

    locations.forEach(({ City }) => {
      if (cities.indexOf(City) === -1) {
        cities.push(City);
      }
    })

    return cities;
  }, [locations]);

  const filteredLocations = useMemo(() => {
    return locations.filter(({ Delivers, Open247, StoreOpen, StoreClose, City, Location, FullAddress }) => {
      if (delivery && !Delivers) {
        return false;
      }

      if (open247 && !Open247) {
        return false;
      }

      if (openNow && !Open247) {
        const storeOpen = moment(moment().format('YYYY-MM-DD') + ' ' + StoreOpen);
        const storeClose = moment(moment().format('YYYY-MM-DD') + ' ' + StoreClose);

        if (storeOpen.isAfter(moment()) || storeClose.isBefore(moment())) {
          return false;
        }
      }

      if (city && City !== city) {
        return false;
      }

      if (search && Location.toLowerCase().indexOf(search.toLowerCase()) === -1 && FullAddress.toLowerCase().indexOf(search.toLowerCase()) === -1) {
        return false;
      }

      return true;
    });
  }, [
    locations,
    delivery,
    open247,
    openNow,
    city,
    search,
  ]);

  const renderLocation = ({
    id,
    Location,
    FullAddress,
    City,
    StoreOpen,
    StoreClose,
    Telephone,
    Mobile,
    Open247,
    handleClick = () => { },
  }) => {
    const storeHours = Open247 ? 'Open 24/7' : `${moment('2000-01-01 ' + StoreOpen).format('h:mm A')} - ${moment('2000-01-01 ' + StoreClose).format('h:mm A')}`;

    return (
      <LocationContainer
        key={id}
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          handleClick();
        }}
        onTouchStart={(e) => {
          e.preventDefault();
          e.stopPropagation();
          handleClick();
        }}
        href="/#"
      >
        <div>
          <h3>{Location}</h3>
        </div>
        <div>
          {FullAddress}
        </div>
        <div>
          {City}
        </div>
        <div>
          Philippines
      </div>
        {Telephone ? (
          <div>{Telephone}</div>
        ) : null}
        {Mobile ? (
          <div>{Mobile}</div>
        ) : null}
        <div style={{ height: '0.5em' }} />
        <h4>Opening Hours</h4>
        <div>
          Mon: {storeHours}<br />
        Tue: {storeHours}<br />
        Wed: {storeHours}<br />
        Thu: {storeHours}<br />
        Fri: {storeHours}<br />
        Sat: {storeHours}<br />
        Sun: {storeHours}
        </div>
      </LocationContainer>
    );
  };

  return (
    <Layout title="Locations">
      <MainContainer>
        <PageContainer>
          <div>
            <h2>Filters</h2>
            <div>
              <label>
                <input type="checkbox" onChange={(e) => setDelivery(e.target.checked)} checked={delivery} />
              Delivery
            </label>
            </div>
            <div>
              <label>
                <input type="checkbox" onChange={(e) => setOpen247(e.target.checked)} checked={open247} />
              Open 24/7
            </label>
            </div>
            <div style={{ marginBottom: '1em' }}>
              <label>
                <input type="checkbox" onChange={(e) => setOpenNow(e.target.checked)} checked={openNow} />
              Open Now
            </label>
            </div>
            <div>
              <SelectInput
                onChange={setCity}
                value={city}
                label="Select a city..."
                allowNull
              >
                {cities.map((city) => (
                  <option value={city} key={city}>{city}</option>
                ))}
              </SelectInput>
            </div>
            <div style={{ marginBottom: '1em' }}>
              <TextInput
                label="Filter by name..."
                value={search}
                onChange={setSearch}
              />
            </div>
            <b>Locations (Tap/Click to View)</b>
            <LocationsContainer>
              {filteredLocations.map((location) => renderLocation({
                ...location,
                handleClick: () => {
                  setCenter([location.Latitude, location.Longitude]);
                  setOpenPopup([location.Latitude, location.Longitude]);
                },
              }))}
              {filteredLocations.length === 0 ? (
                <LocationContainer>
                  No locations found matching selected filters.
                </LocationContainer>
              ) : null}
            </LocationsContainer>
          </div>
          <div style={{ height: '700px' }}>
            {typeof (window) !== 'undefined' ? (
              <MapContainer center={center} zoom={5} scrollWheelZoom={false} style={{ width: '100%', height: '700px' }} ref={mapRef}>
                <TileLayer
                  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {filteredLocations.map((location) => {
                  const { id, Latitude, Longitude } = location;

                  if ((typeof(Latitude) === 'string' && !Latitude.trim()) || (typeof(Longitude) === 'string' && !Longitude.trim()) || (!Latitude || !Longitude)) {
                    return null;
                  }

                  return (
                    <Fragment key={id}>
                      <Marker position={[Latitude, Longitude]}>
                        <Popup>
                          {renderLocation(location)}
                          <a
                            href={`https://www.google.com/maps/search/?api=1&query=${Latitude},${Longitude}`}
                            style={{
                              display: 'block',
                              padding: '1em',
                              textAlign: 'center',
                            }}
                            target="_blank"
                            rel="noopener noreferer"
                          >
                            Click to Get Directions
                          </a>
                        </Popup>
                      </Marker>
                    </Fragment>
                  );
                })}
                <MapController center={center} openPopup={openPopup} />
              </MapContainer>
            ) : null}
          </div>
        </PageContainer>
      </MainContainer>
    </Layout>
  );
};

export default LocationsPage;
