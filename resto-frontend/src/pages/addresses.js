import React, { useState } from 'react';
import api from '../api';
import DeliveryAddress from '../components/forms/delivery-address';

import Pills from '../components/pills';
import ProfileLayout from '../components/profile/profile-layout';
import { useBranding } from '../components/providers/branding-provider';
import { useNotifications } from '../components/providers/notification-provider';
import { useUser } from '../components/providers/user-provider';

const AddressesPage = () => {
  const [addressType, setAddressType] = useState('Home');
  const user = useUser();
  const notifications = useNotifications();
  const branding = useBranding();

  return (
    <ProfileLayout>
      <div style={{ marginLeft: 'auto', marginRight: 'auto', maxWidth: '500px', padding: '1em', boxSizing: 'border-box' }}>
        <Pills withBg activeColor={branding.Theme.Colors.primary} activeTextColor={process.env.GATSBY_SITE === 'rr' ? 'white' : null}>
          <a
            href="/#"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              setAddressType('Home');
            }}
            className={addressType === 'Home' ? 'active' : null}
          >
            Home
        </a>
          <a
            href="/#"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              setAddressType('Office');
            }}
            className={addressType === 'Office' ? 'active' : null}
          >
            Office
        </a>
        </Pills>
        <DeliveryAddress
          addressType={addressType}
          onSubmit={async (address) => {
            const data = await api.updateAddress({
              token: user.token,
              address: {
                addressType,
                ...address,
              },
            });

            user.updateUserData(data);

            notifications.queue({
              type: 'info',
              title: 'Address Saved',
              text: addressType + ' address successfully saved.',
            });
          }}
          submitText="Save"
        />
      </div>
    </ProfileLayout>
  );
};

export default AddressesPage;
