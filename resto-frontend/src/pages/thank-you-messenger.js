import React, { useEffect } from 'react';
import { useCart } from '../components/providers/cart-provider';

const ThankYouPage = () => {
  const cart = useCart();

  useEffect(() => {
    cart.clear();
    localStorage.removeItem('delivery-session');

    if (typeof (window) !== 'undefined') {
      window.close();

      (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/messenger.Extensions.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'Messenger'));
  
      window.extAsyncInit = function () {
        window.MessengerExtensions.requestCloseBrowser();
      };
    }
  }, []);

  return (
    <div
      style={{ fontFamily: 'Arial, Helvetica, sans-serif', fontSize: '4em', textAlign: 'center', color: 'cornflowerblue', textDecoration: 'none', margin: '10%', marginTop: '20%', display: 'block' }}
    >
      Thank you! You may now close this window.
    </div>
  );
};

export default ThankYouPage;
