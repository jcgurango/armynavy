import React, { useState } from 'react';
import styled from 'styled-components';
import v from 'vlid';

import api from '../api';
import DeliveryAddress from '../components/forms/delivery-address';
import Pills from '../components/pills';
import ProfileLayout from '../components/profile/profile-layout';
import { useNotifications } from '../components/providers/notification-provider';
import { useUser } from '../components/providers/user-provider';
import TextInput from '../components/text-input';
import Form from '../components/form';
import useValidation from '../components/utils/useValidation';

const FlexRow = styled.div`
  display: flex;
  flex-direction: column;

  @media only screen and (min-width: 800px) {
    flex-direction: row;

    > :first-child {
      margin-left: 0em;
    }

    > * {
      margin-left: 0.5em;
    }
  }
`;

const validation = v.object({
  FirstName: v.string().required('Please enter a first name.'),
  LastName: v.string().required('Please enter a last name.'),
  Phone: v.string()
    .regex(/^0\d{10}$/, 'Phone number must be in the format "09170000000".'),
  username: v.string().required('Please enter your username.'),
  email: v.string().email('Please enter a valid email.'),
  Password: v.string().allow(null),
});

const Page = () => {
  const user = useUser();
  const notifications = useNotifications();

  const [userData, setUserData] = useState(() => {
    if (user && user.userData) {
      const {
        FirstName,
        LastName,
        Phone,
        username,
        email,
      } = user.userData;

      return {
        FirstName: FirstName || '',
        LastName: LastName || '',
        Phone: Phone || '',
        username: username || '',
        email: email || '',
        Password: '',
      };
    }

    return {
      FirstName: '',
      LastName: '',
      Phone: '',
      username: '',
      email: '',
      Password: '',
    };
  });

  const { isValid, errors } = useValidation(validation, userData);

  const updateField = (field) => {
    return {
      value: userData[field],
      onChange: (newValue) => setUserData((data) => ({
        ...data,
        [field]: newValue,
      })),
    };
  };

  return (
    <Form
      submitText="Save Changes"
      valid={isValid}
      validationError={errors.map(({ message }) => message).join('\n')}
      onSubmit={async () => {
        const data = await api.updateDetails({
          token: user.token,
          userData,
        });

        user.updateUserData(data);
        updateField('Password').onChange(null);

        notifications.queue({
          type: 'info',
          title: 'Updated',
          text: 'Your account details have been updated.',
        });
      }}
    >
      <FlexRow>
        <TextInput label="First Name" style={{ marginRight: '0.5em' }} {...updateField('FirstName')} />
        <TextInput label="Last Name" {...updateField('LastName')} />
      </FlexRow>
      <TextInput label="Display Name" {...updateField('username')} />
      <TextInput label="Email" type="email" {...updateField('email')} />
      <TextInput label="Phone Number" {...updateField('Phone')} />
      <div style={{ marginTop: '1em', marginBottom: '0.5em' }}>
        <b>Password Change</b>
      </div>
      <TextInput label="New Password" type="password" {...updateField('Password')} />
    </Form>
  );
};

const ProfilePage = () => {
  return (
    <ProfileLayout>
      <Page />
    </ProfileLayout>
  );
};

export default ProfilePage;
