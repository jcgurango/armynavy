import React from 'react';
import styled from 'styled-components';
import Button from '../components/button';

import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import Cart from '../components/menu/cart';
import { useBranding } from '../components/providers/branding-provider';
import { useCart } from '../components/providers/cart-provider';

const EmptyCartContainer = styled.main`
  ${({ branding }) => branding.Box(branding)};
`;

const CartContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media only screen and (min-width: 700px) {
    flex-direction: row;
  }
`;

const SummaryContainer = styled.div`
  @media only screen and (min-width: 700px) {
    margin-left: 1em;
  }

  @media only screen and (min-width: 1000px) {
    flex: 1;
  }
`;

const SummaryItem = styled.div`
  padding: 0.75em;
  border-bottom: 0.1em solid ${({ branding: { Theme: { Colors } } }) => Colors.primary};
  display: flex;
  flex-direction: row;

  > :last-child {
    flex: 3;
    text-align: right;
  }
`;

const CartPage = () => {
  const cart = useCart();
  const branding = useBranding();
  const subtotal = cart.calculateTotal(cart.items);

  return (
    <Layout title="Cart">
      <MainContainer>
        {cart.items.length ? (
          <CartContainer id="cart">
            <Cart editable>
              <SummaryItem branding={branding}>
                <div>Subtotal</div>
                <div>₱{subtotal.toLocaleString()}</div>
              </SummaryItem>
            </Cart>
            <SummaryContainer>
              <Button
                buttonType="link"
                to="/checkout"
                style={{ width: '100%', marginTop: '1em' }}
              >
                Proceed to Checkout
              </Button>
              <Button
                buttonType="link"
                to="/menu"
                style={{ width: '100%', marginTop: '1em' }}
                color="secondary"
              >
                Add More Items
              </Button>
            </SummaryContainer>
          </CartContainer>
        ) : (
          <EmptyCartContainer branding={branding} id="cart-empty">
            Your cart is currently empty.
          </EmptyCartContainer>
        )}
      </MainContainer>
    </Layout>
  );
};

export default CartPage;
