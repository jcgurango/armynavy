import { navigate } from 'gatsby-link';
import React, { useState } from 'react';
import api from '../api';
import Form from '../components/form';
import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import { useNotifications } from '../components/providers/notification-provider';
import TextInput from '../components/text-input';

const ResetPasswordPage = () => {
  const notifications = useNotifications();
  const match = /code=(\w+)/g.exec(typeof(window) !== 'undefined' ? window.location.search : '');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');

  if (typeof(window) !== 'undefined' && !match) {
    navigate('/forgot-password');

    return null;
  }

  let code = null;

  if (match) {
    const [, foundCode] = match;
    code = foundCode;
  }

  return (
    <Layout title="Reset Password">
      <MainContainer>
        <Form
          submitText="Reset Password"
          valid={!!(password && passwordConfirm)}
          onSubmit={async () => {
            await api.resetPassword({
              code,
              password,
              passwordConfirm,
            });

            notifications.queue({
              type: 'info',
              title: 'Success',
              text: 'Your password has been reset. You may now log in.',
            });

            navigate('/profile');
          }}
        >
          <p>Please enter a new password for your account.</p>
          <TextInput
            label="Password"
            name="password"
            type="password"
            value={password}
            onChange={setPassword}
          />
          <TextInput
            label="Confirm Password"
            name="password_confirm"
            type="password"
            value={passwordConfirm}
            onChange={setPasswordConfirm}
          />
        </Form>
      </MainContainer>
    </Layout>
  );
};

export default ResetPasswordPage;
