import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import api from '../api';
import Button from '../components/button';
import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import { useBranding } from '../components/providers/branding-provider';
import { useCart } from '../components/providers/cart-provider';
import { useUser } from '../components/providers/user-provider';

const LastOrderId = styled.span`
  font-size: 2em;
  font-weight: bold;

  @media only screen and (min-width: 800px) {
    font-size: 4em;
  }
`;

const Page = () => {
  const user = useUser();
  const [lastOrderId, setLastOrderId] = useState('...');
  const { Theme: { Colors: { primary } } } = useBranding();
  const cart = useCart();

  useEffect(() => {
    cart.clear();
    localStorage.removeItem('delivery-session');
  }, []);

  useEffect(() => {
    if (!user.token) {
      if (typeof(localStorage) !== 'undefined') {
        setLastOrderId(localStorage.getItem('last-order'));
      }
    } else {
      (async () => {
        const order = await api.lastOrder(user.token);
        setLastOrderId(order.OrderNumber);
      })();
    }
  }, []);

  return (
    <MainContainer style={{ textAlign: 'center' }}>
      <h1>Thank you for your order!</h1>
      <div style={{ height: '1em' }} />
      <h2 style={{ fontWeight: 'bold' }}>Order No.</h2>
      <LastOrderId style={{ color: primary }}>
        {lastOrderId}
      </LastOrderId>
      <p>
        was successfully placed.
      </p>
      <p>
        If you need any assistance, please feel free to call us at 8-333-3131.
      </p>
      <Button buttonType="link" to="/menu">
        Order More
      </Button>
    </MainContainer>
  );
};

const ThankYouPage = () => {
  return (
    <Layout title="Thank You">
      <Page />
    </Layout>
  );
};

export default ThankYouPage;
