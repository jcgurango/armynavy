import { graphql, Link, useStaticQuery } from 'gatsby';
import { GatsbyImage, getImage, StaticImage } from 'gatsby-plugin-image';
import React from 'react';
import styled from 'styled-components';
import Banners from '../components/banners';
import Button from '../components/button';

import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import BrandingProvider from '../components/providers/branding-provider';
import { useMenu } from '../components/providers/menu-provider';

const CategoriesContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-template-areas: ". .";
  gap: 0.5em 0.5em;
  ${() => process.env.GATSBY_SITE === 'rr' ? `
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
    grid-template-areas: ".";
  ` : null}
  ${() => process.env.GATSBY_SITE === 'an' ? `
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-areas: ".";

    @media only screen and (max-width: 900px) {
      grid-template-columns: 1fr 1fr;
    }
  ` : null}
  ${() => process.env.GATSBY_SITE === 'pt' ? `
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-areas: ".";
    gap: 2em 2em;
  ` : null}
`;

const HomePage = () => {
  const data = useStaticQuery(graphql`
    query {
      siteProductCategories {
        id
        Category
        Slug
        Homepage
        Image {
          childImageSharp {
            gatsbyImageData
          }
        }
        Position
      }
    }
  `);

  return (
    <Layout>
      <Banners />
      <MainContainer>
        <div style={{ textAlign: 'center', marginTop: '1.5em', marginBottom: '1.5em' }}>
          <Button buttonType="link" to="/menu">
            Order Now
          </Button>
        </div>
        {process.env.GATSBY_SITE === 'an' ? (
          <>
            <StaticImage src="../images/chevron-lines.png" alt="Army Navy brands" layout="fullWidth" />
            <div style={{ textAlign: 'center', marginTop: '5em', marginBottom: '2em', paddingLeft: '4em', paddingRight: '4em' }}>
              <StaticImage src="../images/what-will-it-be-today.png" alt="Army Navy brands" layout="constrained" width="500" />
            </div>
          </>
        ) : null}
        <CategoriesContainer>
          {data.siteProductCategories
            .slice()
            .sort((a, b) => (a.Position - b.Position))
            .filter((a) => a.Homepage === true || a.Homepage === null || a.Homepage === undefined)
            .map((category) => {
              return (
                <Link
                  key={category.id}
                  title={category.Category}
                  to={'/menu/' + category.Slug}
                >
                  <GatsbyImage
                    image={getImage(category.Image)}
                    alt={category.Category}
                  />
                </Link>
              );
            })}
        </CategoriesContainer>
        <div style={{ textAlign: 'center', marginTop: '1.5em', marginBottom: '0.5em' }}>
          <BrandingProvider
            override={(theme) => ({
              ...theme,
              Theme: {
                ...theme.Theme,
                Colors: {
                  ...theme.Theme.Colors,
                  primary: process.env.GATSBY_SITE === 'ltmt' ? 'rgb(255, 199, 162)' : theme.Theme.Colors.primary,
                },
              },
            })}
          >
            <Button buttonType="link" to="/location" color={['an', 'ww', 'pt'].includes(process.env.GATSBY_SITE) ? 'primary' : 'secondary'}>
              Find your store location
            </Button>
          </BrandingProvider>
        </div>
        {process.env.GATSBY_SITE === 'an' ? (
          <div style={{ textAlign: 'center', fontSize: '3em', fontWeight: 'bold' }}>
            8-333-3131
          </div>
        ) : null}
        {process.env.GATSBY_SITE === 'pt' ? (
          <div style={{ textAlign: 'center', fontSize: '3em', fontWeight: 'bold' }}>
            8-553-3333
          </div>
        ) : null}
      </MainContainer>
    </Layout>
  );
};

export default HomePage;
