const SiteConstants = {
  brand: 'LiberTea',
  websiteTitle: '',
  pageTrackingCode: `(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src="https://avd.innity.net/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "863", "607f8a3b47e704ac37000002");`,
  fbPixelId: '1123107511586856',
};

if (process.env.GATSBY_SITE === 'rr') {
  SiteConstants.brand = 'RiceRocket';
  SiteConstants.pageTrackingCode = `(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src="https://avd.innity.net/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "875", "60a4811d47e704203e000001");`;
  SiteConstants.fbPixelId = '726345648779978';
}

if (process.env.GATSBY_SITE === 'ww') {
  SiteConstants.brand = 'WrightWings';
  SiteConstants.pageTrackingCode = `(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src="https://avd.innity.net/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "878", "60b0504547e7043f40000000");`;
  SiteConstants.fbPixelId = '400888785259101';
}

if (process.env.GATSBY_SITE === 'an') {
  SiteConstants.brand = 'ArmyNavy';
  SiteConstants.websiteTitle = 'ArmyNavy Burger + Burrito';
  SiteConstants.pageTrackingCode = `(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src="https://avd.innity.net/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "643", "5d92c98247e704d26ea7e885");`;
  SiteConstants.fbPixelId = '329284452675142';
}

if (process.env.GATSBY_SITE === 'pt') {
  SiteConstants.brand = 'PizzaTelefono';
  SiteConstants.pageTrackingCode = `(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src="https://avd.innity.net/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "825", "600003dc47e704611b000000");`;
  SiteConstants.fbPixelId = '3648192675404252';
}

export default SiteConstants;
