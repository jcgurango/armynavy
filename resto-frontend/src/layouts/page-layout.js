import React from 'react';
import styled from 'styled-components';

import Layout from '../components/layout';
import MainContainer from '../components/main-container';

const PageTitle = styled.h1`
  text-align: center;
`;

const PageDescription = styled.small`
  display: block;
  text-align: center;
  color: rgb(150, 150, 150);
`;

const SideContentLayout = styled.div`
  display: flex;
  flex-direction: column;

  @media only screen and (min-width: 800px) {
    flex-direction: row;

    > * {
      flex: 1;
      margin-left: 1em;
    }

    > :first-child {
      margin-left: 0;
    }
  }
`;

const PageLayout = ({
  pageData: {
    Title,
    Description,
    MetaTitle,
    MetaDescription,
  },
  children,
  sideContent,
}) => {
  return (
    <Layout title={Title} metaTitle={MetaTitle} metaDescription={MetaDescription}>
      <MainContainer>
        <PageTitle>{Title}</PageTitle>
        {Description ? (
          <PageDescription>
            {Description}
          </PageDescription>
        ) : null}
        {sideContent ? (
          <SideContentLayout>
            <div>{sideContent}</div>
            <div>{children}</div>
          </SideContentLayout>
        ) : (
          children
        )}
      </MainContainer>
    </Layout>
  );
};

export default PageLayout;
