import { graphql } from 'gatsby';
import React, { useCallback, useMemo, useState } from 'react';
import v from 'vlid';
import { useDropzone } from 'react-dropzone';
import styled from 'styled-components';

import Form from '../components/form';
import { useNotifications } from '../components/providers/notification-provider';
import SelectInput from '../components/select-input';
import TextInput from '../components/text-input';
import useValidation from '../components/utils/useValidation';
import PageLayout from './page-layout';
import api from '../api';
import ReCaptcha from '../components/recaptcha';
import AcceptTerms from '../components/accept-terms';
import ErrorText from '../components/error-text';
import SiteConstants from '../site-constants';

const Dropzone = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.25);
  padding: 2em;
  box-sizing: border-box;
  margin-bottom: 0.5em;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.1);
  color: rgb(75, 75, 75);
`;

const ContactForm = ({
  data: {
    strapiPage: {
      PageSettings: Positions,
      ...pageData
    },
  },
}) => {
  const [isUploading, setIsUploading] = useState(false);
  const [Brand, setBrand] = useState(SiteConstants.brand);
  const [Position, setPosition] = useState('');
  const [Name, setName] = useState('');
  const [Email, setEmail] = useState('');
  const [Phone, setPhone] = useState('');
  const [Sex, setSex] = useState('');
  const [Captcha, setCaptcha] = useState('');
  const [Upload, setUpload] = useState('');
  const [submitted, setSubmitted] = useState(false);
  const [accepted, setAccepted] = useState(false);
  const onDrop = useCallback((acceptedFiles) => {
    setUpload('');

    if (acceptedFiles.length > 1) {
      setUpload('2');
      return;
    }

    const [file] = acceptedFiles;

    if (file.type !== 'application/pdf') {
      setUpload('1');
      return;
    }

    setIsUploading(true);

    (async () => {
      try {
        const { id } = await api.uploadFile(file);
        setUpload(id);
      } catch (e) {
        setIsUploading(false);
        notifications.queue({
          type: 'error',
          title: 'Error',
          text: e.message,
        })
      }
    })();
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  const formData = useMemo(() => {
    return {
      Brand,
      Position,
      Name,
      Sex,
      Email,
      Phone,
      Upload,
      Captcha,
      accepted,
    };
  }, [Brand, Position, Name, Sex, Email, Phone, Upload, Captcha, accepted]);

  const { isValid, errors, firstError } = useValidation(
    useCallback(() => {
      return v.object({
        Brand: v.string().required('Please select a brand.'),
        Position: v.string().required('Please select a job position.'),
        Name: v.string().required('Please provide your name.'),
        Email: v.string().email('Please provide a valid email address.'),
        Phone: v.string().regex(/^09\d{9}$/g, 'Please enter a valid phone number in the format "09170000000".'),
        Sex: v.string().required('Please select a gender.'),
        Captcha: v.string().required('Please complete the captcha.'),
        Upload: v.string()
          .required('Please upload your resume.')
          .rule((v) => (v !== '1'), 'Please upload a PDF.')
          .rule((v) => (v !== '2'), 'You may only upload 1 file.'),
        accepted: v.any().rule((v) => v === true, 'You must agree to the website terms and conditions.'),
      });
    }, []),
    formData,
  );
  const notifications = useNotifications();

  return (
    <PageLayout
      pageData={pageData}
    >
      <Form
        valid={isValid && !submitted}
        validationError={errors.map(({ message }) => message).join('\n')}
        onSubmit={async () => {
          await api.submit({
            ...formData,
            Type: 'Career',
            Job: formData.Position,
          });

          notifications.queue({
            type: 'info',
            title: 'Message Sent!',
            text: 'Thank you for your interest.'
          });

          setSubmitted(true);
        }}
      >
        <SelectInput name="job" label="Job Selection" value={Position} onChange={setPosition} error={firstError('Position')}>
          {Positions.map(({ Position }) => (
            <option value={Position} key={Position}>{Position}</option>
          ))}
        </SelectInput>
        <TextInput label="Name" value={Name} onChange={setName} error={firstError('Name')} />
        <div style={{ display: 'flex', flexDirection: 'row', marginBottom: '0.5em' }}>
          <label style={{ display: 'block', flex: 1 }}>
            <input type="radio" name="sex" value="Male" checked={Sex === 'Male'} onChange={() => setSex('Male')} />
            Male
          </label>
          <label style={{ display: 'block', flex: 1 }}>
            <input type="radio" name="sex" value="Female" checked={Sex === 'Female'} onChange={() => setSex('Female')} />
            Female
          </label>
        </div>
        {firstError('Sex') ? (
          <ErrorText style={{ textAlign: 'left', marginTop: '0em' }}>
            {firstError('Sex').message}
          </ErrorText>
        ) : null}
        <TextInput label="Phone" value={Phone} onChange={setPhone} error={firstError('Phone')} />
        <TextInput label="Email" value={Email} onChange={setEmail} error={firstError('Email')} />
        <b className="label-required">Please upload your resume in PDF format. Max file size of 500KB only.</b>
        <div style={{ height: '0.5em' }} />
        {isUploading ? (
          <Dropzone>
            {Upload && Upload.length > 1 ? (
              'File uploaded!'
            ) : (
              'Uploading file...'
            )}
          </Dropzone>
        ) : (
          <Dropzone {...getRootProps()}>
            <input {...getInputProps()} />
            {
              isDragActive ?
                <p>Right here!</p> :
                <p>Click or drag a file to this area to upload.</p>
            }
          </Dropzone>
        )}
        <ReCaptcha onChange={setCaptcha} />
        <div style={{ height: '0.5em' }} />
        <AcceptTerms value={accepted} onChange={setAccepted} />
        <div style={{ height: '1em' }} />
      </Form>
    </PageLayout >
  );
};

export default ContactForm;

export const query = graphql`
  query($pageId: String) {
    strapiPage(id: {eq: $pageId }) {
      Title
      Description
      MetaTitle
      MetaDescription
      PageSettings {
        Position
      }
    }
  }
`;
