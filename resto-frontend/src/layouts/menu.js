import { graphql } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import { Colors } from '../branding';
import Layout from '../components/layout';
import MainContainer from '../components/main-container';
import MenuList from '../components/menu/menu-list';
import ProductsList from '../components/menu/products-list';
import AvailabilityProvider from '../components/providers/availability-provider';
import BrandingProvider, { useBranding } from '../components/providers/branding-provider';

const MenuContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media only screen and (min-width: 800px) {
    flex-direction: row;
  }
`;

const CategoryTitle = styled.div`
  border-radius: 0.5em;
  padding: 0.25em 1em;
  font-size: 1.5em;
  margin-bottom: 0.5em;

  ${() => process.env.GATSBY_SITE === 'ltmt' ? `
    font-family: 'ThirstyRough';
  ` : ''}

  ${() => process.env.GATSBY_SITE !== 'ltmt' ? `
    color: white;
    font-family: inherit;
    font-weight: bold;
    padding: 0.45em 2em;
  ` : ''}

  ${() => process.env.GATSBY_SITE === 'ww' ? `
    text-align: center;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 0;
  ` : ''}

  ${() => process.env.GATSBY_SITE === 'an' ? `
    border-radius: 0;
  ` : ''}
`;

const Menu = ({
  data,
  pageContext: {
    productSlug,
  },
}) => {
  const {
    allStrapiProductCategories: {
      nodes: [
        {
          strapiId,
          Category,
          Slug,
        },
      ],
    },
  } = data;
  const branding = useBranding();
  const titleStyle = { backgroundColor: branding.Theme.Colors.primary };

  if (['an'].includes(process.env.GATSBY_SITE)) {
    titleStyle.backgroundColor = Colors.secondary;
  }

  if (process.env.GATSBY_SITE === 'rr') {
    titleStyle.backgroundColor = '#6c6cac';
  }

  if (process.env.GATSBY_SITE === 'pt') {
    titleStyle.color = 'black';
  }

  return (
    <>
      {process.env.GATSBY_SITE !== 'pt' ? (
        <div
          style={{
            height: '0.25em',
            backgroundColor: branding.Theme.Colors.primary,
            marginBottom: '1em',
          }}
        />
      ) : null}
      <MainContainer>
        <AvailabilityProvider allowBypass>
          <MenuContainer>
            <div style={{ flex: 1 }}>
              <MenuList
                currentCategory={Slug}
              />
            </div>
            <div style={{ flex: process.env.GATSBY_SITE === 'pt' ? 2 : 3, marginLeft: '0.5em' }}>
              <CategoryTitle style={titleStyle}>
                {Category}
              </CategoryTitle>
              <ProductsList
                categoryId={String(strapiId)}
                categorySlug={Slug}
                productSlug={productSlug}
              />
            </div>
          </MenuContainer>
        </AvailabilityProvider>
      </MainContainer>
    </>
  );
};

const MenuPage = (props) => {
  const {
    allStrapiProductCategories: {
      nodes: [
        {
          Metadata: {
            color,
          },
        },
      ],
    },
  } = props.data;

  return (
    <Layout invert={process.env.GATSBY_SITE === 'pt'}>
      <BrandingProvider
        override={(theme) => ({
          ...theme,
          Theme: {
            ...theme.Theme,
            Colors: {
              ...theme.Theme.Colors,
              primary: process.env.GATSBY_SITE === 'ltmt' ? color : theme.Theme.Colors.primary,
            },
          },
        })}
      >
        <Menu {...props} />
      </BrandingProvider>
    </Layout>
  );
};

export default MenuPage;

export const query = graphql`
  query($categorySlug: String, $site: String) {
    allStrapiProductCategories(filter: { Slug: { eq: $categorySlug }, Storefront: { Slug: { eq: $site } } }) {
      nodes {
        id
        strapiId
        Category
        Slug
        Metadata {
          color
        }
      }
    }
  }
`;
