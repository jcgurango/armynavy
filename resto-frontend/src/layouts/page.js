import { graphql } from 'gatsby';
import React from 'react';

import PageLayout from './page-layout';

const Page = ({
  data: {
    strapiPage: {
      HtmlContent,
      ...pageData
    },
  },
}) => {
  return (
    <PageLayout pageData={pageData}>
      <div
        dangerouslySetInnerHTML={{
          __html: HtmlContent,
        }}
      />
    </PageLayout>
  );
};

export default Page;

export const query = graphql`
  query($pageId: String) {
    strapiPage(id: {eq: $pageId }) {
      Title
      Description
      MetaTitle
      MetaDescription
      HtmlContent
    }
  }
`;
