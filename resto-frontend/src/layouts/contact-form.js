import { graphql } from 'gatsby';
import React, { useCallback, useMemo, useState } from 'react';
import v from 'vlid';

import Form from '../components/form';
import { useNotifications } from '../components/providers/notification-provider';
import SelectInput from '../components/select-input';
import TextInput from '../components/text-input';
import useValidation from '../components/utils/useValidation';
import PageLayout from './page-layout';
import api from '../api';
import ReCaptcha from '../components/recaptcha';
import AcceptTerms from '../components/accept-terms';
import SiteConstants from '../site-constants';

const ContactForm = ({
  data: {
    strapiPage: pageData,
  },
}) => {
  const [Brand, setBrand] = useState(SiteConstants.brand);
  const [Name, setName] = useState('');
  const [Email, setEmail] = useState('');
  const [Subject, setSubject] = useState('');
  const [Message, setMessage] = useState('');
  const [Captcha, setCaptcha] = useState('');
  const [submitted, setSubmitted] = useState(false);
  const [accepted, setAccepted] = useState(false);

  const { isValid, errors, firstError } = useValidation(
    useCallback(() => {
      return v.object({
        Brand: v.string().required('Please select a brand.'),
        Name: v.string().required('Please provide your name.'),
        Email: v.string().email('Please provide a valid email address.'),
        Subject: v.string().required('Please provide a subject.'),
        Message: v.string().required('Please enter a message.'),
        Captcha: v.string().required('Please complete the captcha.'),
        accepted: v.any().rule((v) => v === true, 'You must agree to the website terms and conditions.'),
      });
    }, []),
    useMemo(() => {
      return {
        Brand,
        Name,
        Email,
        Subject,
        Message,
        Captcha,
        accepted,
      };
    }, [Brand, Name, Email, Subject, Message, Captcha, accepted]),
  );
  const notifications = useNotifications();

  return (
    <PageLayout
      pageData={pageData}
    >
      <Form
        valid={isValid && !submitted}
        validationError={errors.map(({ message }) => message).join('\n')}
        onSubmit={async () => {
          await api.submit({
            Type: 'Contact',
            Brand,
            Name,
            Email,
            Subject,
            Message,
            Captcha,
          });

          notifications.queue({
            type: 'info',
            title: 'Message Sent!',
            text: 'Your message has been sent.'
          });

          setSubmitted(true);
        }}
      >
        <TextInput label="Name" value={Name} onChange={setName} error={firstError('Name')} />
        <TextInput label="Email" value={Email} onChange={setEmail} error={firstError('Email')} />
        <TextInput label="Subject" value={Subject} onChange={setSubject} error={firstError('Subject')} />
        <TextInput label="Message" value={Message} onChange={setMessage} multiline error={firstError('Message')} />
        <ReCaptcha onChange={setCaptcha} />
        <div style={{ height: '0.5em' }} />
        <AcceptTerms value={accepted} onChange={setAccepted} />
        <div style={{ height: '1em' }} />
      </Form>
    </PageLayout>
  );
};

export default ContactForm;

export const query = graphql`
  query($pageId: String) {
    strapiPage(id: {eq: $pageId }) {
      Title
      Description
      MetaTitle
      MetaDescription
    }
  }
`;
