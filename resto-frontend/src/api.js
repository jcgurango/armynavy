import axios from 'axios';

const api = axios.create({
  baseURL: '/api/',
});

const then = (response) => response.data;

const catchError = (e) => {
  console.error([e]);

  if (e.response) {
    if (e.response.status === 413) {
      throw new Error('Your attachment is not within the prescribed size limit. Please upload again.');
    }

    if (e.response.data && e.response.data.message) {
      if (Array.isArray(e.response.data.message)) {
        throw new Error(e.response.data.message[0].messages[0].message);
      }

      if (typeof (e.response.data.message) === 'string') {
        throw new Error(e.response.data.message);
      }

      throw new Error('Error');
    }
  }

  throw e;
};

export default {
  register: ({
    username,
    email,
    password,
  }) => {
    return api
      .post('/auth/local/register', { username, email, password })
      .then(then)
      .catch(catchError);
  },
  login: ({
    identifier,
    password,
  }) => {
    return api
      .post('/auth/local', { identifier, password })
      .then(then)
      .catch(catchError);
  },
  forgotPassword: ({
    email,
  }) => {
    return api
      .post('/auth/forgot-password', { email })
      .then(then)
      .catch(catchError);
  },
  resetPassword: ({
    code,
    password,
    passwordConfirm,
  }) => {
    return api
      .post('/auth/reset-password', {
        code,
        password,
        passwordConfirmation: passwordConfirm,
      })
      .then(then)
      .catch(catchError);
  },
  me: ({
    token,
  }) => {
    return api
      .get('/users/me', {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then(then)
      .catch(catchError);
  },
  updateAddress: ({
    token,
    address,
  }) => {
    return api
      .put('/users/me/addresses', address, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then(then)
      .catch(catchError);
  },
  updateDetails: ({
    token,
    userData,
  }) => {
    return api
      .put('/users/me/details', userData, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then(then)
      .catch(catchError);
  },
  checkAvailability: ({
    address,
    delivery,
  }) => {
    return api
      .post('/orders/availability', {
        address,
        delivery,
        site: process.env.GATSBY_SITE,
      })
      .then(then)
      .catch(catchError);
  },
  placeOrder: ({
    token,
    cart,
    address,
    delivery,
    messengerId,
  }) => {
    return api
      .post('/orders/place', {
        cart,
        address,
        delivery,
        messengerId,
      }, {
        headers: token ? {
          Authorization: 'Bearer ' + token,
        } : {},
      })
      .then(then)
      .catch(catchError);
  },
  confirmAccount: ({
    token,
  }) => {
    return api
      .post('/users/confirm', { token })
      .then(then)
      .catch(catchError);
  },
  submit: (obj) => {
    return api
      .post('/contact-submissions', obj)
      .then(then)
      .catch(catchError);
  },
  uploadFile: (file) => {
    const data = new window.FormData();
    data.append('files', file);

    return api.post('/contact-submissions/upload-file', data)
      .then(then)
      .catch(catchError);
  },
  lastOrder: (token) => {
    return api.get('/orders/last', {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then(then)
      .catch(catchError);
  },
  orders: (token, page) => {
    return api.get('/orders/mine', {
        headers: {
          Authorization: 'Bearer ' + token,
        },
        params: {
          page,
        },
      })
      .then(then)
      .catch(catchError);
  },
  claim: (product, voucher) => {
    return api.post('/vouchers/claim', {
        product,
        voucher,      
      })
      .then(then)
      .catch(catchError);
  },
};
