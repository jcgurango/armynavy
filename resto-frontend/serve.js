const { spawn } = require('child_process');
const express = require('express');
const http = require('http');
let currentProcess = null;
let isBuilding = false;

const rebuild = async () => {
  try {
    isBuilding = true;
    let succeeded = false;

    for (let i = 0; i < 10; i++) {
      try {
        if (i > 0) {
          console.log('Retrying... ' + i);
        }

        await new Promise((resolve, reject) => {
          const buildProcess = spawn('bash', ['-c', 'rm -rf /app-staging/ && cp -r /app-source /app-staging && cd /app-staging && npx gatsby clean && GATSBY_CPU_COUNT=1 npx gatsby build'], {
            stdio: ['inherit', 'inherit', 'inherit'],
          });
  
          buildProcess.on('exit', (code) => {
            if (code) {
              reject('Build process exited with code: ' + code);
            } else {
              resolve();
            }
          });
        });
  
        await new Promise((resolve, reject) => {
          const moveProcess = spawn('bash', ['-c', 'ls /app-staging/public/index.html && cd /app && npx gatsby clean && mv /app-staging/public /app/public'], {
            stdio: ['inherit', 'inherit', 'inherit'],
          });
  
          moveProcess.on('exit', (code) => {
            if (code) {
              reject('Move process exited with code: ' + code);
            } else {
              resolve();
            }
          });
        });

        succeeded = true;
        break;
      } catch (e) {
        console.error(e);
      }
    }

    if (!succeeded) {
      console.log('Failed!');
      throw new Error('Failed after 10 retries!');
    }
  } catch (e) {
    console.error(e);
  }

  isBuilding = false;
};

const proc = (firstRun) => {
  const start = () => currentProcess = spawn('bash', ['-c', 'cd /app && npx gatsby serve -H 0.0.0.0'], {
    stdio: ['inherit', 'inherit', 'inherit'],
  });

  if (firstRun) {
    rebuild().then(start);
  } else {
    start();
  }
};

proc(true);

const app = express();

app.use(async (req, res) => {
  if (isBuilding) {
    return res.send({
      error: 'Publish process currently running.',
    });
  }

  rebuild();

  res.send('OK');
});

/*
let hash = null;

const recheckProducts = async () => {
  await new Promise((resolve) => {
    const timeout = setTimeout(resolve, 120 * 1000);

    http.get('http://admin:1337/products?Storefront.Slug=' + process.env.GATSBY_SITE + '&_limit=1000&ValidFrom_lte=' + (new Date()).toISOString() + '&ValidTo_gte=' + (new Date()).toISOString(), (res) => {
      let response = '';

      res.on('data', (chunk) => {
        response += chunk.toString();
      });

      res.on('end', () => {
        try {
          const parsed = JSON.parse(response);
          const ids = [];

          parsed.forEach(({ id }) => {
            ids.push(id);
          });

          ids.sort();
          const newHash = ids.join(',');

          if (hash) {
            if (true && !isBuilding) {
              rebuild();
              hash = newHash;
            }
          }
        } catch (e) {
          console.error(e);
        }

        clearTimeout(timeout);
        setTimeout(resolve, 60000);
      });
    });
  });

  setImmediate(recheckProducts);
};

setTimeout(recheckProducts, 1000);
*/

app.listen(1986, () => {
  console.log('Builder set up...');
});
