require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

const manifest = {
  name: `gatsby-starter-default`,
  short_name: `starter`,
  start_url: `/`,
  background_color: `#663399`,
  theme_color: `#663399`,
  display: `minimal-ui`,
  icon: `src/images/icon-${process.env.GATSBY_SITE}.png`,
};

if (process.env.GATSBY_SITE === 'ltmt') {
  manifest.name = `LiberTea`;
  manifest.short_name = `ltmt`;
  manifest.background_color = manifest.theme_color = `#9CAF88`;
}

if (process.env.GATSBY_SITE === 'rr') {
  manifest.name = `RiceRocket`;
  manifest.short_name = `rr`;
  manifest.background_color = manifest.theme_color = `#6c6cac`;
}

if (process.env.GATSBY_SITE === 'ww') {
  manifest.name = `WrightWings`;
  manifest.short_name = `ww`;
  manifest.background_color = manifest.theme_color = `#d98723`;
}

if (process.env.GATSBY_SITE === 'an') {
  manifest.name = `ArmyNavy`;
  manifest.short_name = `an`;
  manifest.background_color = manifest.theme_color = `#30705a`;
}

module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: manifest,
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    {
      resolve: `gatsby-source-strapi`,
      options: {
        apiURL: process.env.BACKEND_API_BASE_URL,
        queryLimit: 1000, // Default to 100
        contentTypes: ['storefront', 'product', 'product-categories', 'pickup-locations', 'page', 'shared-product-addons'],
        //If using single types place them in this array.
        singleTypes: [],
        // Possibility to login with a strapi user, when content types are not publically available (optional).
      },
    },
    `gatsby-plugin-styled-components`,
    {
      resolve: 'gatsby-plugin-react-leaflet',
      options: {
        linkStyles: true // (default: true) Enable/disable loading stylesheets via CDN
      }
    },
  ],
}
