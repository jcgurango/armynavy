const { default: axios } = require('axios');
const crypto = require('crypto');
const SocksProxyAgent = require('socks-proxy-agent');
const { retrieveCached } = require('./caching');

const publicKey = process.env.OFS_PUBLIC_KEY;
const privateKey = process.env.OFS_PRIVATE_KEY;

const props = {};

if (process.env.USE_PROXY) {
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

  const agent = new SocksProxyAgent({
    host: 'proxy',
    port: 9050,
    rejectUnauthorized: false,
  });
  props.httpAgent = agent;
  props.httpsAgent = agent;
}

const ofsAxios = axios.create(props);

const makeRequest = async (action, contents, dataKey = 'list') => {
  const start = Date.now();

  const requestData = {
    api_key: publicKey,
    timestamp: Date.now(),
    source: 'web',
    hostname: 'ArmyNavy App V2',
    action,
    contents,
  };

  const data = {
    data: requestData,
    signature: crypto.createHmac('sha256', privateKey).update(JSON.stringify(requestData)).digest('base64'),
  };

  const { data: response } = await ofsAxios.post(process.env.OFS_ENDPOINT, data, props);

  console.log('OFS RESPONSE', JSON.stringify(data, null, '  '), '\n', response, (Date.now() - start) + 'ms');

  if (response.status !== 200) {
    throw new Error(response.error);
  }

  return response[dataKey];
};

module.exports = {
  getRegions: () => retrieveCached('ofs_region_list', () => makeRequest('get_region_list', {
    request: 'list',
  }), 30 * 60 * 1000),
  getCities: () => retrieveCached('ofs_city_list', () => makeRequest('get_city_list', {
    request: 'list',
  }), 30 * 60 * 1000),
  /**
   * @param {String} region 
   * @param {String} city 
   * @param {String} address 
   * @returns {any[] | 'no result'}
   */
  findStore: (region, city, address) => {
    const props = {
      city_name: city.toUpperCase(),
      full_address: address,
    };

    if (region) {
      props.region_name = region;
    }

    return makeRequest('assign_store', props, 'store_list');
  },
  checkAvailability: (store_id, items) => {
    return makeRequest('product_availability', {
      store_id,
      items,
    }, 'result');
  },
  placeOrder: (customer_info, delivery_address, order_items, store_details, other = {}) => {
    return makeRequest('customer_delivery_order', {
      customer_info,
      delivery_address,
      order_items,
      store_details,
      ...other,
    }, 'order_id');
  },
};
