let caches = {

};

const retrieveCached = async (key, retriever = async () => ({ }), refreshTime = 60000, autoUpdate = false) => {
  const refreshData = async () => {
    const start = Date.now();
    caches[key].loading = true;
    caches[key].lastRefresh = Date.now();
    caches[key].lastData = JSON.stringify(await retriever());
    caches[key].lastRefresh = Date.now();
    caches[key].loading = false;
    const time = Date.now() - start;

    if (autoUpdate) {
      const waitTime = Math.max(1, refreshTime - time - 100);
      console.log('Refreshing cache', key, 'in', waitTime + 'ms');
      setTimeout(refreshData, waitTime);
    }
  };

  if (!caches[key]) {
    caches[key] = {
      loading: false,
      lastData: '{ }',
      lastRefresh: 0,
    };
  }

  if (Date.now() - caches[key].lastRefresh > refreshTime) {
    // Cache is invalid.
    if (!caches[key].loading) {
      await refreshData();
    } else {
      while (caches[key].loading) {
        await new Promise((resolve) => setTimeout(resolve, 100));
      }
    }
  }

  return JSON.parse(caches[key].lastData);
};

module.exports = {
  retrieveCached,
};
