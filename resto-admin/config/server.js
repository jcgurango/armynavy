module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: env('URL'),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'b460ab5c0eee1aad973af72a1936d220'),
    },
    url: env('ADMIN_URL'),
    host: '0.0.0.0',
  },
  uploadsUrl: env('UPLOADS_URL'),
});
