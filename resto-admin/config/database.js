module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql2',
        host: env('DATABASE_HOST', 'mysql'),
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'resto'),
        username: env('DATABASE_USERNAME', 'resto'),
        password: env('DATABASE_PASSWORD', 'resto'),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
