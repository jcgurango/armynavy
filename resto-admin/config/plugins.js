module.exports = ({ env }) => ({
  email: {
    provider: 'nodemailer',
    providerOptions: {
      service: 'Outlook365',
      auth: {
        user: env('SMTP_USERNAME'),
        pass: env('SMTP_PASSWORD'),
      }
    },
    settings: {
      defaultFrom: env('SMTP_FROM'),
      defaultReplyTo: env('SMTP_REPLY_TO'),
    },
  },
});
