'use strict';

const { default: axios } = require('axios');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const endpointMap = {
  ltmt: 'http://frontend:1986',
  rr: 'http://frontend-rr:1986',
  ww: 'http://frontend-ww:1986',
  an: 'http://frontend-an:1986',
  pt: 'http://frontend-pt:1986',
};

module.exports = {
  async republish(ctx) {
    const id = ctx.params.id;

    // Retrieve the storefront.
    const storefront = await strapi.query('storefront').findOne({
      id,
    });

    if (!storefront) {
      return ctx.send({
        error: 'Storefront not found.',
      });
    }

    const endpoint = endpointMap[storefront.Slug];

    if (!endpoint) {
      return ctx.send({
        error: 'Unknown slug: ' + storefront.Slug,
      });
    }

    const {
      data,
    } = await axios.get(endpoint);

    if (data && data.error) {
      return ctx.send({
        error: data.error,
      });
    }

    ctx.send({ });
  },
};
