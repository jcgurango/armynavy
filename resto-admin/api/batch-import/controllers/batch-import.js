'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
const { parseString } = require('@fast-csv/parse');
const fs = require('fs');

const runImport = async (importJob) => {
  try {
    const importData = fs.readFileSync('public' + importJob.File.url, { encoding: 'utf-8' });
    const rows = [];
    let error = '';
  
    await new Promise((resolve) => {
      parseString(importData)
        .on('data', (row) => {
          if (row.join(',') !== 'Voucher,Product') {
            if (row[0]) {
              rows.push(row);
            }
          }
        })
        .on('error', (e) => {
          ctx.body = 'Error occurred: ' + (e.message);
          console.error(e);
          resolve();
          next();
        })
        .on('end', () => {
          resolve();
        });
    });
  
    await strapi.query('batch-import').update({ id: importJob.id }, {
      TotalRows: rows.length,
      Error: '',
    });
  
    for (let i = 0; i < rows.length; i++) {
      const [voucherCode, productText] = rows[i];
  
      // Check if the voucher code already exists.
      const existingVoucher = await strapi.query('voucher').findOne({
        Code: voucherCode,
      });
  
      if (existingVoucher) {
        error += `Warning: Voucher ${voucherCode} skipped (code already exists)\n`;
        await strapi.query('batch-import').update({ id: importJob.id }, {
          Error: error,
        });
      } else {
        let product = null;
  
        // Find the product by name.
        const productByExactName = await strapi.query('product').findOne({
          Name: productText,
          Voucher: true,
        });

        console.log(productByExactName);
  
        if (productByExactName) {
          product = productByExactName;
        } else {
          const productByName = await strapi.query('product').findOne({
            Name_contains: productText,
            Voucher: true,
          });
    
          if (productByName) {
            product = productByName;
          }
        }
  
        if (product) {
          await strapi.query('voucher').create({
            Code: voucherCode,
            Product: product,
          });
        } else {
          error += `Warning: Voucher ${voucherCode} skipped (unable to find product "${productText}")\n`;
          await strapi.query('batch-import').update({ id: importJob.id }, {
            Error: error,
          });
        }
      }
  
      await strapi.query('batch-import').update({ id: importJob.id }, {
        ImportedRows: i + 1,
      });
    }
  
    await strapi.query('batch-import').update({ id: importJob.id }, {
      Completed: true,
      Running: false,
    });
  } catch (e) {
    await strapi.query('batch-import').update({ id: importJob.id }, {
      Completed: false,
      Running: false,
      Error: e.message + '\n' + e.stack,
    });
  }
};

module.exports = {
  async start(ctx) {
    try {
      const importJob = await strapi.query('batch-import').findOne({
        id: ctx.params.id,
      });

      if (importJob.Completed) {
        throw new Error('Import has already completed.');
      }

      if (importJob.Running) {
        throw new Error('Import is already running.');
      }

      await strapi.query('batch-import').update({ id: importJob.id }, {
        Running: true,
      });

      runImport(importJob);

      ctx.send({
        status: 'OK',
      });
    } catch (e) {
      ctx.send({
        error: e.message,
      });
    }
  },
};
