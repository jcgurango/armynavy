'use strict';

const { default: axios } = require('axios');

const lastMessageTimestamps = {

};

module.exports = {
  async updateAddress(ctx) {
    if (ctx.state.user.id) {
      const {
        addressType,
        ...address
      } = ctx.request.body;

      await strapi.query('user', 'users-permissions').update({
        id: ctx.state.user.id,
      }, addressType === 'Home' ? {
        HomeAddress: address,
      } : {
        OfficeAddress: address,
      });

      ctx.send(await strapi.query('user', 'users-permissions').findOne({
        id: ctx.state.user.id,
      }));
    }
  },
  async updateDetails(ctx) {
    if (ctx.state.user.id) {
      const {
        FirstName,
        LastName,
        Phone,
        username,
        email,
        Password,
      } = ctx.request.body;

      if (Password) {
        const password = await strapi.plugins['users-permissions'].services.user.hashPassword({ password: Password });
        // Update user password
        await strapi
          .query('user', 'users-permissions')
          .update({ id: ctx.state.user.id }, { resetPasswordToken: null, password });
      }

      ctx.send(await strapi.query('user', 'users-permissions').update({
        id: ctx.state.user.id,
      }, {
        FirstName,
        LastName,
        Phone,
        username,
        email,
      }));
    }
  },
  async confirm(ctx) {
    const { token } = ctx.request.body;

    console.log(token);

    const user = await strapi.query('user', 'users-permissions').findOne({
      confirmationToken: token,
    });

    if (!user) {
      ctx.send({
        message: 'User not found.',
      }, 400);
      return;
    }

    await strapi.query('user', 'users-permissions').update({ id: user.id }, {
      confirmed: true,
      confirmationToken: null,
    });

    ctx.send('OK');
  },
  async messenger(ctx) {
    const VERIFY_TOKEN = '8nPb%hXKASF@R%iV^tcf34&R6XfE@YzNAYM83Za6*9pJMe^rcW6ZHk';

    if (ctx.request.method === 'POST') {
      const {
        object,
        entry: entries,
      } = ctx.request.body;

      if (object === 'page') {
        for (let i = 0; i < entries.length; i++) {
          const entry = entries[i];

          if (entry.messaging) {
            for (let m = 0; m < entry.messaging.length; m++) {
              const {
                sender: { id: senderId },
                recipient: { id: recipientId },
              } = entry.messaging[m];

              console.log(ctx.request.body, entry.messaging[m]);

              (async () => {
                try {
                  const timeSinceLastMessage = Date.now() - lastMessageTimestamps[senderId];
                  const storefront = await strapi.query('storefront').findOne({
                    MessengerPageID: recipientId,
                  });

                  if (storefront) {
                    // Check if 5 minutes has passed.
                    const passControl = timeSinceLastMessage < (5 * 60 * 1000);
                    console.log(timeSinceLastMessage, passControl);

                    if (passControl) {
                      await axios.post(`https://graph.facebook.com/v11.0/me/messages?access_token=${storefront.MessengerPageToken}`, {
                        messaging_type: 'MESSAGE_TAG',
                        tag: 'POST_PURCHASE_UPDATE',
                        recipient: {
                          id: senderId,
                        },
                        message: {
                          text: `A customer service representative will be with you shortly to address your concern.`,
                        },
                      });

                      await axios.post(`https://graph.facebook.com/v12.0/me/pass_thread_control?access_token=${storefront.MessengerPageToken}`, {
                        recipient: {
                          id: senderId,
                        },
                        target_app_id: 263902037430900
                      });

                      delete lastMessageTimestamps[senderId];
                    } else {
                      const endpoint = `https://graph.facebook.com/v11.0/me/messages?access_token=${storefront.MessengerPageToken}`;

                      await axios.post(endpoint, {
                        messaging_type: 'RESPONSE',
                        recipient: {
                          id: senderId,
                        },
                        sender_action: 'mark_seen',
                      });

                      await axios.post(endpoint, {
                        messaging_type: 'RESPONSE',
                        recipient: {
                          id: senderId,
                        },
                        sender_action: 'typing_on',
                      });

                      await axios.post(endpoint, {
                        messaging_type: 'RESPONSE',
                        recipient: {
                          id: senderId,
                        },
                        message: {
                          attachment: {
                            type: 'template',
                            payload: {
                              template_type: 'button',
                              text: `Hello, thank you for contacting ${storefront.Name}! If you'd like to place an order, click "Place Order" below. Otherwise, type a message.`,
                              buttons: [
                                {
                                  type: 'web_url',
                                  title: 'Place Order',
                                  url: storefront.MessengerAppUrl + '#' + senderId,
                                },
                              ],
                            },
                          },
                        },
                      });

                      await axios.post(endpoint, {
                        messaging_type: 'RESPONSE',
                        recipient: {
                          id: senderId,
                        },
                        sender_action: 'typing_off',
                      });


                      lastMessageTimestamps[senderId] = Date.now();
                    }
                  }
                } catch (e) {
                  if (e.isAxiosError) {
                    console.error('Messenger error', e.request, e.response.data);
                  } else {
                    console.error('Messenger error', e);
                  }
                }
              })();
            }
          }
        }
      }

      ctx.send('OK');
      return;
    }

    const {
      'hub.mode': mode,
      'hub.verify_token': token,
      'hub.challenge': challenge,
    } = ctx.request.query;

    if (mode && token) {
      if (mode === 'subscribe' && token === VERIFY_TOKEN) {
        ctx.send(challenge);
        return;
      }
    }

    ctx.response.status = 403;
  },
};
