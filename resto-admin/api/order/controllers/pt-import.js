const sharedProductAddons = [
  {
    "Name": "10\" Pizza",
    "Addons": [
      {
        "Name": "Mozzarella",
        "POSCode": "RCP00052",
        "Price": "70"
      },
      {
        "Name": "Ham",
        "POSCode": "RCP00053",
        "Price": "60"
      },
      {
        "Name": "Bacon",
        "POSCode": "RCP00054",
        "Price": "60"
      },
      {
        "Name": "Ground Beef",
        "POSCode": "RCP00055",
        "Price": "60"
      },
      {
        "Name": "Italian Sausage",
        "POSCode": "RCP00056",
        "Price": "60"
      },
      {
        "Name": "Pepperoni",
        "POSCode": "RCP00057",
        "Price": "60"
      },
      {
        "Name": "Salami",
        "POSCode": "RCP00058",
        "Price": "60"
      },
      {
        "Name": "Parmesan",
        "POSCode": "RCP00059",
        "Price": "70"
      },
      {
        "Name": "Button Mushroom",
        "POSCode": "RCP00060",
        "Price": "65"
      },
      {
        "Name": "Black Olives",
        "POSCode": "RCP00061",
        "Price": "35"
      },
      {
        "Name": "Feta Cheese",
        "POSCode": "RCP00062",
        "Price": "70"
      },
      {
        "Name": "Onion",
        "POSCode": "RCP00063",
        "Price": "35"
      },
      {
        "Name": "Green Bell Pepper",
        "POSCode": "RCP00064",
        "Price": "35"
      },
      {
        "Name": "Red Bell Pepper",
        "POSCode": "RCP00065",
        "Price": "35"
      },
      {
        "Name": "Pineapple",
        "POSCode": "RCP00066",
        "Price": "65"
      },
      {
        "Name": "Tomato",
        "POSCode": "RCP00067",
        "Price": "35"
      },
      {
        "Name": "Cream Cheese",
        "POSCode": "RCP00194",
        "Price": "70"
      }
    ]
  },
  {
    "Name": "14\" Pizza",
    "Addons": [
      {
        "Name": "Mozzarella",
        "POSCode": "RCP00072",
        "Price": "90"
      },
      {
        "Name": "Ham",
        "POSCode": "RCP00073",
        "Price": "80"
      },
      {
        "Name": "Bacon",
        "POSCode": "RCP00074",
        "Price": "80"
      },
      {
        "Name": "Ground Beef",
        "POSCode": "RCP00075",
        "Price": "80"
      },
      {
        "Name": "Italian Sausage",
        "POSCode": "RCP00076",
        "Price": "80"
      },
      {
        "Name": "Pepperoni",
        "POSCode": "RCP00077",
        "Price": "80"
      },
      {
        "Name": "Salami",
        "POSCode": "RCP00078",
        "Price": "80"
      },
      {
        "Name": "Parmesan",
        "POSCode": "RCP00079",
        "Price": "90"
      },
      {
        "Name": "Button Mushroom",
        "POSCode": "RCP00080",
        "Price": "85"
      },
      {
        "Name": "Black Olives",
        "POSCode": "RCP00097",
        "Price": "60"
      },
      {
        "Name": "Feta Cheese",
        "POSCode": "RCP00082",
        "Price": "90"
      },
      {
        "Name": "Onion",
        "POSCode": "RCP00083",
        "Price": "60"
      },
      {
        "Name": "Green Bell Pepper",
        "POSCode": "RCP00084",
        "Price": "60"
      },
      {
        "Name": "Red Bell Pepper",
        "POSCode": "RCP00085",
        "Price": "60"
      },
      {
        "Name": "Pineapple",
        "POSCode": "RCP00086",
        "Price": "85"
      },
      {
        "Name": "Tomato",
        "POSCode": "RCP00087",
        "Price": "60"
      },
      {
        "Name": "Cream Cheese",
        "POSCode": "RCP00195",
        "Price": "90"
      }
    ]
  },
  {
    "Name": "16\" Pizza",
    "Addons": [
      {
        "Name": "Mozzarella",
        "POSCode": "RCP00088",
        "Price": "110"
      },
      {
        "Name": "Ham",
        "POSCode": "RCP00089",
        "Price": "100"
      },
      {
        "Name": "Bacon",
        "POSCode": "RCP00090",
        "Price": "100"
      },
      {
        "Name": "Ground Beef",
        "POSCode": "RCP00091",
        "Price": "100"
      },
      {
        "Name": "Italian Sausage",
        "POSCode": "RCP00092",
        "Price": "100"
      },
      {
        "Name": "Pepperoni",
        "POSCode": "RCP00093",
        "Price": "100"
      },
      {
        "Name": "Salami",
        "POSCode": "RCP00094",
        "Price": "100"
      },
      {
        "Name": "Parmesan",
        "POSCode": "RCP00095",
        "Price": "110"
      },
      {
        "Name": "Button Mushroom",
        "POSCode": "RCP00096",
        "Price": "105"
      },
      {
        "Name": "Black Olives",
        "POSCode": "RCP00097",
        "Price": "80"
      },
      {
        "Name": "Feta Cheese",
        "POSCode": "RCP00098",
        "Price": "110"
      },
      {
        "Name": "Onion",
        "POSCode": "RCP00099",
        "Price": "80"
      },
      {
        "Name": "Green Bell Pepper",
        "POSCode": "RCP00100",
        "Price": "80"
      },
      {
        "Name": "Red Bell Pepper",
        "POSCode": "RCP00101",
        "Price": "80"
      },
      {
        "Name": "Pineapple",
        "POSCode": "RCP00102",
        "Price": "105"
      },
      {
        "Name": "Tomato",
        "POSCode": "RCP00103",
        "Price": "80"
      },
      {
        "Name": "Cream Cheese",
        "POSCode": "RCP00196",
        "Price": "110"
      }
    ]
  }
];

const categories = [
  {
    Category: 'Hand-Tossed Pizza',
    Homepage: true,
    Products: [
      {
        "Name": "Pepperoni",
        "Slug": "hand-tossed-pepperoni",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "380",
            "POSCode": "RCP00002",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "590",
            "POSCode": "RCP00010",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "830",
            "POSCode": "RCP00018",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Pepperoni & Mushroom",
        "Slug": "hand-tossed-pepperoni-and-mushroom",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "400",
            "POSCode": "RCP00006",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "610",
            "POSCode": "RCP00014",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "895",
            "POSCode": "RCP00022",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Hawaiian",
        "Slug": "hand-tossed-hawaiian",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "380",
            "POSCode": "RCP00003",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "590",
            "POSCode": "RCP00011",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "830",
            "POSCode": "RCP00019",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Cheese",
        "Slug": "hand-tossed-cheese",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "345",
            "POSCode": "RCP00001",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "565",
            "POSCode": "RCP00009",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "830",
            "POSCode": "RCP00017",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Four Cheese",
        "Slug": "hand-tossed-four-cheese",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "380",
            "POSCode": "RCP00005",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "590",
            "POSCode": "RCP00013",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "830",
            "POSCode": "RCP00021",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Meatza Pizza",
        "Slug": "hand-tossed-meatza-pizza",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "400",
            "POSCode": "RCP00004",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "610",
            "POSCode": "RCP00012",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "895",
            "POSCode": "RCP00020",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Very Veggie",
        "Slug": "hand-tossed-very-veggie",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "400",
            "POSCode": "RCP00008",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "610",
            "POSCode": "RCP00016",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "895",
            "POSCode": "RCP00024",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Super Telefono",
        "Slug": "hand-tossed-super-telefono",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "440",
            "POSCode": "RCP00007",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "725",
            "POSCode": "RCP00015",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "935",
            "POSCode": "RCP00023",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Gambas",
        "Slug": "hand-tossed-gambas",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "440",
            "POSCode": "RCP00154",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "725",
            "POSCode": "RCP00155",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "935",
            "POSCode": "RCP00156",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Margherita",
        "Slug": "hand-tossed-margherita",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "400",
            "POSCode": "RCP00178",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "620",
            "POSCode": "RCP00179",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "905",
            "POSCode": "RCP00180",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Creamy Margherita",
        "Slug": "hand-tossed-creamy-margherita",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "450",
            "POSCode": "RCP00188",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "745",
            "POSCode": "RCP00189",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "1020",
            "POSCode": "RCP00190",
            "HasSharedAddons": true
          }
        ]
      }
    ],
  },
  {
    Category: 'Premium Hand-Tossed',
    Homepage: true,
    Products: [
      {
        "Name": "Pacific Clam",
        "Slug": "premium-hand-tossed-pacific-clam",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "450",
            "POSCode": "RCP00115",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "745",
            "POSCode": "RCP00120",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "1020",
            "POSCode": "RCP00125",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Mexican",
        "Slug": "premium-hand-tossed-mexican",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "450",
            "POSCode": "RCP00116",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "745",
            "POSCode": "RCP00121",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "1020",
            "POSCode": "RCP00126",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Artichokes & Bacon",
        "Slug": "premium-hand-tossed-artichokes-and-bacon",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "450",
            "POSCode": "RCP00117",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "745",
            "POSCode": "RCP00122",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "1020",
            "POSCode": "RCP00127",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Thai Chicken",
        "Slug": "premium-hand-tossed-thai-chicken",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "450",
            "POSCode": "RCP00118",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "745",
            "POSCode": "RCP00123",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "1020",
            "POSCode": "RCP00128",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Tele Quattro",
        "Slug": "premium-hand-tossed-tele-quattro",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "450",
            "POSCode": "RCP00151",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "745",
            "POSCode": "RCP00152",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "1020",
            "POSCode": "RCP00153",
            "HasSharedAddons": true
          }
        ]
      }
    ],
  },
  {
    Category: 'Chicago Pizza Pot Pie',
    Homepage: true,
    Products: [
      {
        Name: 'Ragu & Maushroom',
        Variations: {
          Text: 'Default',
          POSCode: 'RCP00041',
          Price: '410',
        },
      },
    ],
  },
  {
    Category: 'Thin Crust Pizza',
    Homepage: true,
    Products: [
      {
        "Name": "Pepperoni",
        "Slug": "thin-crust-pepperoni",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00026",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00034",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00159",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Pepperoni & Mushroom",
        "Slug": "thin-crust-pepperoni-and-mushroom",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00030",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00038",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00160",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Hawaiian",
        "Slug": "thin-crust-hawaiian",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00027",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00035",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00161",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Cheese",
        "Slug": "thin-crust-cheese",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "285",
            "POSCode": "RCP00025",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "450",
            "POSCode": "RCP00033",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "715",
            "POSCode": "RCP00162",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Four Cheese",
        "Slug": "thin-crust-four-cheese",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00029",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00037",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00163",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Meatza Pizza",
        "Slug": "thin-crust-meatza-pizza",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00028",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00036",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00164",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Very Veggie",
        "Slug": "thin-crust-very-veggie",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00032",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00040",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00165",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Super Telefono",
        "Slug": "thin-crust-super-telefono",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00031",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00039",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00166",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Gambas",
        "Slug": "thin-crust-gambas",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00157",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00158",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00167",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Margherita",
        "Slug": "thin-crust-margherita",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00181",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00182",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00183",
            "HasSharedAddons": true
          }
        ]
      },
      {
        "Name": "Creamy Margherita",
        "Slug": "thin-crust-creamy-margherita",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "10\"",
            "Price": "310",
            "POSCode": "RCP00191",
            "HasSharedAddons": true
          },
          {
            "Text": "14\"",
            "Price": "520",
            "POSCode": "RCP00192",
            "HasSharedAddons": true
          },
          {
            "Text": "16\"",
            "Price": "800",
            "POSCode": "RCP00193",
            "HasSharedAddons": true
          }
        ]
      }
    ],
  },
  {
    Category: 'Pasta',
    Homepage: true,
    Products: [
      {
        "Name": "Spaghetti & Meatballs",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00044",
            "Price": "305"
          }
        ]
      },
      {
        "Name": "Creamy Carbonara",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00043",
            "Price": "305"
          }
        ]
      },
      {
        "Name": "Asian Pasta",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00042",
            "Price": "335"
          }
        ]
      },
      {
        "Name": "Pasta Al Telefono",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00045",
            "Price": "335"
          }
        ]
      }
    ],
  },
  {
    Category: 'Fried Chicken',
    Homepage: true,
    Products: [
      {
        "Name": "2-piece with rice",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00046",
            "Price": "215"
          }
        ]
      },
      {
        "Name": "3-piece",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00047",
            "Price": "285"
          }
        ]
      },
      {
        "Name": "5-piece",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00048",
            "Price": "465"
          }
        ]
      },
      {
        "Name": "7-piece",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00049",
            "Price": "650"
          }
        ]
      }
    ],
  },
  {
    Category: 'Baked Rolls',
    Homepage: false,
    Products: [
      {
        "Name": "ABC Roll",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00050",
            "Price": "310"
          }
        ]
      },
      {
        "Name": "Thai Chiken Roll",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00071",
            "Price": "310"
          }
        ]
      }
    ],
  },
  {
    Category: 'Soup and Salad',
    Homepage: false,
    Products: [
      {
        "Name": "Tomato Cream Soup",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00051",
            "Price": "130"
          }
        ]
      },
      {
        "Name": "Caesar Salad",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Single",
            "POSCode": "RCP00104",
            "Price": "205"
          },
          {
            "Text": "Sharing",
            "POSCode": "RCP00105",
            "Price": "310"
          }
        ]
      },
      {
        "Name": "Sweet Vinaigrette Salad",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Single",
            "POSCode": "RCP00106",
            "Price": "205"
          },
          {
            "Text": "Sharing",
            "POSCode": "RCP00107",
            "Price": "310"
          }
        ]
      },
    ],
  },
  {
    Category: 'Premium Breakfast',
    Homepage: false,
    Products: [
      {
        "Name": "Pancakes",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00129",
            "Price": "300"
          }
        ]
      },
      {
        "Name": "Waffle",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00130",
            "Price": "300"
          }
        ]
      },
      {
        "Name": "Garlic Smoked Sausage",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00132",
            "Price": "280"
          }
        ]
      },
      {
        "Name": "Tapa",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00133",
            "Price": "255"
          }
        ]
      },
      {
        "Name": "Bacon",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00134",
            "Price": "255"
          }
        ]
      },
      {
        "Name": "Longganisa",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00146",
            "Price": "280"
          }
        ]
      },
      {
        "Name": "Chicken Tocino",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00135",
            "Price": "245"
          }
        ]
      }
    ],
  },
  {
    Category: 'Beverages',
    Homepage: false,
    Products: [
      {
        "Name": "LiberTea Iced Tea",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "470ml",
            "POSCode": "RCP00184",
            "Price": "115"
          },
          {
            "Text": "940ml",
            "POSCode": "RCP00185",
            "Price": "190"
          },
        ],
      },
      {
        "Name": "Teamarind",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "470ml",
            "POSCode": "RCP00186",
            "Price": "115"
          },
          {
            "Text": "940ml",
            "POSCode": "RCP00187",
            "Price": "190"
          },
        ],
      },
      {
        "Name": "Coke",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00002",
            "Price": "65"
          }
        ]
      },
      {
        "Name": "Coke Zero",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00006",
            "Price": "65"
          }
        ]
      },
      {
        "Name": "Coke Light",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00007",
            "Price": "65"
          }
        ]
      },
      {
        "Name": "Sprite",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00003",
            "Price": "65"
          }
        ]
      },
      {
        "Name": "Royal",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00004",
            "Price": "65"
          }
        ]
      },
      {
        "Name": "Sarsi",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00005",
            "Price": "65"
          }
        ]
      },
      {
        "Name": "Minute Maid",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00008",
            "Price": "55"
          }
        ]
      },
      {
        "Name": "Bottled Water",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "BEV00009",
            "Price": "35"
          }
        ]
      }
    ],
  },
  {
    Category: 'Hot Wings',
    Homepage: false,
    Products: [
      {
        "Name": "3 Full-sized Wings",
        "ValidFrom": "1990-01-01",
        "ValidTo": "2100-01-01",
        "Variations": [
          {
            "Text": "Default",
            "POSCode": "RCP00108",
            "Price": "290"
          }
        ]
      },
    ],
  },
];

const locations = [
  {
    "Location": "JUPITER",
    "FullAddress": "74 Jupiter St., Bel-Air Village",
    "City": "Makati",
    "Latitude": "14.5617881",
    "Longitude": "121.0290763",
    "StoreOpen": "09:00:00",
    "StoreClose": "02:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  },
  {
    "Location": "BICUTAN",
    "FullAddress": "Doña Soledad Ave. cor. Afghanistan St., Brgy. Don Bosco",
    "City": "Paranaque",
    "Latitude": "14.484975",
    "Longitude": "121.0397551",
    "StoreOpen": "08:00:00",
    "StoreClose": "00:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  },
  {
    "Location": "WESTGATE ALABANG",
    "FullAddress": "Westgate, Filinvest Corporate City, Filinvest Coroporate City, Westgate, Alabang",
    "City": "Muntinlupa",
    "Latitude": "14.4219645",
    "Longitude": "121.0345702",
    "StoreOpen": "09:00:00",
    "StoreClose": "23:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  },
  {
    "Location": "LA SALLE TAFT",
    "FullAddress": "Ground Level, D Student Place Bldg., Estrada St. cor. Taft Avenue, Malate",
    "City": "Manila",
    "Latitude": "14.5645433",
    "Longitude": "120.9945981",
    "StoreOpen": "10:00:00",
    "StoreClose": "21:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  },
  {
    "Location": "BGC",
    "FullAddress": "32nd street, BGC",
    "City": "Taguig",
    "Latitude": "14.5541397",
    "Longitude": "121.0465335",
    "StoreOpen": "10:00:00",
    "StoreClose": "00:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  },
  {
    "Location": "LIPA",
    "FullAddress": "J.P. Laurel Highway Mataas na Lupa, Lipa City, Batangas (inside ArmyNavy)",
    "City": "Lipa",
    "Latitude": "13.944372",
    "Longitude": "121.1535258",
    "StoreOpen": "11:00:00",
    "StoreClose": "20:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  },
  {
    "Location": "CAINTA",
    "FullAddress": "Ortigas Avenue Extension corner Tanguile St., Saint Anthony Subdivision, Cainta, Rizal (inside ArmyNavy)",
    "City": "Cainta",
    "Latitude": "14.5811856",
    "Longitude": "121.1308601",
    "StoreOpen": "11:00:00",
    "StoreClose": "22:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  },
  {
    "Location": "VISAYAS AVENUE",
    "FullAddress": "70 Visayas Avenue, Brgy. Vasra (inside ArmyNavy)",
    "City": "Quezon City",
    "Latitude": "14.6620906",
    "Longitude": "121.0426023",
    "StoreOpen": "11:00:00",
    "StoreClose": "00:00:00",
    "PickupLocation": true,
    "Delivers": true,
    "Open247": false
  }
];

module.exports = { sharedProductAddons, categories, locations };
