'use strict';
const { default: axios } = require('axios');
const moment = require('moment');
const fs = require('fs');
const { getCities, getRegions, findStore, checkAvailability, placeOrder } = require('../../../ofs');
const { initiateCheckout, retrieveCheckout, upsertCallbackUrl } = require('../../../paymaya');
const { retrieveCached } = require('../../../caching');

const endpointMap = {
  'WW': 'http://admin-ww:1337',
  'RR': 'http://admin-rr:1337',
  'AN': 'http://admin-an:1337',
  'PT': 'http://admin-pt:1337',
};

const sendMessengerThankYou = async (order, storefront) => {
  try {
    const endpoint = `https://graph.facebook.com/v11.0/me/messages?access_token=${storefront.MessengerPageToken}`;

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      sender_action: 'mark_seen',
    });

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      sender_action: 'typing_on',
    });

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      message: {
        text: `Thank you for your order! We'll contact you soon.`,
      },
    });

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      sender_action: 'typing_off',
    });
  } catch (e) {
    if (e.isAxiosError) {
      console.error('Messenger error', e.request.options, e.response.data);
    } else {
      console.error('Messenger error', e);
    }
  }
};

const sendMessengerReceipt = async (order, storefront) => {
  try {
    const endpoint = `https://graph.facebook.com/v11.0/me/messages?access_token=${storefront.MessengerPageToken}`;

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      sender_action: 'mark_seen',
    });

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      sender_action: 'typing_on',
    });

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      message: {
        attachment: {
          type: 'template',
          payload: {
            template_type: 'receipt',
            recipient_name: order.RecipientName || `${order.FirstName} ${order.LastName}`,
            order_number: order.OrderNumber,
            currency: 'PHP',
            payment_method: order.PaymentMethod,
            order_url: 'https://www.armynavy.com.ph/orders',
            timestamp: moment.utc(order.created_at).unix(),
            address: order.DeliveryAddress ? {
              street_1: order.DeliveryAddress.StreetAddress,
              city: order.DeliveryAddress.City,
              postal_code: order.DeliveryAddress.ZipCode || '0000',
              state: order.DeliveryAddress.Region,
              country: 'PH',
            } : {
              street_1: order.PickupLocation.FullAddress || 'Army Navy',
              city: order.PickupLocation.City || 'NA',
              postal_code: 'NA',
              state: order.PickupLocation.Region || 'NA',
              country: 'PH',
            },
            adjustments: [],
            summary: {
              subtotal: calculateOrderItemsTotal(order.OrderItems),
              shipping_cost: order.DeliveryType === 'Pickup' ? 0 : getCurrentDeliveryFee(),
              total_tax: 0,
              total_cost: calculateOrderItemsTotal(order.OrderItems) + (order.DeliveryType === 'Pickup' ? 0 : getCurrentDeliveryFee()),
            },
            elements: order.OrderItems.map((item) => {
              let totalPrice = Number(item.Price);
              let title = item.ProductName + ` (₱${Number(item.Price).toFixed(2)})`;
              let subtitle = '';

              item.Addons.forEach((addon) => {
                if (subtitle) {
                  subtitle += '\n';
                }

                subtitle += `+${addon.Name} (+₱${Number(addon.Price).toFixed(2)})`;
                totalPrice += Number(addon.Price);
              });

              return {
                title,
                subtitle,
                price: totalPrice,
                quantity: item.Quantity,
                currency: 'PHP',
                image_url: 'https://placehold.it/150x150'
              };
            }),
          },
        },
      },
    });

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      message: {
        text: `For reference, your order number is ${order.OrderNumber}.`,
      },
    });

    await axios.post(endpoint, {
      messaging_type: 'MESSAGE_TAG',
      tag: 'POST_PURCHASE_UPDATE',
      recipient: {
        id: order.MessengerId,
      },
      sender_action: 'typing_off',
    });
  } catch (e) {
    if (e.isAxiosError) {
      console.error('Messenger error', e.request, e.response.data);
    } else {
      console.error('Messenger error', e);
    }
  }
};

const convertCart = async (cart) => {
  const convertedCart = [];
  const sharedAddons = await strapi.query('shared-product-addons').find();

  for (let i = 0; i < cart.length; i++) {
    const item = cart[i];
    item.options = item.options || {};
    const product = await strapi.query('product').findOne({
      id: item.productId,
    });

    const variation = product.Variations.find(({ id }) => Number(id) === Number(item.variationId));

    if (variation) {
      convertedCart.push({
        name: `${product.Name}${(product.Variations.length > 1) ? ` ${variation.Text}` : ''}`,
        qty: item.quantity,
        poscode: variation.POSCode,
        product_type: 1,
      });

      (variation.Addons || [])
        .concat(
          variation
            .SharedAddons
            .map((addon) => sharedAddons.find(({ id }) => id == addon.id))
            .reduce((current, next) => (current.concat(next.Addons)), [])
        )
        .filter(({ id }) => item.addons.indexOf(Number(id)) > -1)
        .forEach((addon) => {
          convertedCart.push({
            name: addon.Name,
            qty: item.quantity,
            poscode: addon.POSCode,
            product_type: 2,
          });
        });

      variation.FixedOptions.forEach((option) => {
        if (option.POSCode) {
          convertedCart.push({
            name: option.Name,
            qty: item.quantity,
            poscode: option.POSCode,
            product_type: 2,
          });
        }
      });

      variation.SelectableOptions.forEach((optionSet) => {
        Object.values(item.options[optionSet.id] || {}).forEach((optionId) => {
          const option = optionSet.Options.find(({ id }) => Number(id) === Number(optionId));

          convertedCart.push({
            name: option.Name,
            qty: item.quantity,
            poscode: option.POSCode,
            product_type: 2,
          });
        });
      });
    }
  }

  return convertedCart.filter(({ poscode }) => poscode);
};

let availabilitiesRunning = false;

const updateAvailabilities = async (skipCheck) => {
  if (!skipCheck) {
    if (availabilitiesRunning) {
      return;
    } else {
      availabilitiesRunning = true;
    }
  }

  if (process.env.STOREFRONT_ID) {
    try {
      const start = Date.now();
      const availabilities = await strapi.query('availability').find({
        storefront: process.env.STOREFRONT_ID,
      });
      const PER_RETRIEVAL = 5;

      for (let i = 0; i < availabilities.length; i += PER_RETRIEVAL) {
        const slice = availabilities.slice(i, i + PER_RETRIEVAL);
        
        await Promise.all(slice.map(async (availability) => {
          const availabilityResult = await checkAvailability(availability.StoreData.store_id, availability.CartData);
          await strapi.query('availability').update({
            id: availability.id,
          }, {
            AvailabilityData: availabilityResult,
          });
        }));
      }

      const remainingTime = (5 * 60 * 1000) - (Date.now() - start);

      if (remainingTime > 0) {
        console.log('Refreshing availabilities in', remainingTime, 'ms');
        setTimeout(() => updateAvailabilities(true), remainingTime);
      } else {
        setImmediate(() => updateAvailabilities(true));
      }
    } catch (e) {
      console.error('Availability update error', e);
    }
  }
};

const convertAddress = async (address) => {
  if (address.PickupLocation) {
    const location = await strapi.query('pickup-locations').findOne({
      id: address.PickupLocation,
    });

    if (!location) {
      return [null, null, null];
    }

    return [location.Region || null, location.City, location.FullAddress];
  }

  return [
    address.Region,
    address.City,
    [address.UnitNumber, address.StreetAddress, address.Barangay].filter(Boolean).join(', ') + ' ' + address.ZipCode,
  ];
};

const checkOrderAvailability = async (address, cart, returnUnavailable, addCity, isInitial) => {
  const cities = await getCities();
  const stores = await findStore(...address.map((part, index) => {
    if (index === 1 && addCity) {
      // Add "City" if OFS requires it.
      if (cities.find(({ city_name }) => `${part} City`.toUpperCase() === city_name)) {
        return `${part} City`;
      }
    }

    return part;
  }));

  if (stores === 'no result') {
    throw {
      display: true,
      message: 'We could not find a store to deliver to your address.',
    };
  }

  const [store] = stores;

  // Check availability of products.
  const response = await checkAvailability(store.store_id, cart);
  const unavailableProducts = [];

  response.forEach(({ poscode, available }) => {
    if (poscode && available !== 'yes') {
      // Find which product this is.
      const item = cart.find(({ poscode: pc }) => poscode === pc);
      unavailableProducts.push(item);
    }
  });

  if (!returnUnavailable && unavailableProducts.length) {
    throw {
      display: true,
      message: `Unfortunately, the following products are unavailable:\n${unavailableProducts.map(({ name }) => `- ${name}`).join('\n')}`,
    };
  }

  return {
    store,
    cart,
    unavailableProducts,
  };
};

const calculateOrderItemsTotal = (orderItems) => {
  let sum = 0;

  orderItems.forEach((item) => {
    sum += item.Price * item.Quantity;

    item.Addons.forEach((addon) => {
      sum += addon.Price * item.Quantity;
    });
  });

  return sum;
};

const processSubmission = async (order) => {
  const orderCart = [];

  order.OrderItems.forEach((item) => {
    orderCart.push({
      name: item.ProductName,
      qty: item.Quantity,
      poscode: item.POSCode,
      product_type: 1,
    });

    item.Addons.forEach((addon) => {
      if (addon.POSCode) {
        orderCart.push({
          name: addon.Name,
          qty: item.Quantity,
          poscode: addon.POSCode,
          product_type: 2,
        });
      }
    });
  });

  let orderAddress = null;
  let deliveryAddress = null;
  const regions = await getRegions();
  const cities = await getCities();
  const other = {};

  if (order.DeliveryType === 'Pickup') {
    orderAddress = {
      PickupLocation: order.PickupLocation.id,
    };
  } else {
    const { id: region } = regions.find(({ region_name }) => region_name === order.DeliveryAddress.Region);
    const city = cities.find(({ city_name, region_id }) => city_name === order.DeliveryAddress.City && region_id === region);

    deliveryAddress = {
      id: order.DeliveryAddress.id,
      company_name: order.DeliveryAddress.CompanyName || '',
      building: '',
      floor: order.DeliveryAddress.UnitNumber || '',
      area: order.DeliveryAddress.Barangay || '',
      street: order.DeliveryAddress.StreetAddress || '',
      city_id: city.id,
      city_name: city.city_name,
      landmark: order.SpecialInstructions,
      address_remarks: order.SpecialInstructions,
    };

    orderAddress = order.DeliveryAddress;
  }

  const address = await convertAddress(orderAddress);
  const { store } = await checkOrderAvailability(address, orderCart, false, order.DeliveryType === 'Pickup');

  if (order.DeliveryType === 'Pickup') {
    const city = cities.find(({ city_name }) => city_name === store.city);

    deliveryAddress = {
      id: order.id,
      city_id: city.id,
      city_name: city.city_name,
      landmark: order.SpecialInstructions,
      address_remarks: order.SpecialInstructions,
    };
  }

  if (order.DeliveryType === 'DeliverLater' || (order.DeliveryType === 'Pickup' && order.DeliveryDate)) {
    other.advance_order = {
      date: moment.utc(order.DeliveryDate).add(8, 'hours').format('YYYY-MM-DD'),
      time: moment.utc(order.DeliveryDate).add(8, 'hours').format('HH:mm:ss'),
    };
  }

  let payment_type = 'CC';

  if (order.PaymentMethod === 'COD') {
    payment_type = 'C';
  }

  if (order.PaymentMethod === 'Card') {
    payment_type = 'CCOD';
  }

  if (order.PaymentMethod === 'GCashScan') {
    payment_type = 'GCASH';
  }

  const orderId = await placeOrder(
    {
      contact_number: order.RecipientContactNumber || order.ContactNumber,
      email_address: order.Email,
      firstname: order.FirstName,
      lastname: order.LastName,
      order_remarks: order.SpecialInstructions,
      payment_type,
      tendered_amount: order.Tendered || 0,
      total_order_amount: calculateOrderItemsTotal(order.OrderItems),
      user_id: order.User ? order.User.id : '',
      is_guest: !(order.User && order.User.id),
      scd_id: order.ScdId,
      pwd_id: order.PwdId,
      for_pickup: order.DeliveryType === 'Pickup' || false,
    },
    deliveryAddress,
    orderCart,
    (order.ScdId || order.PwdId) ? {
      area_subd_district: '',
      city: '',
      coordinates: '',
      delivery_time: '',
      intersect_street: null,
      relevance: '',
      rta_id: '',
      store_code: 'API01',
      store_id: '200',
      store_name: '*API',
      street: '',
      available: true,
    } : store,
    other
  );

  await strapi.query('order').update({
    id: order.id,
  }, {
    Status: 'Confirmed',
    OfsId: orderId,
    FailureReason: null,
  });
};

const submitOrder = async (orderId, throwErrors) => {
  const order = await strapi.query('order').findOne({
    id: orderId,
  });

  if (order.Submitting) {
    console.error(`Order ${orderId} already submitting.`);
    return;
  }

  try {
    await strapi.query('order').update({ id: order.id }, {
      Submitting: true,
    });

    if (order.OrderItems.find(({ Voucher }) => Voucher)) {
      const vouchers = order.OrderItems.filter(({ Voucher }) => Voucher).map(({ Voucher }) => Voucher);

      for (let i = 0; i < vouchers.length; i++) {
        const voucher = vouchers[i];

        await strapi.query('voucher').update({ id: voucher.id || voucher }, {
          Claimed: true,
        });
      }

      await strapi.query('order').update({ id: order.id }, {
        Submitting: false,
        Status: 'AwaitingCSR',
      });
    } else {
      await processSubmission(order);
    }

    let AddressInfo = '';
    let DeliveryScheduleInfo = '';

    if (order.DeliveryType === 'Pickup') {
      AddressInfo = `Pickup from ${order.PickupLocation.Location}<br />${order.PickupLocation.FullAddress}<br />${order.PickupLocation.City}`;
    } else {
      AddressInfo = `
      Address Type: ${order.AddressType}<br />
      Street Address: ${order.DeliveryAddress.StreetAddress}<br />
      Barangay: ${order.DeliveryAddress.Barangay}<br />
      City: ${order.DeliveryAddress.City}
      `;
    }

    if (order.DeliveryType === 'DeliverNow') {
      DeliveryScheduleInfo = `Type: Deliver Now<br />Date: ${moment().utc().add(8, 'hours').format('LL')}<br />Time:`;
    } else if (order.DeliveryType === 'DeliverLater') {
      DeliveryScheduleInfo = `Type: Deliver Later<br />Date: ${moment.utc(order.DeliveryDate).add(8, 'hours').format('LL')}<br />Time: ${moment.utc(order.DeliveryDate).add(8, 'hours').format('LT')}`;
    } else if (order.DeliveryType === 'Pickup') {
      DeliveryScheduleInfo = `Type: Pickup<br />Date: ${moment.utc(order.DeliveryDate).add(8, 'hours').format('LL')}<br />Time: ${moment.utc(order.DeliveryDate).add(8, 'hours').format('LT')}`;
    }

    let Products = '';
    let Subtotal = 0;
    let Total = 0;
    let DeliveryFee = order.DeliveryType === 'Pickup' ? '0.00' : (getCurrentDeliveryFee()).toFixed(2);

    order.OrderItems.forEach((item) => {
      let totalPrice = Number(item.Price);
      let Product = item.ProductName + ` (&#8369;${Number(item.Price).toFixed(2)})`;

      item.Addons.forEach((addon) => {
        Product += `<br />+${addon.Name} (+&#8369;${Number(addon.Price).toFixed(2)})`;
        totalPrice += Number(addon.Price);
      });

      Subtotal += (totalPrice * item.Quantity);

      Products += `
        <tr>
          <td>${Product}</td>
          <td style="text-align: center;">${item.Quantity}</td>
          <td style="text-align: right;">&#8369;${(totalPrice * item.Quantity).toFixed(2)}</td>
        </tr>
      `;
    });

    if (order.DeliveryType === 'Pickup') {
      Total = Subtotal;
    } else {
      Total = Subtotal + getCurrentDeliveryFee();
    }

    let PaymentMethod = order.PaymentMethod;

    if (PaymentMethod === 'COD') {
      PaymentMethod += '<br />Change For: ' + order.ChangeFor;
    }

    if (order.MessengerId) {
      await sendMessengerReceipt(order, order.storefront);
    }

    await strapi.plugins['email-designer'].services.email.sendTemplatedEmail(
      {
        to: order.Email,
        from: 'no-reply@armynavy.com.ph',
      },
      {
        templateId: 1,
      },
      {
        ORDERNUMBER: order.OrderNumber,
        OrderNumber: order.OrderNumber,
        Name: `${order.FirstName} ${order.LastName}`,
        RecipientName: order.RecipientName || `${order.FirstName} ${order.LastName}`,
        RecipientContactNumber: order.RecipientContactNumber || order.ContactNumber,
        AddressInfo,
        DeliveryScheduleInfo,
        ScdId: order.ScdId,
        PwdId: order.PwdId,
        OrderNotes: order.SpecialInstructions,
        PaymentMethod,
        Products,
        Subtotal: Subtotal.toFixed(2),
        DeliveryFee,
        Total: Total.toFixed(2),
      }
    );
  } catch (e) {
    await strapi.query('order').update({
      id: order.id,
    }, {
      Status: 'Failed',
      FailureReason: e.message,
    });

    if (throwErrors) {
      throw e;
    }
  }

  await strapi.query('order').update({ id: order.id }, {
    Submitting: false,
  });
};

let deliveryFeeTimes = [
  {
    time: 0,
    fee: 50,
  },
];

const refreshDeliveryTimes = () => {
  if (fs.existsSync('delivery_times')) {
    deliveryFeeTimes = [
      {
        time: 0,
        fee: 50,
      },
      ...fs.readFileSync('delivery_times', { encoding: 'utf8' })
      .split('\n')
      .filter(Boolean)
      .map((x) => x.split('|'))
      .map(([time, fee]) => ({ time: (new Date(time)).getTime(), fee: parseFloat(fee) })),
    ];
  }

  setTimeout(refreshDeliveryTimes, 5000);
};

const getCurrentDeliveryFee = () => {
  let lastFee = 0;
  let lastTime = -1;

  for (let i = 0; i < deliveryFeeTimes.length; i++) {
    const { time, fee } = deliveryFeeTimes[i];
    
    if (time < Date.now() && time > lastTime) {
      lastFee = fee;
      lastTime = time;
    }
  }

  return lastFee;
};

refreshDeliveryTimes();

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
module.exports = {
  async test(ctx) {
    const locations = await strapi.query('pickup-locations').find({
      'storefront.Slug': 'an',
    });
    let lines = '';

    for (let i = 0; i < locations.length; i++) {
      const name = locations[i].Location;
      locations[i] = await convertAddress({
        PickupLocation: locations[i].id,
      });
      const [region, city, address] = locations[i].map((part, index) => {
        if (index === 1) {
          // Add "City" to the end for compatibility with cities that don't have "City" on them in Strapi.
          if (String(part).toLowerCase().indexOf('city') === -1 && String(part).toLowerCase() !== 'binan') {
            return `${part} City`;
          }
        }

        return part;
      });

      const props = {
        city_name: city.toUpperCase(),
        full_address: address,
      };

      if (region) {
        props.region_name = region;
      }

      locations[i] = props;
      lines += `Store: ${name}, assign_store Payload:\n${JSON.stringify(props, null, '  ')}\n\n`;
    }

    ctx.send(lines);
  },
  async resubmit(ctx) {
    const orderId = ctx.params.id;
    const order = await strapi.query('order').findOne({
      id: orderId,
    });

    const prefixes = Object.keys(endpointMap);

    for (let i = 0; i < prefixes.length; i++) {
      const prefix = prefixes[i];
      const endpoint = endpointMap[prefix];

      if (order.OrderNumber.indexOf(prefix) === 0 && prefix !== process.env.ORDER_PREFIX) {
        const response = await axios.post(`/orders/${orderId}/resubmit`, {}, {
          baseURL: endpoint,
          headers: ctx.request.header,
        });

        return ctx.send(response.data);
      }
    }

    await submitOrder(orderId);

    ctx.send(await strapi.query('order').findOne({
      id: orderId,
    }));
  },
  async regions(ctx) {
    const regions = await getRegions();
    ctx.send(regions);
  },
  async cities(ctx) {
    const cities = await getCities();
    ctx.send(cities);
  },
  async pay(ctx) {
    const { id, action } = ctx.params;

    const order = await strapi.query('order').findOne({
      id,
    });

    if (order && action === 'failure') {
      await strapi.query('order').update({
        id: order.id,
      }, {
        Status: 'Cancelled',
        FailureReason: 'PayMaya checkout failed.',
      });
      ctx.redirect('/failed');
      return;
    }

    if (order && (order.Status === 'Confirmed' || order.Status === 'Paid' || order.Submitting)) {
      if (order.MessengerId) {
        sendMessengerThankYou(order, order.storefront);
        ctx.redirect('/thank-you-messenger');
        return;
      }

      ctx.redirect('/thank-you');
      return;
    }

    if (!order || (order.Status !== 'Received')) {
      ctx.send('No order.');
      return;
    }

    try {
      if (action === 'pay') {
        const subtotal = calculateOrderItemsTotal(order.OrderItems);
        const deliveryFee = (order.DeliveryType === 'Pickup' ? 0 : getCurrentDeliveryFee());
        const total = subtotal + deliveryFee;
        let address = {};

        if (order.DeliveryAddress) {
          address = {
            streetAddress: order.DeliveryAddress.StreetAddress,
            city: order.DeliveryAddress.City,
            state: order.DeliveryAddress.Region,
            zipCode: order.DeliveryAddress.ZipCode,
          };
        }

        const {
          checkoutId,
          redirectUrl,
        } = await initiateCheckout({
          phone: order.ContactNumber,
          email: order.Email,
          firstName: order.FirstName,
          lastName: order.LastName,
          ...address,
          subtotal,
          deliveryFee,
          total,
          tax: total * 0.12,
          orderNumber: order.OrderNumber,
          orderId: order.id,
        }, order.MessengerId);

        await strapi.query('order').update({
          id: order.id,
        }, {
          PayMayaCheckoutId: checkoutId,
        });

        ctx.redirect(redirectUrl);
        return;
      }

      if (action === 'cancel') {
        await strapi.query('order').update({
          id: order.id,
        }, {
          Status: 'Cancelled',
        });
        ctx.redirect('/failed');
        return;
      }

      if (action === 'success') {
        if (!order.PayMayaCheckoutId) {
          ctx.redirect(`/orders/${id}/pay`);
          return;
        }

        const checkout = await retrieveCheckout(order.PayMayaCheckoutId, order.MessengerId);

        if (checkout.status !== 'COMPLETED') {
          ctx.redirect(`/orders/${id}/pay`);
          return;
        }

        await strapi.query('order').update({
          id: order.id,
        }, {
          Status: 'Paid',
          Tendered: checkout.totalAmount.amount,
        });

        submitOrder(order.id);

        if (order.MessengerId) {
          sendMessengerThankYou(order, order.storefront);
          ctx.redirect('/thank-you-messenger');
          return;
        }

        ctx.redirect('/thank-you');
        return;
      }
    } catch (e) {
      console.error(e);
      ctx.send(`Error: ${e.message}`, 500);
      return;
    }

    ctx.redirect('/');
  },
  async availability(ctx) {
    const {
      site,
      address,
      delivery: {
        DeliveryType,
      },
    } = ctx.request.body;

    try {
      // First retrieve the storefront.
      const start = Date.now();

      const {
        id,
      } = await strapi.query('storefront').findOne({
        Slug: site,
      });

      console.log('Find storefront', Date.now() - start + 'ms');

      // Retrieve all products for that storefront.
      const posCodeMap = await retrieveCached(JSON.stringify('pos_code_map_' + id), async () => {
        const products = await strapi.query('product').find({
          Storefront: id,
        });
  
        // Retrieve all shared addons.
        const sharedAddons = await strapi.query('shared-product-addons').find();
        const posCodeMap = {};
  
        products.forEach((product) => {
          product.Variations.forEach((variation) => {
            if (!posCodeMap[variation.POSCode + ':::' + 1]) {
              posCodeMap[variation.POSCode + ':::' + 1] = { variations: [], addons: [] };
            }
  
            posCodeMap[variation.POSCode + ':::' + 1].variations.push(variation.id);
  
            variation.Addons
              .concat(
                variation
                  .SharedAddons
                  .map((addon) => sharedAddons.find(({ id }) => id == addon.id))
                  .reduce((current, next) => (current.concat(next.Addons)), [])
              )
              .forEach((addon) => {
                if (!posCodeMap[addon.POSCode + ':::' + 2]) {
                  posCodeMap[addon.POSCode + ':::' + 2] = { variations: [], addons: [] };
                }
  
                posCodeMap[addon.POSCode + ':::' + 2].addons.push(addon.id);
              });
          });
  
          product.Addons.forEach((addon) => {
            if (!posCodeMap[addon.POSCode + ':::' + 2]) {
              posCodeMap[addon.POSCode + ':::' + 2] = { variations: [], addons: [] };
            }
  
            posCodeMap[addon.POSCode + ':::' + 2].addons.push(addon.id);
          });
        });

        return posCodeMap;
      }, 10 * 60 * 1000);

      console.log('Map codes', Date.now() - start + 'ms');

      const items = Object.keys(posCodeMap).filter(Boolean).map((poscode) => ({
        name: 'AVAILABILITY',
        poscode: poscode.split(':::')[0],
        product_type: Number(poscode.split(':::')[1]),
        qty: 1,
      }));

      console.log('Convert to items', Date.now() - start + 'ms');

      const {
        unavailableProducts,
      } = await checkOrderAvailability(await convertAddress(address), items, true, DeliveryType === 'Pickup', true);
      const v = [];
      const a = [];

      Object.keys(posCodeMap).forEach((poscode) => {
        if (!unavailableProducts.find(({ poscode: pc }) => poscode.split(':::')[0] === pc)) {
          v.push(...posCodeMap[poscode].variations);
          a.push(...posCodeMap[poscode].addons);
        }
      });

      console.log('Check availability', Date.now() - start + 'ms');

      ctx.send({
        v,
        a,
        deliveryFee: getCurrentDeliveryFee(),
      });
    } catch (e) {
      if (e.display) {
        ctx.send({
          message: e.message,
        }, 400);
      } else {
        throw e;
      }
    }
  },
  async place(ctx) {
    const {
      cart,
      address,
      delivery: {
        FirstName,
        LastName,
        Email,
        ContactNumber,
        RecipientName,
        RecipientContactNumber,
        SpecialInstructions,
        AddressType,
        DeliveryType,
        PaymentMethod,
        DeliveryDate,
        ScdId,
        PwdId,
        ChangeFor,
      },
      messengerId: MessengerId,
    } = ctx.request.body;

    try {
      // First ensure availability.
      const convertedCart = await convertCart(cart);

      if (convertedCart.length) {
        await checkOrderAvailability(await convertAddress(address), convertedCart, false, DeliveryType === 'Pickup');
      }

      const perProductSpecialInstructions = [];
      const OrderItems = [];
      const sharedAddons = await strapi.query('shared-product-addons').find();

      for (let i = 0; i < cart.length; i++) {
        const item = cart[i];
        item.options = item.options || {};
        const product = await strapi.query('product').findOne({
          id: item.productId,
        });

        const variation = product.Variations.find(({ id }) => Number(id) === Number(item.variationId));

        if (variation) {
          let voucherId = null;
          const Addons = [];

          variation.SelectableOptions.forEach((optionSet) => {
            Object.values(item.options[optionSet.id] || {}).forEach((optionId) => {
              const option = optionSet.Options.find(({ id }) => Number(id) === Number(optionId));

              if (option) {
                Addons.push(option);
              }
            });
          });

          variation.FixedOptions.forEach((option) => {
            if (option.POSCode) {
              Addons.push(option);
            }
          });

          Addons.push(...variation.Addons.concat(
            variation
              .SharedAddons
              .map((addon) => sharedAddons.find(({ id }) => id == addon.id))
              .reduce((current, next) => (current.concat(next.Addons)), [])
          ).filter(({ id }) => item.addons.indexOf(id) > -1));

          if (item.voucher) {
            const voucher = await strapi.query('voucher').findOne({
              Code: item.voucher,
              Product: item.productId,
            });

            if (!voucher || voucher.Claimed) {
              throw new Error('Voucher ' + item.voucher + ' doesn\'t exist or has already been claimed.');
            }

            voucherId = voucher.id;
          }

          OrderItems.push({
            Product: product.id,
            ProductName: `${product.Name}${(product.Variations.length > 1) ? ` ${variation.Text}` : ''}`,
            Quantity: voucherId ? 1 : item.quantity,
            POSCode: variation.POSCode,
            Addons,
            Price: voucherId ? 0 : variation.Price,
            Voucher: voucherId,
          });

          const instructions = [];

          Addons.forEach((addon) => {
            if (!addon.POSCode) {
              instructions.push(addon.Name);
            }
          });

          if (item.specialInstructions) {
            instructions.push(item.specialInstructions);
          }

          if (instructions.length) {
            perProductSpecialInstructions.push(`${product.Name}${(product.Variations.length > 1) ? ` ${variation.Text}` : ''} - ${instructions.join(', ')}`);
          }
        }
      }

      // Create the order.
      const OrderNumber = process.env.ORDER_PREFIX + ('00000000' + Math.floor(Math.random() * 99999999).toString()).slice(-8);

      const newOrder = await strapi.query('order').create({
        OrderNumber,
        FirstName,
        LastName,
        Email,
        ContactNumber,
        RecipientName,
        RecipientContactNumber,
        SpecialInstructions: [...perProductSpecialInstructions, SpecialInstructions].join('\n'),
        AddressType,
        DeliveryType,
        PaymentMethod,
        OrderItems,
        Status: 'Received',
        DeliveryAddress: DeliveryType === 'Pickup' ? null : address,
        PickupLocation: DeliveryType === 'Pickup' ? address.PickupLocation : null,
        User: (ctx.state.user && ctx.state.user.id) || null,
        DeliveryDate: DeliveryDate || null,
        ScdId,
        PwdId,
        ChangeFor,
        MessengerId,
        storefront: process.env.STOREFRONT_ID,
      });

      if (PaymentMethod !== 'Online' || (calculateOrderItemsTotal(newOrder.OrderItems) + (newOrder.DeliveryType === 'Pickup' ? 0 : getCurrentDeliveryFee())) === 0) {
        // Submit the order to OFS asynchronously.
        if (newOrder.MessengerId) {
          sendMessengerThankYou(newOrder, newOrder.storefront);
        }

        submitOrder(newOrder.id);
      }

      ctx.send(newOrder);
    } catch (e) {
      if (e.display) {
        ctx.send({
          message: e.message,
        }, 400);
      } else {
        throw e;
      }
    }
  },
  async lastOrder(ctx) {
    const user = ctx.state.user.id;

    const [order] = await strapi.query('order').find({
      User: user,
      _sort: 'created_at:desc',
      _limit: 1,
      OrderNumber_contains: process.env.ORDER_PREFIX,
    });

    if (!order) {
      return ctx.send(null);
    }

    const {
      id,
      ...data
    } = order;

    ctx.send(data);
  },
  async myOrders(ctx) {
    const user = ctx.state.user.id;
    const page = Number(ctx.request.query.page) || 1;

    const orders = await strapi.query('order').find({
      User: user,
      _limit: 10,
      _start: (page - 1) * 10,
      OrderNumber_contains: process.env.ORDER_PREFIX,
    });

    ctx.send(orders.map(({
      OrderNumber,
      FirstName,
      LastName,
      RecipientName,
      Status,
      DeliveryType,
      // eslint-disable-next-line no-unused-vars
      DeliveryAddress,
      PickupLocation,
      OrderItems,
      created_at,
    }) => ({
      OrderNumber,
      FirstName,
      LastName,
      RecipientName,
      Status,
      DeliveryType,
      DeliveryAddress: DeliveryAddress && {
        ...DeliveryAddress,
        id: undefined,
      },
      PickupLocation: PickupLocation && PickupLocation.Location,
      OrderItems: OrderItems.map(({ id, ProductName, Price, Addons, Quantity }) => ({
        id: 'order-items-' + id + '-nomLjPX8yO',
        ProductName,
        Price,
        Quantity,
        Addons: Addons.map(({ Name, Price }) => ({
          id: 'order-items-' + id + '-addon-wtDdGl0wdQ',
          Name,
          Price,
        })),
      })),
      Date: created_at,
    })));
  },
  async claimVoucher(ctx) {
    const {
      product,
      voucher,
    } = ctx.request.body;

    const voucherRecord = await strapi.query('voucher').findOne({
      Code: voucher,
      Product: product,
    });

    if (voucherRecord && !voucherRecord.Claimed) {
      return {
        result: 'ok',
      };
    }

    return {
      result: 'notFound',
    };
  },
  async paymayaWebhook(ctx) {
    const order = await strapi.query('order').findOne({
      PayMayaCheckoutId: ctx.request.body.id,
    });

    console.log('PAYMAYA WEBHOOK', ctx.request.body);

    if (order && order.Status != 'Paid' && order.Status != 'Confirmed') {
      if (ctx.request.body.paymentStatus === 'PAYMENT_SUCCESS') {
        await strapi.query('order').update({
          id: order.id,
        }, {
          Status: 'Paid',
        });

        this.resubmit({
          ...ctx,
          params: {
            ...ctx.params,
            id: order.id,
          },
          send: () => { },
        });
      } else if (['AUTH_FAILURE', 'PAYMENT_FAILURE', 'ACCOUNT_ABUSE', 'VOID', 'EXPIRED'].includes(ctx.request.body.paymentStatus)) {
        await strapi.query('order').update({
          id: order.id,
        }, {
          Status: 'Cancelled',
          FailureReason: 'PayMaya checkout failed.',
        });
      }
    }

    ctx.send('OK');
  },
  async registerWebhook(ctx) {
    try {
      await upsertCallbackUrl(Boolean(ctx.request.query.remove));
      await upsertCallbackUrl(Boolean(ctx.request.query.removem), true);
      ctx.send('OK');
    } catch (e) {
      console.error(e);
      ctx.send(e);
    }
  },
  updateAvailabilities,
  getCurrentDeliveryFee,
  getDeliveryTimes: () => deliveryFeeTimes,
};
