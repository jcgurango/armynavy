'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
const { parseFile } = require('@fast-csv/parse');
const koaBody = require('koa-body');

strapi.router.post('/vouchers/import', koaBody(), async (ctx, next) => {
  const file = ctx.request.files.import.path;
  const rows = [];

  await new Promise((resolve) => {
    parseFile(file)
      .on('data', (row) => {
        if (row.join(',') !== 'Voucher,Product') {
          rows.push(row);
        }
      })
      .on('error', (e) => {
        ctx.body = 'Error occurred: ' + (e.message);
        console.error(e);
        resolve();
        next();
      })
      .on('end', () => {
        resolve();
      });
  });

  let allowRedirect = true;
  ctx.body = '';

  for (let i = 0; i < rows.length; i++) {
    const [voucherCode, productText] = rows[i];

    // Check if the voucher code already exists.
    const existingVoucher = await strapi.query('voucher').findOne({
      Code: voucherCode,
    });

    if (existingVoucher) {
      ctx.body += `<p>Warning: Voucher ${voucherCode} skipped (code already exists)</p>\n`;
      allowRedirect = false;
    } else {
      let product = null;

      // Find the product by name.
      const productByName = await strapi.query('product').findOne({
        Name_contains: productText,
      });

      if (productByName) {
        product = productByName;
      }

      if (product) {
        await strapi.query('voucher').create({
          Code: voucherCode,
          Product: product,
        });
      } else {
        ctx.body += `<p>Warning: Voucher ${voucherCode} skipped (unable to find product "${productText}")</p>\n`;
        allowRedirect = false;
      }
    }
  }

  if (allowRedirect) {
    ctx.redirect('/admin/plugins/content-manager/collectionType/application::voucher.voucher');
  } else {
    ctx.set('Content-Type', 'text/html');
  }
});

module.exports = {};
