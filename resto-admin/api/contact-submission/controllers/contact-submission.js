'use strict';
const _ = require('lodash');
const recaptcha = require('../../../recaptcha');
const fs = require('fs');
const randomstring = require('randomstring');
const { pick } = require('lodash/fp');

const typeRecipients = {
  Contact: ['an_complaints@armynavy.com.ph'],
  Subscription: ['marketing@armynavy.com.ph'],
  Career: ['recruitment@armynavy.com.ph', 'crystal.valencia@armynavy.com.ph', 'marketing@armynavy.com.ph'],
};

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
module.exports = {
  async upload(ctx) {
    const {
      request: { files: { files } = {} },
    } = ctx;

    if (_.isEmpty(files) || files.size === 0) {
      throw strapi.errors.badRequest(null, {
        errors: [{ id: 'Upload.status.empty', message: 'Files are empty' }],
      });
    }

    if (files.type !== 'application/pdf') {
      throw strapi.errors.badRequest(null, {
        errors: [{ id: 'Upload.status.bad-file', message: 'Please upload a PDF' }],
      });
    }

    const id = randomstring.generate(32);
    fs.copyFileSync(files.path, 'public/uploads/cv_' + id + '.pdf');

    return { id };
  },
  async create(ctx) {
    const {
      Captcha,
      ...entity
    } = ctx.request.body;

    if (!Captcha) {
      throw strapi.errors.badRequest(null, {
        errors: [{ id: 'Contact.status.bad-captcha', message: 'Captcha validation missing.' }],
      });
    }

    const successful = await recaptcha.verify(Captcha);

    if (!successful) {
      throw strapi.errors.badRequest(null, {
        errors: [{ id: 'Contact.status.bad-captcha', message: 'Captcha validation failed.' }],
      });
    }

    if (entity.Type === 'Career' && entity.Upload) {
      entity.Upload = `${strapi.config.get('server.uploadsUrl', 'https://admin.armynavy.com.ph/uploads')}/cv_${entity.Upload}.pdf`;
    }
    const submission = await strapi.query('contact-submission').create(entity);

    if (typeRecipients[submission.Type] && typeRecipients[submission.Type].length) {
      const config = strapi.plugins.email.services.email.getProviderSettings();
      const { settings: { defaultFrom } } = pick(
        ['provider', 'settings.defaultFrom', 'settings.defaultReplyTo', 'settings.testAddress'],
        config
      );

      const emailDetails = ['id', 'Brand', 'Type', 'Name', 'Email', 'Phone', 'Sex', 'Job', 'Subject', 'Message', 'Upload']
        .filter((key) => submission[key])
        .map((key) => {
          return `<b>${key}:</b> ${(submission[key] || '').toString().replace(/"/g, '""')}`;
        })
        .join('<br />');

      const emailDetailsText = ['id', 'Brand', 'Type', 'Name', 'Email', 'Phone', 'Sex', 'Job', 'Subject', 'Message', 'Upload']
        .filter((key) => submission[key])
        .map((key) => {
          return `${key}: ${(submission[key] || '').toString().replace(/"/g, '""')}`;
        })
        .join('\n');

      await strapi.plugins['email'].services.email.send({
        to: typeRecipients[submission.Type],
        from: defaultFrom,
        subject: `Form Submission: ${submission.Type === 'Contact' ? 'Contact Us' : (submission.Type === 'Career' ? 'Careers' : submission.Type)}`,
        text: `
          Hello, you have received a new ${submission.Type} submission.
          
          ${emailDetailsText}
        `,
        html: `
          <p>Hello, you have received a new ${submission.Type} submission.</p>
          <p>${emailDetails}</P>
        `,
      });
    }

    return 'OK';
  },
  async export(ctx) {
    const { type } = ctx.params;
    let csv = 'Id,Brand,Type,Name,Email,Phone,Sex,Job,Subject,Message,Upload\n';

    const submissions = await strapi.query('contact-submission').find({
      Type: type,
    }, {
      __limit: 100000,
    });

    submissions.forEach((submission) => {
      csv += ['id', 'Brand', 'Type', 'Name', 'Email', 'Phone', 'Sex', 'Job', 'Subject', 'Message', 'Upload'].map((key) => {
        return `"${(submission[key] || '').toString().replace(/"/g, '""')}"`;
      }).join(',') + '\n';
    });

    ctx.send({
      csv,
    });
  },
};
