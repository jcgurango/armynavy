const paymayaPublic = require('axios').default.create({
  baseURL: process.env.PAYMAYA_PRODUCTION ? 'https://pg.paymaya.com' : 'https://pg-sandbox.paymaya.com',
  auth: {
    username: process.env.PAYMAYA_PUBLIC_KEY,
  },
});
const paymayaPrivate = require('axios').default.create({
  baseURL: process.env.PAYMAYA_PRODUCTION ? 'https://pg.paymaya.com' : 'https://pg-sandbox.paymaya.com',
  auth: {
    username: process.env.PAYMAYA_PRIVATE_KEY,
  },
});
const paymayaMessengerPublic = require('axios').default.create({
  baseURL: process.env.PAYMAYA_MESSENGER_PRODUCTION ? 'https://pg.paymaya.com' : 'https://pg-sandbox.paymaya.com',
  auth: {
    username: process.env.PAYMAYA_MESSENGER_PUBLIC_KEY,
  },
});
const paymayaMessengerPrivate = require('axios').default.create({
  baseURL: process.env.PAYMAYA_MESSENGER_PRODUCTION ? 'https://pg.paymaya.com' : 'https://pg-sandbox.paymaya.com',
  auth: {
    username: process.env.PAYMAYA_MESSENGER_PRIVATE_KEY,
  },
});

module.exports = {
  initiateCheckout: async ({
    phone,
    email,
    firstName,
    lastName,
    streetAddress,
    city,
    state,
    zipCode,
    subtotal,
    deliveryFee,
    total,
    tax,
    orderNumber,
    orderId,
  }, isMessenger) => {
    try {
      const response = await (isMessenger ? paymayaMessengerPublic : paymayaPublic).post('/checkout/v1/checkouts', {
        totalAmount: {
          value: total,
          currency: 'PHP',
          details: {
            discount: 0,
            serviceCharge: 0,
            shippingFee: deliveryFee,
            tax,
            subtotal,
          },
        },
        buyer: {
          firstName,
          lastName,
          contact: {
            phone,
            email,
          },
          shippingAddress: {
            firstName,
            lastName,
            phone,
            email,
            line1: streetAddress,
            line2: '',
            city,
            state,
            zipCode,
            countryCode: 'PH',
            shippingType: 'ST',
          },
          billingAddress: {
            line1: streetAddress,
            line2: '',
            city,
            state,
            zipCode,
            countryCode: 'PH',
          },
        },
        requestReferenceNumber: orderNumber,
        redirectUrl: {
          success: `${process.env.APP_URL}orders/${orderId}/success`,
          failure: `${process.env.APP_URL}orders/${orderId}/failure`,
          cancel: `${process.env.APP_URL}orders/${orderId}/cancel`
        },
      });

      return response.data;
    } catch (e) {
      if (e.response && e.response.data) {
        throw {
          ...e.response.data,
          message: e.response.data.error,
        };
      }

      throw e;
    }
  },
  retrieveCheckout: async (id, isMessenger) => {
    try {
      const response = await (isMessenger ? paymayaMessengerPrivate : paymayaPrivate).get('/checkout/v1/checkouts/' + id);

      return response.data;
    } catch (e) {
      if (e.response && e.response.data) {
        throw {
          ...e.response.data,
          message: e.response.data.error,
        };
      }

      throw e;
    }
  },
  upsertCallbackUrl: async (del, isMessenger) => {
    try {
      const webhookUrl = process.env.URL + 'orders/paymaya-webhook';

      if (del) {
        try {
          const { data: existingWebhooks } = await (isMessenger ? paymayaMessengerPrivate : paymayaPrivate).get('/checkout/v1/webhooks');

          // Look for this webhook.
          for (let i = 0; i < existingWebhooks.length; i++) {
            await (isMessenger ? paymayaMessengerPrivate : paymayaPrivate).delete(`/checkout/v1/webhooks/${existingWebhooks[i].id}`); 
          }
        } catch (e) {
          if (e.response && e.response.data) {
            console.error(e, e.response.data);
            throw {
              ...e.response.data,
              message: e.response.data.error,
            };
          }
        }
      }

      // Create the webhooks.
      await paymayaPrivate.post('/checkout/v1/webhooks', {
        name: 'CHECKOUT_SUCCESS',
        callbackUrl: webhookUrl,
      });
      await paymayaPrivate.post('/checkout/v1/webhooks', {
        name: 'CHECKOUT_FAILURE',
        callbackUrl: webhookUrl,
      });
      await paymayaPrivate.post('/checkout/v1/webhooks', {
        name: 'CHECKOUT_DROPOUT',
        callbackUrl: webhookUrl,
      });
    } catch (e) {
      if (e.response && e.response.data) {
        console.error(e, e.response.data);
        throw {
          ...e.response.data,
          message: e.response.data.error,
        };
      }

      throw e;
    }
  },
};
