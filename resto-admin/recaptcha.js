const recaptcha = require('axios').default.create({
  baseURL: 'https://google.com/recaptcha/api/siteverify',
});

module.exports = {
  verify: async (captcha) => {
    const response = await recaptcha.get('', {
      params: {
        secret: process.env.RECAPTCHA_SECRET_KEY,
        response: captcha,
      },
    });

    return response.data.success !== undefined && !!response.data.success;
  },
};
