'use strict';
const original = require('strapi-plugin-email-designer/services/email');

/**
 * email-designer.js email service
 */

const _ = require('lodash');
const isValidEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const decode = require('decode-html');
const { htmlToText } = require('html-to-text');
const { isEmpty } = require('lodash');

/**
 * fill subject, text and html using lodash template
 * @param {object} emailOptions - to, from and replyto...
 * @param {object} emailTemplate - object containing attributes to fill
 * @param {object} data - data used to fill the template
 * @returns {{ subject, text, subject }}
 */
const sendTemplatedEmail = async (emailOptions = {}, emailTemplate = {}, data = {}) => {
  Object.entries(emailOptions).forEach(([key, address]) => {
    if (Array.isArray(address)) {
      address.forEach((email) => {
        if (!isValidEmail.test(email)) throw new Error(`Invalid "${key}" email address with value "${email}"`);
      });
    } else {
      if (!isValidEmail.test(address)) throw new Error(`Invalid "${key}" email address with value "${address}"`);
    }
  });

  const requiredAttributes = ['templateId'];
  const attributes = [...requiredAttributes, 'text', 'html', 'subject'];
  const missingAttributes = _.difference(requiredAttributes, Object.keys(emailTemplate));
  if (missingAttributes.length > 0) {
    throw new Error(`Following attributes are missing from your email template : ${missingAttributes.join(', ')}`);
  }

  let { bodyHtml, bodyText, subject } = await strapi
    .query('email-template', 'email-designer')
    .findOne({ id: emailTemplate.templateId });

  if ((!bodyText || !bodyText.length) && bodyHtml && bodyHtml.length)
    bodyText = htmlToText(bodyHtml, { wordwrap: 130, trimEmptyLines: true });

  emailTemplate = {
    ...emailTemplate,
    subject:
      (!isEmpty(emailTemplate.subject) && emailTemplate.subject) ||
      (!isEmpty(subject) && decode(subject)) ||
      'No Subject',
    html: decode(bodyHtml),
    text: decode(bodyText),
  };

  const templatedAttributes = attributes.reduce(
    (compiled, attribute) =>
      emailTemplate[attribute]
        ? Object.assign(compiled, { [attribute]: _.template(emailTemplate[attribute])(data) })
        : compiled,
    {}
  );

  const options = { ...emailOptions, ...templatedAttributes };

  options.subject = `${process.env.EMAIL_PREFIX} ${options.subject}`;
  options.html = options.html.replace(/#96dbe5/g, process.env.BRAND_COLOR);

  return strapi.plugins.email.provider.send(options);
};

module.exports = {
  ...original,
  sendTemplatedEmail,
};
