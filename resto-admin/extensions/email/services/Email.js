'use strict';

const original = require('strapi-plugin-email/services/Email');

module.exports = {
  ...original,
  send: async (options) => {
    options.subject = `${process.env.EMAIL_PREFIX} ${options.subject}`;
    options.from = options.from && options.from.replace(/.+ <(.+)>/g, `${process.env.EMAIL_PREFIX} <$1>`);

    return original.send(options);
  },
};
