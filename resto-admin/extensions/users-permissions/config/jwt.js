const originalEmailValidation = require('strapi-plugin-users-permissions/controllers/validation/email-template');

originalEmailValidation.isValidEmailTemplate = require('../controllers/validation/email-template').isValidEmailTemplate;

module.exports = {
  jwtSecret: process.env.JWT_SECRET || '4e5e8e0b-3e9c-448e-9ac1-69ffcda638b3'
};
